@extends('merchant.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">{{ $user->UserName }} @lang('admin.message13')</h3>
                </div>
                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('retailer.index') }}">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="user-profile-cards-with-stats" class="row mt-2">
                    @foreach($user->FavouriteDriver as $driver)
                        <div class="col-xl-4 col-md-6 col-12">
                            <div class="card profile-card-with-stats">
                                <div class="text-center">
                                    <div class="card-body">
                                        <img src="{{ asset($driver->Driver->profile_image) }}"
                                             class="rounded-circle  height-150"
                                             alt="Card image">
                                    </div>
                                    <div class="card-body">
                                        <h4 class="card-title">{{ $driver->Driver->fullName }}</h4>
                                        <h4 class="card-title">{{ $driver->Driver->phoneNumber }}</h4>
                                        <h4 class="card-title">{{ $driver->Driver->email }}</h4>
                                    </div>
                                    <div class="card-body">
                                        <p>{{ $driver->Driver->CountryArea->AreaName}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </section>


            </div>
        </div>
    </div>
@endsection
