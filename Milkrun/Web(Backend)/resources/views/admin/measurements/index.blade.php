@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.melvin_measurement_message1')</h3>
                </div>

                <div class="col-md-6 col-12">
                    @if(session('moneyAdded'))
                        <div class="col-md-8 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>@lang('admin.message430')</strong>
                        </div>
                    @endif
                </div>


                <!--<div class="content-header-right col-md-2 col-12">-->
                <!--    <div class="btn-group float-md-right">-->
                <!--        <div class="heading-elements">-->
                <!--            <a href="{{route('retailer.create')}}">-->
                <!--                <button type="button" class="btn btn-icon btn-success mr-1"><i-->
                <!--                        class="fa fa-plus"></i>-->
                <!--                </button>-->
                <!--            </a>-->
                <!--        </div>-->
                <!--    </div>-->
                <!--</div>-->

            </div>
            <div class="content-body">
                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table
                                            class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>@lang('admin.melvin_measurement_message2')</th>
                                                <th>@lang('admin.melvin_measurement_message3')</th>
                                                <th>@lang('admin.melvin_measurement_message8')</th>
                                                <th>@lang('admin.melvin_measurement_message4')</th>
                                                <th>@lang('admin.melvin_measurement_message5')</th>
                                                <th>@lang('admin.melvin_measurement_message7')</th>
                                                <th>@lang('admin.created_at')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($measurements as $measurement)
                                                <tr>
                                                    <td>{{ $measurement->id }}</td>
                                                    <td>{{ $measurement->measure_unit_name }}</td>
                                                    <td>{{ $measurement->measure_unit }}</td>
                                                    <td>{{ $measurement->measure_unit_value }}</td>
                                                    <td>
                                                        @if($measurement->measure_unit_status == 1)
                                                            <label class="label_success">@lang('admin.active')</label>
                                                        @else
                                                            <label class="label_danger">@lang('admin.deactive')</label>
                                                        @endif
                                                    </td>

                                                    <td>

                                                        <a href="{{ route('measurements.edit',$measurement->id) }}"
                                                           data-original-title="Edit" data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_edit action_btn"> <i
                                                                class="fa fa-pencil"></i> </a>

                                                        @if($measurement->measure_unit_status == 1)
                                                            <a href="{{ route('measurement.active-deactive',['id'=>$measurement->id,'status'=>2]) }}"
                                                               data-original-title="Inactive" data-toggle="tooltip"
                                                               data-placement="top"
                                                               class="btn menu-icon btn_eye_dis action_btn"> <i
                                                                    class="fa fa-eye-slash"></i> </a>
                                                        @else
                                                            <a href="{{ route('measurement.active-deactive',['id'=>$measurement->id,'status'=>1]) }}"
                                                               data-original-title="Active" data-toggle="tooltip"
                                                               data-placement="top"
                                                               class="btn menu-icon btn_eye action_btn"> <i
                                                                    class="fa fa-eye"></i> </a>
                                                        @endif
                                                    </td>
                                                    <td>{{ $measurement->created_at->toformatteddatestring() }}</td>
                                                </tr>

                                            @endforeach

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                {{--<div class="col-sm-12">--}}
                                    {{--<div class="pagination1">{{ $measurements->links() }}</div>--}}
                                {{--</div>--}}
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>

@endsection
