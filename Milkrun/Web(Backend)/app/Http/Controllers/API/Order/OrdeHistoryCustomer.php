<?php
/**
 * Created by PhpStorm.
 * User: Yogesh Apporio
 * Date: 20-12-2018
 * Time: 16:14
 * app_type ==> 1 For Retailer
 * app_type ==> 2 For Deliverer
 * app_type ==> 3 For User
 */

namespace App\Http\Controllers\API\Order;

use App\Models\Driver;
use App\Models\Retailer;
use App\Models\Customer;
use App\Models\Country;
use App\Models\Onesignal;
use App\Models\OrderManagement;
use App\Models\Station;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client;

class OrdeHistoryCustomer extends Controller
{

    public function OrderHistory(Request $request)
    {
        $customer = $request->user('api_customer');
        //$pending_orders = $driver->PendingOrderManagements->toArray();
        $pending_orders = OrderManagement::with(['Retailer','Customer'])->where([['driver_id',false]])->get();
        $active_orders = $customer->AcceptedOrderManagements->toArray();
        $completed_orders = $customer->CompletedOrdeManagements->toArray();
        return response()->json(['result' => "1", 'message' => "Orde History Customer", 'data' => ['pending_orders'=>$pending_orders,'active_orders'=>$active_orders,'completed_orders'=>$completed_orders]]);
    }
}