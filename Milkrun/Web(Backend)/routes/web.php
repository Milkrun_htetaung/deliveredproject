<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Admin Routes

Route::prefix('admin')->group(function () {
    Route::get('/', ['as' => 'admin.login', 'uses' => 'Auth\AdminLoginController@showLoginForm']);
    Route::post('/login', ['as' => 'admin.login.submit', 'uses' => 'Auth\AdminLoginController@login']);
    Route::group(['middleware' => ['auth:admin']], function () {
        Route::get('/home', ['as' => 'admin.dashboard', 'uses' => 'Admin\AdminController@index']);
        Route::get('/change-language/{locale}', ['as' => 'admin.language', 'uses' => 'Admin\AdminController@SetLangauge']);
        Route::get('/logout', ['as' => 'admin.logout', 'uses' => 'Auth\AdminLoginController@logout']);
        Route::resource('documents', 'Admin\DocumentController');
        Route::post('document/update', ['as' => 'doc.update', 'uses' => 'Admin\DocumentController@update']);
        Route::resource('cms', 'Admin\CmsPagesController');
        Route::post('/cms/search', ['as' => 'admin.cms.search', 'uses' => 'Admin\CmsPagesController@Search']);

        Route::resource('promotions', 'Admin\PromotionNotificationController');
        Route::post('/promotions/search', ['as' => 'promotions.search', 'uses' => 'Admin\PromotionNotificationController@Search']);

        Route::get('/onesignal', ['as' => 'onesignal.index', 'uses' => 'Admin\AdminController@OneSignal']);
        Route::post('/onesignal', ['as' => 'onesignal.store', 'uses' => 'Admin\AdminController@UpdateOneSignal']);

        Route::get('/customer_support', ['as' => 'customer_support.index', 'uses' => 'Admin\AdminController@Customer_Support']);
        Route::post('/customer_support', ['as' => 'customer_support.search', 'uses' => 'Admin\AdminController@Customer_Support_Search']);

//Retailer
        Route::resource('retailer', 'Admin\RetailerController');
        Route::get('/retailer/wallet/{id}', ['as' => 'retailer.wallet', 'uses' => 'Admin\RetailerController@Wallet']);
        Route::post('/retailer/addmoney', ['as' => 'retailer.add.wallet', 'uses' => 'Admin\RetailerController@AddWalletMoney']);
        Route::get('/retailer/active/deactive/{id}/{status}', ['as' => 'retailer.active-deactive', 'uses' => 'Admin\RetailerController@ChangeStatus']);
        Route::post('/search/retailer', ['as' => 'retailer.search', 'uses' => 'Admin\RetailerController@Serach']);

//Driver
        Route::resource('drivers', 'Admin\DriverController');
        Route::get('/driver/wallet/{id}', ['as' => 'driver.wallet', 'uses' => 'Admin\DriverController@Wallet']);
        Route::post('/driver/addmoney', ['as' => 'driver.add.wallet', 'uses' => 'Admin\DriverController@AddWalletMoney']);
        Route::get('/driver/active/deactive/{id}/{status}', ['as' => 'driver.active-deactive', 'uses' => 'Admin\DriverController@ChangeStatus']);
        Route::post('/search/driver', ['as' => 'driver.search', 'uses' => 'Admin\DriverController@Serach']);

//Customer
        Route::resource('customers', 'Admin\CustomersController');
        Route::get('/customer/wallet/{id}', ['as' => 'customer.wallet', 'uses' => 'Admin\CustomersController@Wallet']);
        Route::post('/customer/addmoney', ['as' => 'customer.add.wallet', 'uses' => 'Admin\CustomersController@AddWalletMoney']);
        Route::get('/customer/active/deactive/{id}/{status}', ['as' => 'customer.active-deactive', 'uses' => 'Admin\CustomersController@ChangeStatus']);
        Route::post('/search/customer', ['as' => 'customer.search', 'uses' => 'Admin\CustomersController@Serach']);

//OrdersManagement
            Route::resource('orders', 'Admin\OrderManagementController');

//Station
        Route::resource('stations', 'Admin\StationController');
        Route::get('/station/wallet/{id}', ['as' => 'station.wallet', 'uses' => 'Admin\StationController@Wallet']);
        Route::post('/station/addmoney', ['as' => 'station.add.wallet', 'uses' => 'Admin\StationController@AddWalletMoney']);
        Route::get('/station/active/deactive/{id}/{status}', ['as' => 'station.active-deactive', 'uses' => 'Admin\StationController@ChangeStatus']);
        Route::post('/search/station', ['as' => 'station.search', 'uses' => 'Admin\StationController@Serach']);

//Station
        Route::resource('measurements', 'Admin\MeasurementController');
        Route::get('/measurement/wallet/{id}', ['as' => 'measurement.wallet', 'uses' => 'Admin\MeasurementController@Wallet']);
        Route::post('/measurement/addmoney', ['as' => 'measurement.add.wallet', 'uses' => 'Admin\MeasurementController@AddWalletMoney']);
        Route::get('/measurement/active/deactive/{id}/{status}', ['as' => 'measurement.active-deactive', 'uses' => 'Admin\MeasurementController@ChangeStatus']);
        Route::post('/search/measurement', ['as' => 'measurement.search', 'uses' => 'Admin\MeasurementController@Serach']);

    });
});


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
