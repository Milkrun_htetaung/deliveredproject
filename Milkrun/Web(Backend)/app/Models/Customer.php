<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Customer extends Authenticatable
{
    use Notifiable,HasApiTokens;
    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token','pivot'
    ];

    public function Retailer()
    {
        return $this->belongsToMany(Retailer::class);
    }
    
    public function findForPassport($user_cred = null)
    {
        return Customer::where([['customer_phone', '=', $user_cred]])->first();
    }
    
    public function PendingOrderManagements()
    {
        return $this->HasMany(OrderManagement::class)->with(['Customer','Retailer'])->whereIn('order_status', array(1));
    }

    public function AcceptedOrderManagements()
    {
        return $this->HasMany(OrderManagement::class)->with(['Customer','Driver'])->whereIn('order_status', array(2,3,4));;
    }

    public function CompletedOrdeManagements()
    {
        return $this->HasMany(OrderManagement::class)->with(['Customer','Driver'])->whereIn('order_status', array(5,6));;
    }
}
