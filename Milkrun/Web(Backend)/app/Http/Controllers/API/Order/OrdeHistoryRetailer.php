<?php
/**
 * Created by PhpStorm.
 * User: Yogesh Apporio
 * Date: 20-12-2018
 * Time: 16:14
 * app_type ==> 1 For Retailer
 * app_type ==> 2 For Deliverer
 * app_type ==> 3 For User
 */

namespace App\Http\Controllers\API\Order;

use App\Models\Driver;
use App\Models\Retailer;
use App\Models\Customer;
use App\Models\Country;
use App\Models\Onesignal;
use App\Models\OrderManagement;
use App\Models\Station;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client;

class OrdeHistoryRetailer extends Controller
{

    public function OrderHistory(Request $request)
    {
        $retailer = $request->user('api_retailer');
        $pending_orders = $retailer->PendingOrderManagements->toArray();
        $active_orders = $retailer->AcceptedOrderManagements->toArray();
        $completed_orders = $retailer->CompletedOrdeManagements->toArray();
        return response()->json(['result' => "1", 'message' => "Orde History Retailer", 'data' => ['pending_orders'=>$pending_orders,'active_orders'=>$active_orders,'completed_orders'=>$completed_orders]]);
    }
}