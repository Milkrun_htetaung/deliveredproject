<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.message14'); ?></h3>
                </div>
                <div class="col-md-6 col-12">
                    <?php if(session('rideradded')): ?>
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo app('translator')->getFromJson('admin.message224'); ?></strong>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="<?php echo e(route('users.index')); ?>">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data"
                                              action="<?php echo e(route('users.update', $user->id)); ?>">
                                            <?php echo e(method_field('PUT')); ?>

                                            <?php echo csrf_field(); ?>
                                            <fieldset>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                <?php echo app('translator')->getFromJson('admin.message382'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="retailer_shop_name"
                                                                   name="user_name" value="<?php echo e($user->retailer_shop_name); ?>"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.message382'); ?>" required>
                                                            <?php if($errors->has('retailer_shop_name')): ?>
                                                                <label class="danger"><?php echo e($errors->first('retailer_shop_name')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                <?php echo app('translator')->getFromJson('admin.message383'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="user_name"
                                                                   name="user_name" value="<?php echo e($user->UserName); ?>"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.message383'); ?>" required>
                                                            <?php if($errors->has('user_name')): ?>
                                                                <label class="danger"><?php echo e($errors->first('user_name')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                <?php echo app('translator')->getFromJson('admin.message384'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="retailer_shop_address"
                                                                   name="retailer_shop_address"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.message384'); ?>"
                                                                   value="<?php echo e($user->retailer_shop_address); ?>" required>
                                                            <?php if($errors->has('retailer_shop_address')): ?>
                                                                <label class="danger"><?php echo e($errors->first('retailer_shop_address')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>



                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="lastName3">
                                                                <?php echo app('translator')->getFromJson('admin.RiderPhone'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="user_phone"
                                                                   name="user_phone"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.RiderPhone'); ?>"
                                                                   value="<?php echo e($user->UserPhone); ?>" required>
                                                            <?php if($errors->has('user_phone')): ?>
                                                                <label class="danger"><?php echo e($errors->first('user_phone')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>


                                                </div>


                                                <div class="row">




                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                <?php echo app('translator')->getFromJson('admin.RiderEmail'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="email" class="form-control" id="user_email"
                                                                   name="user_email"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.RiderEmail'); ?>"
                                                                   value="<?php echo e($user->email); ?>" required>
                                                            <?php if($errors->has('user_email')): ?>
                                                                <label class="danger"><?php echo e($errors->first('user_email')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ProfileImage">
                                                                <?php echo app('translator')->getFromJson('admin.ProfileImage'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="file" class="form-control" id="profile"
                                                                   name="profile"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.ProfileImage'); ?>">
                                                            <?php if($errors->has('profile')): ?>
                                                                <label class="danger"><?php echo e($errors->first('profile')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                <?php echo app('translator')->getFromJson('admin.RiderPassword'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="password" class="form-control" id="password"
                                                                   name="password"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.RiderPassword'); ?>" disabled>
                                                            <?php if($errors->has('password')): ?>
                                                                <label class="danger"><?php echo e($errors->first('password')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <fieldset class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="1" name="edit_password"
                                                                       id="edit_password" onclick="EditPassword()">
                                                                <?php echo app('translator')->getFromJson('admin.message223'); ?>
                                                            </label>
                                                        </fieldset>
                                                    </div>
                                                </div>


                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script>
        function EditPassword() {
            if (document.getElementById("edit_password").checked = true) {
                document.getElementById('password').disabled = false;
            }
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>