@extends('merchant.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">{{ $driver->fullName }} @lang('admin.message260')</h3>

                </div>
                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('driver.index') }}">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="content-body">

                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>@lang('admin.message261')</th>
                                                <th>@lang('admin.payment')</th>
                                                <th>@lang('admin.message271')</th>
                                                <th>@lang('admin.message264')</th>
                                                <th>@lang('admin.message265')</th>
                                                <th>@lang('admin.message266')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($wallet_transactions as $wallet_transaction)
                                                <tr>
                                                    <td>
                                                        @if($wallet_transaction->type == 1)
                                                            @lang('admin.message262')
                                                        @else
                                                            @lang('admin.message263')
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($wallet_transaction->payment_method == 1)
                                                            @lang('admin.message269')
                                                        @else
                                                            @lang('admin.message270')
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{$wallet_transaction->receipt_number}}
                                                    </td>
                                                    <td>
                                                        {{ $wallet_transaction->amount }}
                                                    </td>
                                                    <td>
                                                        @if($wallet_transaction->type == 1)
                                                            @lang('admin.message267')
                                                        @else
                                                            @lang('admin.message268'){{ $wallet_transaction->booking_id }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ $wallet_transaction->created_at }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1">{{ $wallet_transactions->links() }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection