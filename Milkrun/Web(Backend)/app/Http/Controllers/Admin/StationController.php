<?php

namespace App\Http\Controllers\Admin;

use App\Models\Station;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stations = Station::latest()->paginate(25);
        return view('admin.stations.index', compact('stations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.stations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'station_name' => "required",
            'station_address' => "required",
            'station_address' => ['required', 'max:255', 'unique:stations,station_address'],
        ]);
        $station = new Station();
        $station = Station::create([
            'station_name' => $request->station_name,
            'station_address' => $request->station_address,
            'station_lat' => '0.0',
            'station_long' => '0.0',
            'station_status' => '1',

        ]);
        return redirect()->back()->with('stationadded', 'Station Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Station  $station
     * @return \Illuminate\Http\Response
     */
    public function show(Station $station)
    {
        $user = Retailer::findOrFail($id);
        $bookings = Booking::where([['user_id', '=', $id]])->whereIn('booking_status', [1005])->paginate(5);
        return view('admin.retailer.show', compact('user', 'bookings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Station  $station
     * @return \Illuminate\Http\Response
     */
    public function edit(Station $station)
    {
        $retailer = Retailer::findOrFail($id);
        return view('admin.retailer.edit', compact('retailer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Station  $station
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Station $station)
    {
        $request->validate([
            'retailer_shop' => "required",
            'retailer_contact_name' => "required",
            'retailer_email' => ['required', 'max:255', 'unique:retailers,retailer_email'],
            'retailer_phone' => ['required', 'max:255', 'unique:retailers,retailer_phone'],
            'retailer_password' => 'required_if:edit_password,1',
            'retailer_shop_address' => "required"
        ]);
        $retailer = Retailer::findOrFail($id);
        $retailer->retailer_shop = $request->retailer_shop;
        $retailer->retailer_contact_name = $request->retailer_contact_name;
        $retailer->retailer_email = $request->retailer_email;
        $retailer->retailer_shop_address = $request->retailer_shop_address;
        if ($request->retailer_password == 1) {
            $password = Hash::make($request->retailer_password);
            $retailer->retailer_password = $password;
        }
        if ($request->hasFile('profile')) {
            $request->file('profile');
            $profile_image = $request->profile->store('user', 'public');
            $retailer->retailer_image = $profile_image;
        }
        $retailer->save();
        return redirect()->back()->with('rideradded', 'Rider Added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Station  $station
     * @return \Illuminate\Http\Response
     */
    public function destroy(Station $station)
    {
        //
    }
}
