<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.AddDriver'); ?></h3>

                </div>
                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <a href="<?php echo e(route('drivers.index')); ?>">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            
                
            
            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" enctype="multipart/form-data"
                                              class="steps-validation wizard-notification"
                                              enctype="multipart/form-data" action="<?php echo e(route('drivers.store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <fieldset>
                                                <div class="row">


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="driver_name">
                                                                <?php echo app('translator')->getFromJson('admin.fullname'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="driver_name"
                                                                   name="driver_name"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.fullname'); ?>" required>
                                                            <?php if($errors->has('driver_name')): ?>
                                                                <label class="danger"><?php echo e($errors->first('fullname')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="driver_email">
                                                                <?php echo app('translator')->getFromJson('admin.email'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="email" class="form-control required" id="driver_email"
                                                                   name="driver_email" placeholder="<?php echo app('translator')->getFromJson('admin.email'); ?>"
                                                                   required>
                                                            <?php if($errors->has('driver_email')): ?>
                                                                <label class="danger"><?php echo e($errors->first('driver_email')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="location3"><?php echo app('translator')->getFromJson('admin.melvin_retailer_create_message7'); ?> :</label>
                                                            <select class="form-control" name="country" id="country"
                                                                    required>
                                                                <option value=""><?php echo app('translator')->getFromJson('admin.melvin_retailer_create_message8'); ?></option>
                                                                <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option data-min=""
                                                                            data-max=""
                                                                            value="<?php echo e($country->phonecode); ?>"><?php echo e($country->name); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                            <?php if($errors->has('country')): ?>
                                                                <label
                                                                    class="danger"><?php echo e($errors->first('country')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="driver_phone">
                                                                <?php echo app('translator')->getFromJson('admin.merchantPhone'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="driver_phone"
                                                                   name="driver_phone"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.merchantPhone'); ?>" required>
                                                            <?php if($errors->has('driver_phone')): ?>
                                                                <label class="danger"><?php echo e($errors->first('driver_phone')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="driver_password">
                                                                <?php echo app('translator')->getFromJson('admin.password'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="password" class="form-control" id="driver_password"
                                                                   name="driver_password"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.password'); ?>" required>
                                                            <?php if($errors->has('driver_password')): ?>
                                                                <label class="danger"><?php echo e($errors->first('driver_password')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="location">
                                                                <?php echo e(__('Confirm Password')); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="password" class="form-control"
                                                                   id="password_confirmation"
                                                                   name="password_confirmation"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.password'); ?>" required>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                <?php echo app('translator')->getFromJson('admin.message227'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="file" class="form-control" id="image"
                                                                   name="image" required>
                                                            <?php if($errors->has('image')): ?>
                                                                <label class="danger"><?php echo e($errors->first('image')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>



                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="button" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>