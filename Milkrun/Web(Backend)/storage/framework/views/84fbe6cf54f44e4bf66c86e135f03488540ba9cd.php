<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.melvin_customer_add_message1'); ?></h3>
                </div>
                <div class="col-md-6 col-12">
                    <?php if(session('customeradded')): ?>
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo app('translator')->getFromJson('admin.melvin_customer_add_message9'); ?></strong>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="<?php echo e(route('retailer.index')); ?>">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            
            
            

            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data" action="<?php echo e(route('customers.store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <fieldset>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label
                                                                for="location3"><?php echo app('translator')->getFromJson('admin.melvin_customer_add_message2'); ?>
                                                                :</label>
                                                            <select class="form-control" name="retailer_id"
                                                                    id="retailer_id"
                                                                    required>
                                                                <option
                                                                    value=""><?php echo app('translator')->getFromJson('admin.melvin_customer_add_message3'); ?></option>
                                                                <?php $__currentLoopData = $retailers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $retailer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option data-min=""
                                                                            data-max=""
                                                                            value="<?php echo e($retailer->id); ?>"><?php echo e($retailer->retailer_shop); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                            <?php if($errors->has('retailer_id')): ?>
                                                                <label
                                                                    class="danger"><?php echo e($errors->first('retailer_id')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_customer_add_message4'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   id="customer_name"
                                                                   name="customer_name"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_customer_add_message4'); ?>"
                                                                   required>
                                                            <?php if($errors->has('customer_name')): ?>
                                                                <label
                                                                    class="danger"><?php echo e($errors->first('customer_name')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_email">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_customer_add_message5'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="email" class="form-control" id="customer_email"
                                                                   name="customer_email"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_customer_add_message5'); ?>"
                                                                   required>
                                                            <?php if($errors->has('customer_email')): ?>
                                                                <label
                                                                    class="danger"><?php echo e($errors->first('customer_email')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_address">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_customer_add_message6'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   id="customer_address"
                                                                   name="customer_address"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_customer_add_message6'); ?>"
                                                                   required>
                                                            <?php if($errors->has('customer_address')): ?>
                                                                <label
                                                                    class="danger"><?php echo e($errors->first('customer_address')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>



                                                </div>


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label
                                                                for="location3"><?php echo app('translator')->getFromJson('admin.melvin_retailer_create_message7'); ?>
                                                                :</label>
                                                            <select class="form-control" name="country" id="country"
                                                                    required>
                                                                <option
                                                                    value=""><?php echo app('translator')->getFromJson('admin.melvin_retailer_create_message8'); ?></option>
                                                                <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option data-min=""
                                                                            data-max=""
                                                                            value="<?php echo e($country->phonecode); ?>"><?php echo e($country->name); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                            <?php if($errors->has('country')): ?>
                                                                <label
                                                                    class="danger"><?php echo e($errors->first('country')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="lastName3">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_customer_add_message7'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="customer_phone"
                                                                   name="customer_phone"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_customer_add_message7'); ?>"
                                                                   required>
                                                            <?php if($errors->has('customer_phone')): ?>
                                                                <label
                                                                    class="danger"><?php echo e($errors->first('customer_phone')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ProfileImage">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_retailer_create_message10'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="file" class="form-control" id="profile"
                                                                   name="profile"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_retailer_create_message10'); ?>"
                                                                   required>
                                                            <?php if($errors->has('profile')): ?>
                                                                <label
                                                                    class="danger"><?php echo e($errors->first('profile')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_password">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_customer_add_message8'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="password" class="form-control" id="customer_password"
                                                                   name="customer_password"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_customer_add_message8'); ?>"
                                                                   required>
                                                            <?php if($errors->has('customer_password')): ?>
                                                                <label
                                                                    class="danger"><?php echo e($errors->first('customer_password')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>


                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> <?php echo app('translator')->getFromJson('admin.melvin_retailer_create_message13'); ?>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script>
        function RideType(val) {
            if (val == "1") {
                document.getElementById('corporate_div').style.display = 'block';
            } else {
                document.getElementById('corporate_div').style.display = 'none';
            }
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>