<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $guarded = [];


    public function findForPassport($user_cred = null)
    {

        return User::where([['UserPhone', '=', $user_cred], ['UserStatus', '=', 1]])->orWhere([['social_id', '=', $user_cred], ['UserStatus', '=', 1]])->first();
    }

    public function GenrateReferCode()
    {
        $code = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 5);
        $referCode = strtoupper($code);
        if ($this->CheckReferCode($referCode)) {
            return $this->GenrateReferCode();
        }
        return $referCode;
    }

    public function CheckReferCode($referCode)
    {
        return static::where([['ReferralCode', '=', $referCode]])->exists();
    }
}
