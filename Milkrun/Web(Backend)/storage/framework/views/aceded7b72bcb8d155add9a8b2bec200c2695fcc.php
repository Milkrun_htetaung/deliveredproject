<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.DriverManagement'); ?></h3>
                </div>
                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            
                                
                                    
                                    
                                

                            
                            <a href="<?php echo e(route('drivers.create')); ?>">
                                <button type="button" class="btn btn-icon btn-success mr-1" title="<?php echo app('translator')->getFromJson('admin.AddDriver'); ?>"><i class="fa fa-plus"></i>
                                </button>
                            <!--<button class="btn btn-primary btn-sm"><i
                                            class="ft-plus white"></i> <?php echo app('translator')->getFromJson('admin.AddDriver'); ?>
                                    </button>-->
                            </a>

                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="horizontal">
                    <div class="row">
                        <?php if(session('moneyAdded')): ?>
                            <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                                <span class="alert-icon"><i class="fa fa-info"></i></span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong><?php echo app('translator')->getFromJson('admin.message207'); ?></strong>
                            </div>
                        <?php endif; ?>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th><?php echo app('translator')->getFromJson('admin.driverId'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.fullname'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.email'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.merchantPhone'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message276'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message277'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message282'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message283'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message292'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.Registerdate'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.lastUpdate'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.Status'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.action'); ?></th>
                                            </tr>
                                            </thead>
                                            <?php $__currentLoopData = $drivers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $driver): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($driver->id); ?></td>
                                                    <td><?php echo e($driver->driver_name); ?></td>
                                                    <td><?php echo e($driver->driver_email); ?></td>
                                                    <td><?php echo e($driver->driver_phone); ?></td>

                                                    <td>
                                                        <?php if($driver->rating == "0.0"): ?>
                                                            <?php echo app('translator')->getFromJson('admin.message278'); ?>
                                                        <?php else: ?>
                                                            <?php while($driver->rating>0): ?>
                                                                <?php if($driver->rating >0.5): ?>
                                                                    <img src="<?php echo e(asset("star.png")); ?>"
                                                                         alt='Whole Star'>
                                                                <?php else: ?>
                                                                    <img src="<?php echo e(asset('halfstar.png')); ?>"
                                                                         alt='Half Star'>
                                                                <?php endif; ?>
                                                                <?php $driver->rating--; ?>
                                                            <?php endwhile; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php if($driver->total_trips): ?>
                                                            <?php echo e($driver->total_trips); ?>

                                                        <?php else: ?>
                                                            <?php echo app('translator')->getFromJson('admin.message279'); ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php if($driver->total_earnings): ?>
                                                            <?php echo e($driver->total_earnings); ?>

                                                        <?php else: ?>
                                                            <?php echo app('translator')->getFromJson('admin.message279'); ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php if($driver->total_comany_earning): ?>
                                                            <?php echo e($driver->total_comany_earning); ?>

                                                        <?php else: ?>
                                                            <?php echo app('translator')->getFromJson('admin.message279'); ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td><a href=""><?php echo e($driver->wallet_money); ?></a></td>
                                                    <td><?php echo e($driver->created_at); ?></td>
                                                    <td><?php echo e($driver->updated_at); ?></td>
                                                    <td>
                                                        <?php if($driver->login_logout == 1): ?>
                                                            <?php if($driver->online_offline == 1): ?>
                                                                <label class="label_success"
                                                                       style="width:48px;display: inline-block;"> <?php echo app('translator')->getFromJson('admin.message314'); ?></label>
                                                            <?php else: ?>
                                                                <label class="label_info"
                                                                       style="width:48px;display: inline-block;"> <?php echo app('translator')->getFromJson('admin.message315'); ?></label>
                                                            <?php endif; ?>
                                                        <?php else: ?>
                                                            <label class="label_danger"><?php echo app('translator')->getFromJson('admin.message313'); ?></label>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <span data-target="#sendNotificationModel"
                                                              data-toggle="modal" id="<?php echo e($driver->id); ?>"><a
                                                                    data-original-title="Send Notification"
                                                                    data-toggle="tooltip"
                                                                    id="<?php echo e($driver->id); ?>" data-placement="top"
                                                                    class="btn menu-icon btn_eye action_btn"> <i
                                                                        class="fa fa-bell"></i> </a></span>

                                                        <span data-target="#addMoneyModel"
                                                              data-toggle="modal" id="<?php echo e($driver->id); ?>"><a
                                                                    data-original-title="Add Money"
                                                                    data-toggle="tooltip"
                                                                    id="<?php echo e($driver->id); ?>" data-placement="top"
                                                                    class="btn menu-icon btn_detail action_btn"> <i
                                                                        class="fa fa-money"></i> </a></span>

                                                        <a href=" "
                                                           class="btn menu-icon btn_money action_btn"><span
                                                                    class="fa fa-history"
                                                                    title="Driver Wallet"></span></a>

                                                        <a href=""
                                                           class="btn menu-icon btn_detail action_btn"><span
                                                                    class="fa fa-list-alt" title="View Driver Profile"></span></a>


                                                        <?php if($driver->driver_admin_status == 1): ?>
                                                            <a href=""
                                                               data-original-title="Inactive"
                                                               data-toggle="tooltip" data-placement="top"
                                                               class="btn menu-icon btn_eye_dis action_btn"> <i
                                                                        class="fa fa-eye-slash"></i> </a>
                                                        <?php else: ?>
                                                            <a href=""
                                                               data-original-title="Active"
                                                               data-toggle="tooltip" data-placement="top"
                                                               class="btn menu-icon btn_eye action_btn"> <i class="fa fa-eye"></i>
                                                            </a>
                                                        <?php endif; ?>
                                                        <?php if($driver->login_logout == 1): ?>
                                                            <a href=""
                                                               data-original-title="Logout"
                                                               data-toggle="tooltip" data-placement="top"
                                                               class="btn menu-icon btn_delete action_btn"> <i
                                                                        class="fa fa-sign-out"></i>
                                                            </a>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1"><?php echo e($drivers->links()); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    
         
         
        
            
                
                    
                    
                        
                    
                
                
                    
                    
                        
                        
                            
                                   
                                   
                        

                        
                        
                           
                                     
                                     
                        

                        
                        
                            
                                   
                                   
                            
                        

                        
                        
                            
                                   
                                   
                        
                    
                    
                        
                        
                    
                
            
        
    

    
         
        
            
                
                    
                    
                        
                    
                
                
                    
                    
                        
                        
                            
                                
                                
                            
                        

                        
                        
                            
                                   
                        

                        
                        
                            
                                   
                            
                        

                        
                        
                            
                                      
                        
                    
                    
                        
                        
                    
                
            
        
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>