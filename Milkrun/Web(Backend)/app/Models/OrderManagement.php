<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderManagement extends Model
{
    protected $guarded = [];

    public function Customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function Driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function Retailer()
    {
        return $this->belongsTo(Retailer::class);
    }
}
