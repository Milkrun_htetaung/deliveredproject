$(document).ready(function () {

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: true                 // set focus to editable area after initializing summernote
    });
    $('.mytimepicker').clockTimePicker();
    $('#resetForm').on('click', function () {
        window.location.reload(true);
    });
    $('#sendNotificationModel').on('show.bs.modal', function (e) {
        let $modal = $(this),
            esseyId = e.relatedTarget.id;
        $modal.find('#persion_id').val(esseyId);
    });
    $('#sendNotificationModelUser').on('show.bs.modal', function (e) {
        let $modal = $(this),
            esseyId = e.relatedTarget.id;
        $modal.find('#persion_id').val(esseyId);
    });
    $('#cancelbooking').on('show.bs.modal', function (e) {
        let $modal = $(this),
            esseyId = e.relatedTarget.id;
        $modal.find('#booking_id').val(esseyId);
    });

    $('#addMoneyModel').on('show.bs.modal', function (e) {
        let $modal = $(this),
            esseyId = e.relatedTarget.id;
        $modal.find('#add_money_driver_id').val(esseyId);
    });

    $("#removeButton").on("click", function () {
        $(this).parent().parent().remove();
    });
    $('.scroll-horizontal').DataTable({
        "scrollX": true,
        "searching": false,
        paging: false,
    });

    $('.scroll-horizontalSecond').DataTable({
        "scrollX": true,
        "searching": false,
        paging: false,
    });
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function (date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    });

    $('.datepickersearch').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function (date) {
            return date.valueOf() > now.valueOf() ? 'disabled' : '';
        }
    });

    $(".openPopup").click(function () {
        var data = $(this).attr('data-href');
        let data1 = JSON.parse(data);
        var table = document.createElement('table');
        table.className = "table display nowrap table-striped table-bordered scroll-horizontal";
        for (var i in data1) {
            var parameter = data1[i].parameter;
            var amount = data1[i].amount;
            var tr = document.createElement('tr');
            var td1 = document.createElement('th');
            var td2 = document.createElement('td');
            var text1 = document.createTextNode(parameter);
            var text2 = document.createTextNode(amount);
            td1.appendChild(text1);
            td2.appendChild(text2);
            tr.appendChild(td1);
            tr.appendChild(td2);

            table.appendChild(tr);
        }
        $('#detailBooking').modal({show: true});
        $('#detailBooking').find('.modal-body').html(table);
    });

    $(".select2").select2();

    $('select[name=first_logic]').on('change', function () {
        var self = this;
        $('select[name=second_logic]').find('option').prop('disabled', function () {
            return this.value == self.value
        });
    });

    $('#country').on('change', function () {
        var max = $('option:selected', this).attr('data-max');
        var min = $('option:selected', this).attr('data-min');
        $("#user_phone").attr({'maxlength': max});
        $("#user_phone").attr({'minlength': min});

    });
    $('select[name=second_logic]').on('change', function () {
        var firstVal = $('#first_logic').find(":selected").attr("id");
        var secondval = $('#second_logic').find(":selected").attr("id");
        $("select#third_logic option").prop('disabled', false).filter("#" + firstVal + " ,#" + secondval).prop('disabled', true);
    });


    $('select[name=third_logic]').on('change', function () {
        var firstVal = $('#first_logic').find(":selected").attr("id");
        var secondval = $('#second_logic').find(":selected").attr("id");
        var thirdval = $('#third_logic').find(":selected").attr("id");
        $("select#fourth_logic option").prop('disabled', false).filter("#" + firstVal + " ,#" + secondval + ",#" + thirdval).prop('disabled', true);
    });

    $('#check_user_details').click(function () {
        var user_phone = $('[name="user_phone"]').val();
        var country = $('[name="country"]').val();
        if (user_phone == "") {
            alert('Enter Phone Number')
            return false;
        } else {
            $("#loader1").show();
            var token = $('[name="_token"]').val();
            let number = country + user_phone;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'POST',
                url: "SearchUser",
                data: {user_phone: number},
                success: function (data) {
                    $('#user_id').val(data);
                    $("#full_details").attr("href", "users/" + data);
                    $("#full_details_div").show();
                }, error: function (err) {
                    $('#user_id').val('');
                    alert("This Phone Number Is Not Registered")
                    $("#full_details").removeAttr("href");
                    $("#full_details_div").hide();
                }
            });
            $("#loader1").hide();
        }
    });

    $("#ride_radius_driver").on('change', function () {
        var radius = this.value;
        var pickup_latitude = $('[name="pickup_latitude"]').val();
        var manual_area = $('[name="area"]').val();
        var pickup_longitude = $('[name="pickup_longitude"]').val();
        var drop_latitude = $('[name="drop_latitude"]').val();
        var drop_longitude = $('[name="drop_longitude"]').val();
        var service = $('[name="service"]').val();
        var vehicle_type = $('[name="vehicle_type"]').val();
        if (pickup_latitude == "" && drop_latitude == "") {
            alert("Enter Pickup And Drop Location");
            $('#ride_radius_driver').prop('selectedIndex', 0);
            return false;
        } else {
            var token = $('[name="_token"]').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'POST',
                url: "checkDriver",
                data: {
                    manual_area: manual_area,
                    radius: radius,
                    pickup_latitude: pickup_latitude,
                    pickup_longitude: pickup_longitude,
                    drop_latitude: drop_latitude,
                    drop_longitude: drop_longitude,
                    service: service,
                    vehicle_type: vehicle_type,
                },
                success: function (data) {
                    $('#number_of_drivers').text(data)
                }
            });
        }
    });

    $("#check_ride_estimate").click(function () {
        var area = $('[name="area"]').val();
        var distance = $('[name="distance"]').val();
        var ride_time = $('[name="ride_time"]').val();
        var service = $('[name="service"]').val();
        var vehicle_type = $('[name="vehicle_type"]').val();
        if (distance == "" || ride_time == "" || service == "" || vehicle_type == "" || area == "") {
            alert("Required Field Missing");
        } else {
            var token = $('[name="_token"]').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'POST',
                url: "estimatePrice",
                data: {
                    distance: distance,
                    ride_time: ride_time,
                    service: service,
                    vehicle_type: vehicle_type,
                    area: area
                },
                success: function (data) {
                    var estimate = "Fare Estimate: " + data;
                    $('#estimate_fare_ride').html(estimate);
                    $('#estimate_fare').val(data);
                }
            });
        }
    });

    $("#manual_area").on('change', function () {
        var area_id = this.value;
        if (area_id != "") {
            var token = $('[name="_token"]').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'POST',
                url: "getServices",
                data: {area_id: area_id},
                success: function (data) {
                    $("#service").html(data);
                }
            });
        }
    });
    $('#manual_user_type').on('change', function () {
        var user = this.value;
        if (user == 1) {
            $("#corporate_div").hide();
            $("#email_new_div").show();
            $("#name_new_div").show();
            $("#new_user_details_div").show();
            $("#check_user_details_div").hide();
            $("#full_details_div").hide();
        } else if (user == 2) {
            $("#corporate_div").hide();
            $("#email_new_div").hide();
            $("#name_new_div").hide();
            $("#new_user_details_div").hide();
            $("#check_user_details_div").show();
            $("#full_details_div").hide();
        } else {
            $("#corporate_div").show();
            $("#email_new_div").hide();
            $("#name_new_div").hide();
            $("#new_user_details_div").hide();
            $("#check_user_details_div").show();
            $("#full_details_div").hide();
        }
    });

    $("#new_user_details").click(function () {
        var country = $('[name="country"]').val();
        var new_user_name = $('[name="new_user_name"]').val();
        var new_user_phone = $('[name="user_phone"]').val();
        var new_user_email = $('[name="new_user_email"]').val();
        if (new_user_name === "" || new_user_phone === "" || new_user_email === "") {
            alert("Enter Rider Details");
            return false;
        } else {
            $("#loader1").show();
            var token = $('[name="_token"]').val();
            let fullNumber = country + new_user_phone;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'POST',
                url: "AddManualUser",
                data: {new_user_name: new_user_name, new_user_phone: fullNumber, new_user_email: new_user_email},
                success: function (data) {
                    $('#user_id').val(data);
                    $("#new_user_details").prop('disabled', true);
                    alert("Rider Register Successfully");
                }, error: function (err) {
                    $('#user_id').val('');
                    var errors = err.responseJSON;
                    alert(errors.message);
                }
            });
            $("#loader1").hide();
        }
    });


    $('#all_driver_booking').click(function () {
        $("#ride_radius_driver").prop('disabled', false);
        $("#driver_id").prop('disabled', true);
        $("#favourite_list").prop('disabled', true);
    });

    $('#favourite_driver').click(function () {
        $("#ride_radius_driver").prop('disabled', true);
        $("#driver_id").prop('disabled', true);
        $("#favourite_list").prop('disabled', false);

        var area = $('[name="area"]').val();
        var service = $('[name="service"]').val();
        var vehicle_type = $('[name="vehicle_type"]').val();
        var user_id = $('[name="user_id"]').val();
        var pickup_latitude = $('[name="pickup_latitude"]').val();
        var pickup_longitude = $('[name="pickup_longitude"]').val();
        if (pickup_latitude == "" && pickup_longitude == "") {
            alert("Enter Pickup Location");
            return false;
        } else {
            var token = $('[name="_token"]').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'POST',
                url: "getFavouriteDriver",
                data: {
                    manual_area: area,
                    service: service,
                    vehicle_type: vehicle_type,
                    user_id: user_id,
                    pickup_latitude: pickup_latitude,
                    pickup_longitude: pickup_longitude,
                },
                success: function (data) {
                    $('#favourite_list').html(data)
                }
            });
        }

    });
    $('#package').on("change", function () {
        var service = $('#service').val();
        var manual_area = $('#manual_area').val();
        var package_id = $('#package').val();
        var token = $('[name="_token"]').val();
        if (package_id != "") {
            $("#loader1").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'POST',
                url: "getVehicle",
                data: {
                    package_id: package_id,
                    service: service,
                    area: manual_area,
                },
                success: function (data) {
                    if (data) {
                        $("#vehicle_type_id").show();
                        $("#vehicle_type").html(data);
                    } else {
                        alert("No Price Card For This Package");
                    }
                }
            });
            $("#loader1").hide();
        }
    });
    $('#service').on("change", function () {
        var service = this.value;
        var user_id = $('#user_id').val();
        var manual_area = $('#manual_area').val();
        if (user_id == "") {
            $("#vehicle_type_id").hide();
            alert("User information is not found");
            $('#service').prop('selectedIndex', 0);
            return false;
        } else {
            var token = $('[name="_token"]').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'POST',
                url: "getRideConfig",
                data: {
                    user_id: user_id,
                    service: service,
                    manual_area: manual_area,
                },
                success: function (data) {
                    if (service == 1) {
                        $("#package_id").hide();
                        $("#vehicle_type_id").show();
                        $("#vehicle_type").html(data);
                    } else {
                        $("#vehicle_type_id").hide();
                        $("#package_id").show();
                        $("#package").html(data);
                    }
                }
            });
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'POST',
                url: "getPromoCode",
                data: {
                    user_id: user_id,
                    service: service,
                    manual_area: manual_area,
                },
                success: function (data) {
                    $('#promo_code').html(data);
                }
            });
        }
    });
    $('#manually_driver').click(function () {
        $("#ride_radius_driver").prop('disabled', true);
        $("#driver_id").prop('disabled', false);
        $("#favourite_list").prop('disabled', true);
        var pickup_latitude = $('[name="pickup_latitude"]').val();
        var manual_area = $('[name="area"]').val();
        var pickup_longitude = $('[name="pickup_longitude"]').val();
        var drop_latitude = $('[name="drop_latitude"]').val();
        var drop_longitude = $('[name="drop_longitude"]').val();
        var service = $('[name="service"]').val();
        var vehicle_type = $('[name="vehicle_type"]').val();
        if (pickup_latitude == "" && pickup_longitude == "" && service == "" && vehicle_type == "") {
            alert("Required Field Missing")
            return false;
        } else {
            var token = $('[name="_token"]').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'POST',
                url: "getallDriver",
                data: {
                    manual_area: manual_area,
                    pickup_latitude: pickup_latitude,
                    pickup_longitude: pickup_longitude,
                    drop_latitude: drop_latitude,
                    drop_longitude: drop_longitude,
                    service: service,
                    vehicle_type: vehicle_type,
                },
                success: function (data) {
                    $('#driver_id').html(data)
                }
            });
        }

    });


    // $("#multi_step_form").validate();
    // $('#first_step').click(function () {
    //     var formValida = $("#multi_step_form").valid();
    //     if (formValida == true) {
    //         var token = $('[name="_token"]').val();
    //         var fullname = $('[name="fullname"]').val();
    //         var email = $('[name="email"]').val();
    //         var phone = $('[name="phone"]').val();
    //         var password = $('[name="password"]').val();
    //         var area = $('[name="area"]').val();
    //
    //         $.ajax({
    //             headers: {
    //                         'X-CSRF-TOKEN': token
    //                      },
    //             method:'POST',
    //             url:"firststep",
    //             data: {fullname: fullname,email:email,phone:phone,password:password,area:area},
    //             success:function(data){
    //                // $("#second_step_input").html(data);
    //                 $("#first_step_div").hide();
    //                 $("#second_step_div").show();
    //                 $('#third_step_div').hide();
    //                 $("#second_step_input").after(data);
    //             },error:function(err){
    //                 var errors = err.responseJSON;
    //                 alert(errors.message);
    //             }
    //         });
    //     }
    // });

    // $('#second_step').click(function(){
    //     var formValida = $("#multi_step_form").valid();
    //     if(formValida == true){
    //         var token = $('[name="_token"]').val();
    //         var values = $("input[name='documents[]']").map(function(){return $(this).val();}).get();
    //         alert(values);
    //         // var image = $("input[name='documents[]']")[0].files[0];
    //         // var driver_id = $("input[name='driver_id']").val();
    //         // // console.log(values);
    //         // var form = new FormData();
    //         // form.append('id', driver_id);
    //         // form.append('image', image);
    //         // $.ajax({
    //         //     headers: {
    //         //         'X-CSRF-TOKEN': token
    //         //     },
    //         //     method:'POST',
    //         //     url:"secondstep",
    //         //    // data: $('#multi_step_form').serialize(),
    //         //     data: form,
    //         //     cache:false,
    //         //     contentType: false,
    //         //     processData: false,
    //         //     success:function(data){
    //         //        console.log(data);
    //         //     },error:function(err){
    //         //         console.log(err);
    //         //     }
    //         // });
    //     }
    //
    // });
    var url = window.location;
    $('ul#main-menu-navigation a').filter(function () {
        return this.href == url;
        var k = 5;
    }).parent().addClass('active');
    $('ul#main-menu-navigation a').filter(function () {
        return this.href == url;
    }).parentsUntil("#main-menu-navigation > .treeview-menu").addClass('open');

    /*-------------------Other One -----------------------*/
    var a = window.location.pathname;
    $('#main-menu-navigation > li > a[href="http://localhost' + a + '"').parent().addClass("active");
    $('#main-menu-navigation ul li').find('a').each(function () {
        var link = new RegExp($(this).attr('href')); //Check if some menu compares inside your the browsers link
        if (link.test(document.location.href)) {
            if (!$(this).parents().hasClass('active')) {
                $(this).parents('li').addClass('open');
                $(this).parents().addClass("open");
                $(this).addClass("active"); //Add this too
            }
        }
    });
});