package com.milkrun.delivered;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import com.google.android.material.navigation.NavigationView;
import com.milkrun.delivered.ui.helpsup.HelpAndSupportActivity;
import com.milkrun.delivered.ui.message.MessageActivity;
import com.milkrun.delivered.ui.profile.ProfileActivity;

import static android.view.Gravity.LEFT;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected   DrawerLayout fullLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.w("IN Drawer","acitity");


        setUpDrawerLayout(R.layout.app_bar_drawer);
        setContentView(fullLayout);

//        setContentView(R.layout.activity_drawer);
    }

    @Override
    public void onBackPressed() {
        if (fullLayout.isDrawerOpen(GravityCompat.START)) {
            fullLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    protected void setUpDrawerLayout(@LayoutRes int layoutRes)
    {
        fullLayout = new DrawerLayout(this);
        fullLayout.setLayoutParams(new DrawerLayout.LayoutParams(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.MATCH_PARENT));
        fullLayout.setFitsSystemWindows(true);




//        LinearLayout.LayoutParams tParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent,0, 10);
//        toolbar.LayoutParameters = tParams;
//        toolbar.SetBackgroundColor(Color.Gray);
//        toolbar.SetMinimumHeight(50);

//        FrameLayout activityContainer = new FrameLayout(this);
//
//        activityContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,0,90));
//
//        relLayout.addView(activityContainer);


        fullLayout.addView(LayoutInflater.from(this).inflate(layoutRes,null));

        NavigationView navigationView = new NavigationView(this);
        DrawerLayout.LayoutParams navLayout = new DrawerLayout.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.MATCH_PARENT, GravityCompat.START);

        navLayout.gravity=LEFT;
        navigationView.setLayoutParams(navLayout);
        navigationView.setFitsSystemWindows(true);

        View navHeader=LayoutInflater.from(this).inflate(R.layout.nav_header_drawer,null);
        navHeader.setLayoutParams(new DrawerLayout.LayoutParams(DrawerLayout.LayoutParams.MATCH_PARENT,convertDipToPixels(250)));

        navigationView.addHeaderView(navHeader);

        // FOR NAVIGATION VIEW ITEM TEXT COLOR
        int[][] states = new int[][]{
                new int[]{-android.R.attr.state_checked},  // unchecked
                new int[]{android.R.attr.state_checked},   // checked
                new int[]{}                                // default
        };

// Fill in color corresponding to state defined in state
        int[] colors = new int[]{
                Color.parseColor("#714293"),
                Color.parseColor("#714293"),
                Color.parseColor("#714293"),
        };
        ColorStateList navigationViewColorStateList = new ColorStateList(states, colors);

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(navigationViewColorStateList);
        navigationView.setItemTextColor(navigationViewColorStateList);

        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.activity_drawer_drawer,navigationView.getMenu());

        fullLayout.addView(navigationView);


    }
    public int convertDipToPixels(float dips)
    {
        return (int) (dips * this.getResources().getDisplayMetrics().density + 0.5f);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {


        fullLayout.closeDrawer(GravityCompat.START);

        if(item.getItemId()==R.id.nav_help_support){
            startActivity(new Intent(this, HelpAndSupportActivity.class));
        }

        if(item.getItemId()==R.id.nav_message){
            startActivity(new Intent(this, MessageActivity.class));
        }


        if(item.getItemId()==R.id.nav_profile){
            startActivity(new Intent(this, ProfileActivity.class));
        }

        return true;
    }
}
