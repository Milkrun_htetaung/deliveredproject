<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('driver_name');
            $table->string('driver_email')->unique();
            $table->string('driver_phone')->unique();
            $table->string('driver_password');
            $table->string('driver_image')->nullable();
            $table->string('current_lat')->default(0.0);
            $table->string('current_long')->default(0.0);
            $table->string( 'wallet_money' )->default( 0 );
            $table->string( 'rating' )->default( 0 );
            $table->string( 'player_id' )->default( 0 );
            $table->unsignedInteger( 'total_trips' )->default( 0 );
            $table->unsignedInteger( 'total_earnings' )->default( 0 );
            $table->tinyInteger( 'login_type' )->default( 0 );
            $table->tinyInteger( 'login_logout' )->default( 0 );
            $table->tinyInteger( 'online_offline' )->default( 0 );
            $table->tinyInteger( 'driver_status' )->default( 0 );
            $table->tinyInteger( 'driver_verify_status' )->default( 0 );
            $table->tinyInteger( 'driver_admin_status' )->default( 1 );
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
