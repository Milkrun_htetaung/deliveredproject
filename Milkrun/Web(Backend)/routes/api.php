<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['check_api_status']], function () {
    Route::post('/Send_Otp', 'API\Common_APIs\CommonController@Send_Otp');
    Route::post('/Country_Data', 'API\Common_APIs\CommonController@Country_Data');
    Route::post('/Register', 'API\Account_Module\AccountController@Register');
    Route::post('/Login', 'API\Account_Module\AccountController@Login');

});

/****** Retailer Middleware ******/
Route::group(['middleware' => ['auth:api_retailer']], function () {
    Route::post('Update_Retailer_Playerid', 'API\PlayerIds\PlayerIdRetailer@Update_Player_Id');
    Route::post('View_Retailer_Profile', 'API\Profiles\Retailer\RetailerProfileController@View_Profile');
    Route::post('Edit_Retailer_Profile', 'API\Profiles\Retailer\RetailerProfileController@Edit_Profile');
    Route::post('Metro_Stations', 'API\MetroStation\MetroStationController@MetroStation_List');
    Route::post('Customer_List', 'API\RetailerAPI\CustomerController@RetailerCustomers');
    /***ORDER APIS***/
    Route::post('Post_Order', 'API\Order\OrderController@Post_Order');
    Route::post('OrderHistoryRetailer', 'API\Order\OrdeHistoryRetailer@OrderHistory');
    Route::post('AddRetailerCustomer', 'API\RetailerAPI\CustomerController@AddRetailerCustomer');
});

/****** Driver Middleware ******/
Route::group(['middleware' => ['auth:api_driver']], function () {
    Route::post('Update_Driver_Playerid', 'API\PlayerIds\PlayerIdDriver@Update_Player_Id');
    Route::post('View_Driver_Profile', 'API\Profiles\Driver\DriverProfileController@View_Profile');
    Route::post('Edit_Driver_Profile', 'API\Profiles\Driver\DriverProfileController@Edit_Profile');
    Route::post('OrderAccept', 'API\Order\OrderController@OrderAccept');
    Route::post('OrderPicked', 'API\Order\OrderController@OrderPicked');
    Route::post('OrderOnWay', 'API\Order\OrderController@OrderOnWay');
    Route::post('OrderEnd', 'API\Order\OrderController@OrderEnd');
    Route::post('OrderHistoryDriver', 'API\Order\OrdeHistoryDriver@OrderHistory');
    Route::post('RatingDriver', 'API\Profiles\Driver\DriverProfileController@Rating_Driver');
     

});

/****** Customer Middleware ******/
Route::group(['middleware' => ['auth:api_customer']], function () {
    Route::post('View_Customer_Profile', 'API\Profiles\Customer\CustomerProfileController@View_Profile');
    Route::post('Update_Customer_Playerid', 'API\PlayerIds\PlayerIdCustomer@Update_Player_Id');
    Route::post('OrderHistoryCustomer', 'API\Order\OrdeHistoryCustomer@OrderHistory');
    
});
