@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.melvin_retailer_message12')</h3>
                </div>

                <div class="col-md-6 col-12">
                    @if(session('moneyAdded'))
                        <div class="col-md-8 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>@lang('admin.melvin_retailer_message19')</strong>
                        </div>
                    @endif
                </div>


                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            <a href="{{route('retailer.create')}}">
                                <button type="button" class="btn btn-icon btn-success mr-1"><i
                                        class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="">
                                    <form action="{{ route('retailer.search') }}" method="post">
                                        @csrf
                                        <div class="table_search">
                                            <div class="col-md-2 col-xs-4 form-group ">
                                                <div class="input-group">
                                                    <select class="form-control" name="parameter"
                                                            id="parameter"
                                                            required>
                                                        <option value="1">@lang('admin.melvin_retailer_message2')</option>
                                                        <option value="2">@lang('admin.melvin_retailer_message4')</option>
                                                        <option value="3">@lang('admin.melvin_retailer_message5')</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-xs-6 form-group ">
                                                <div class="input-group">
                                                    <input type="text" name="keyword"
                                                           placeholder="@lang('admin.message337')"
                                                           class="form-control col-md-12 col-xs-12" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-2  col-xs-12 form-group ">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"
                                                                                                 aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table
                                            class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>@lang('admin.melvin_retailer_message1')</th>
                                                <th>@lang('admin.melvin_retailer_message2')</th>
                                                <th>@lang('admin.melvin_retailer_message3')</th>
                                                <th>@lang('admin.melvin_retailer_message6')</th>
                                                <th>@lang('admin.melvin_retailer_message5')</th>
                                                <th>@lang('admin.melvin_retailer_message4')</th>
                                                <th>@lang('admin.melvin_retailer_message9')</th>
                                                <th>@lang('admin.melvin_retailer_message13')</th>
                                                <th>@lang('admin.melvin_retailer_message8')</th>
                                                <th>@lang('admin.melvin_retailer_message14')</th>
                                                <th>@lang('admin.melvin_retailer_message15')</th>
                                                <th>@lang('admin.melvin_retailer_message17')</th>
                                                <th>@lang('admin.melvin_retailer_message18')</th>
                                                <th>@lang('admin.melvin_retailer_message10')</th>
                                                <th>@lang('admin.melvin_retailer_message11')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($retailers as $retailer)
                                                <tr>
                                                    <td>{{ $retailer->id }}</td>
                                                    <td>{{ $retailer->retailer_shop }}</td>
                                                    <td>{{ $retailer->retailer_contact_name }}</td>
                                                    <td>{{ $retailer->retailer_shop_address }}</td>
                                                    <td>{{ $retailer->retailer_phone }}</td>
                                                    <td>{{ $retailer->retailer_email }}</td>
                                                    <td>
                                                        @if ($retailer->rating == "0.0")
                                                            @lang('admin.message278')
                                                        @else
                                                            @while($retailer->rating>0)
                                                                @if($retailer->rating >0.5)
                                                                    <img src="{{ asset("star.png") }}"
                                                                         alt='Whole Star'>
                                                                @else
                                                                    <img src="{{ asset('halfstar.png') }}"
                                                                         alt='Half Star'>
                                                                @endif
                                                                @php $retailer->rating--; @endphp
                                                            @endwhile
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($retailer->total_trips)
                                                            {{ $retailer->total_trips }}
                                                        @else
                                                            @lang('admin.message279')
                                                        @endif
                                                    </td>
                                                    <td><a href="{{ route('retailer.wallet',$retailer->id) }}" j>
                                                            @if($retailer->wallet_balance)
                                                                {{ $retailer->wallet_balance }}
                                                            @else
                                                                0.00
                                                            @endif
                                                        </a>
                                                    </td>
                                                    <td>{{ $retailer->ReferralCode }}</td>

                                                    @switch($retailer->login_type)
                                                        @case(1)
                                                        <td>@lang('admin.application')</td>
                                                        @break
                                                        @case(2)
                                                        <td>@lang('admin.admin')</td>
                                                        @break
                                                        @case(3)
                                                        <td>@lang('admin.web')</td>
                                                        @break
                                                    @endswitch
                                                    <td>{{ $retailer->created_at->toformatteddatestring() }}</td>
                                                    <td>{{ $retailer->updated_at->toformatteddatestring() }}</td>
                                                    <td>
                                                        @if($retailer->retailer_status == 1)
                                                            <label class="label_success">@lang('admin.active')</label>
                                                        @else
                                                            <label class="label_danger">@lang('admin.deactive')</label>
                                                        @endif
                                                    </td>

                                                    <td>


                                                            <span data-target="#sendNotificationModelUser"
                                                                  data-toggle="modal" id="{{ $retailer->id }}"><a
                                                                    data-original-title="Send Notification"
                                                                    data-toggle="tooltip"
                                                                    id="{{ $retailer->id }}" data-placement="top"
                                                                    class="btn menu-icon btn_detail action_btn"> <i
                                                                        class="fa fa-bell"></i> </a></span>
                                                        <a href="{{ route('retailer.edit',$retailer->id) }}"
                                                           data-original-title="Edit" data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_edit action_btn"> <i
                                                                class="fa fa-pencil"></i> </a>

                                                        @if($retailer->retailer_status == 1)
                                                            <a href="{{ route('retailer.active-deactive',['id'=>$retailer->id,'status'=>2]) }}"
                                                               data-original-title="Inactive" data-toggle="tooltip"
                                                               data-placement="top"
                                                               class="btn menu-icon btn_eye_dis action_btn"> <i
                                                                    class="fa fa-eye-slash"></i> </a>
                                                        @else
                                                            <a href="{{ route('retailer.active-deactive',['id'=>$retailer->id,'status'=>1]) }}"
                                                               data-original-title="Active" data-toggle="tooltip"
                                                               data-placement="top"
                                                               class="btn menu-icon btn_eye action_btn"> <i
                                                                    class="fa fa-eye"></i> </a>
                                                        @endif
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1">{{ $retailers->links() }}</div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="sendNotificationModelUser" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel33"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33">@lang('admin.message299')</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="#" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="modal-body">
                        <label>@lang('admin.message63'): </label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="title"
                                   name="title"
                                   placeholder="@lang('admin.message63')" required>
                        </div>

                        <label>@lang('admin.message64'): </label>
                        <div class="form-group">
                           <textarea class="form-control" id="message" name="message"
                                     rows="3"
                                     placeholder="@lang('admin.message64')"></textarea>
                        </div>

                        <label>@lang('admin.message65'): </label>
                        <div class="form-group">
                            <input type="file" class="form-control" id="image"
                                   name="image"
                                   placeholder="@lang('admin.message65')" required>
                            <input type="hidden" name="persion_id" id="persion_id">
                        </div>

                        <label>@lang('admin.message66'): </label>
                        <div class="form-group">
                            <input type="url" class="form-control" id="url"
                                   name="url"
                                   placeholder="@lang('admin.message66')" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                        <input type="submit" class="btn btn-outline-primary btn-lg" value="Send">
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--add money in user wallet--}}
    <div class="modal fade text-left" id="addMoneyModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33">@lang('admin.message200')</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('retailer.add.wallet') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <label>@lang('admin.message203'): </label>
                        <div class="form-group">
                            <select class="form-control" name="payment_method" id="payment_method" required>
                                <option value="1">@lang('admin.message201')</option>
                                <option value="2">@lang('admin.message202')</option>
                            </select>
                        </div>

                        <label>@lang('admin.message205'): </label>
                        <div class="form-group">
                            <input type="text" name="amount" placeholder="@lang('admin.message205')"
                                   class="form-control" required>
                            <input type="hidden" name="add_money_user_id" id="add_money_driver_id">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                        <input type="submit" class="btn btn-outline-primary btn-lg" value="Add">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
