<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderManagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_managements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('driver_id');
            $table->string('retailer_id');
            $table->string('customer_id');
            $table->string('pickup_address');
            $table->string('dropoff_address');
            $table->string('size')->default(0);
            $table->string('quantity')->default(0);
            $table->string('weight')->default(0);
            $table->string('images')->default(0);
            $table->string('documents')->default(0);
            $table->string('delivery_charges')->default(0);
            $table->string('payment_id');
            $table->string('posting_time');
            $table->string('order_status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_managements');
    }
}
