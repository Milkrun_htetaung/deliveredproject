<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message1'); ?></h3>
                </div>
                <div class="col-md-6 col-12">
                    <?php if(session('rideradded')): ?>
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message2'); ?></strong>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="<?php echo e(route('retailer.index')); ?>">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data"
                                              action="<?php echo e(route('retailer.update', $retailer->id)); ?>">
                                            <?php echo e(method_field('PUT')); ?>

                                            <?php echo csrf_field(); ?>
                                            <fieldset>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="retailer_shop">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message3'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="retailer_shop"
                                                                   name="retailer_shop" value="<?php echo e($retailer->retailer_shop); ?>"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message3'); ?>" required>
                                                            <?php if($errors->has('retailer_shop')): ?>
                                                                <label class="danger"><?php echo e($errors->first('retailer_shop')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="retailer_contact_name">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message4'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="retailer_contact_name"
                                                                   name="retailer_contact_name" value="<?php echo e($retailer->retailer_contact_name); ?>"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message4'); ?>" required>
                                                            <?php if($errors->has('retailer_contact_name')): ?>
                                                                <label class="danger"><?php echo e($errors->first('retailer_contact_name')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="retailer_shop_address">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message5'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="retailer_shop_address"
                                                                   name="retailer_shop_address"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message5'); ?>"
                                                                   value="<?php echo e($retailer->retailer_shop_address); ?>" required>
                                                            <?php if($errors->has('retailer_shop_address')): ?>
                                                                <label class="danger"><?php echo e($errors->first('retailer_shop_address')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>



                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="retailer_phone">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message6'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="retailer_phone"
                                                                   name="retailer_phone"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message6'); ?>"
                                                                   value="<?php echo e($retailer->retailer_phone); ?>" required>
                                                            <?php if($errors->has('retailer_phone')): ?>
                                                                <label class="danger"><?php echo e($errors->first('retailer_phone')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>


                                                </div>


                                                <div class="row">
                                                   <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="retailer_email">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message7'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="email" class="form-control" id="retailer_email"
                                                                   name="retailer_email"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message7'); ?>"
                                                                   value="<?php echo e($retailer->retailer_email); ?>" required>
                                                            <?php if($errors->has('retailer_email')): ?>
                                                                <label class="danger"><?php echo e($errors->first('retailer_email')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ProfileImage">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message8'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="file" class="form-control" id="profile"
                                                                   name="profile"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message8'); ?>">
                                                            <?php if($errors->has('profile')): ?>
                                                                <label class="danger"><?php echo e($errors->first('profile')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="retailer_password">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message9'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="password" class="form-control" id="retailer_password"
                                                                   name="retailer_password"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message9'); ?>" disabled>
                                                            <?php if($errors->has('retailer_password')): ?>
                                                                <label class="danger"><?php echo e($errors->first('retailer_password')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <fieldset class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="1" name="edit_password"
                                                                       id="edit_password" onclick="EditPassword()">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message10'); ?>
                                                            </label>
                                                        </fieldset>
                                                    </div>
                                                </div>


                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> <?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message11'); ?>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script>
        function EditPassword() {
            if (document.getElementById("edit_password").checked = true) {
                document.getElementById('retailer_password').disabled = false;
            }
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>