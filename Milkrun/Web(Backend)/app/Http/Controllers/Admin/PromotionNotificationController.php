<?php

namespace App\Http\Controllers\Admin;


//use App\Models\Onesignal;
use App\Models\PromotionNotification;
//use App\Models\UserDevice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class PromotionNotificationController extends Controller
{
    public function index()
    {

        $promotions = PromotionNotification::paginate(25);
        return view('admin.promotion.index', compact('promotions'));
    }

    public function create()
    {
        return view('admin.promotion.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'application' => 'required|integer|between:1,2',
            'title' => 'required|string',
            'message' => 'required|string',
            'image' => 'required|mimes:jpeg,jpg,png,gif',
        ]);
        $promotion = new PromotionNotification();
        $promotion->title = $request->title;
        $promotion->application = $request->application;
        $promotion->message = $request->message;
        $promotion->url = $request->url;
        if ($request->hasFile('image')) {
            $request->file('image');
            $profile_image = $request->image->store('promotions', 'public');
            $promotion->image = $profile_image;
            $promotion->save();
        }
        $promotion->save();
        if ($request->application == 1) {
            $playerids = array('All');
            //Onesignal::DriverPushMessage($playerids, $promotion->toArray(), $request->title, 2, $merchant_id);
        } else {
            $playerids = array('All');
            //Onesignal::UserPushMessage($playerids, $promotion->toArray(), $request->title, 2, $merchant_id);
        }
        return redirect()->back()->with('notification', 'Notification Sended');
    }

    public function SendNotificationDriver(Request $request)
    {
        $request->validate([
            'persion_id' => ['required',
                Rule::exists('drivers', 'id')],
            'title' => 'required|string',
            'message' => 'required|string',
            'image' => 'required|mimes:jpeg,jpg,png,gif',
        ]);
        $promotion = new PromotionNotification();
        $promotion->title = $request->title;
        $promotion->application = 1;
        $promotion->message = $request->message;
        $promotion->driver_id = $request->persion_id;
        $promotion->url = $request->url;
        if ($request->hasFile('image')) {
            $request->file('image');
            $profile_image = $request->image->store('promotions', 'public');
            $promotion->image = $profile_image;
            $promotion->save();
        }
        $promotion->save();
//        $driver = Driver::find($request->persion_id);
//        $playerids = array($driver->player_id);
//        Onesignal::DriverPushMessage($playerids, $promotion->toArray(), $request->title, 2, $merchant_id, 1);
        return redirect()->back()->with('notification', 'Notification Sended');
    }

    public function SendNotificationUser(Request $request)
    {
        $request->validate([
            'persion_id' => ['required',
                Rule::exists('users', 'id')],
            'title' => 'required|string',
            'message' => 'required|string',
            'image' => 'required|mimes:jpeg,jpg,png,gif',
        ]);
        $promotion = new PromotionNotification();
        $promotion->title = $request->title;
        $promotion->application = 2;
        $promotion->message = $request->message;
        $promotion->user_id = $request->persion_id;
        $promotion->url = $request->url;
        if ($request->hasFile('image')) {
            $request->file('image');
            $profile_image = $request->image->store('promotions', 'public');
            $promotion->image = $profile_image;
            $promotion->save();
        }
        $promotion->save();
//        $userdevices = UserDevice::where([['user_id', '=', $request->persion_id]])->get();
//        $playerids = array_pluck($userdevices, 'player_id');
//        Onesignal::UserPushMessage($playerids, $promotion->toArray(), $request->title, 2, $merchant_id, 1);
        return redirect()->back()->with('notification', 'Notification Sended');
    }


    public function Search(Request $request)
    {
        $query = PromotionNotification::where([['created_at', '!=', ""]]);
        if ($request->title) {
            $query->where('title', $request->title);
        }
        if ($request->application) {
            $query->where('application', $request->application);
        }
        if ($request->date) {
            $query->whereDate('created_at', '=', $request->date);
        }
        $promotions = $query->paginate(25);
        return view('admin.promotion.index', compact('promotions'));
    }

    public function edit($id)
    {
        $promotion = PromotionNotification::findOrFail($id);
        return view('admin.promotion.edit', compact('promotion'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string',
            'message' => 'required|string',
        ]);
        $promotion = PromotionNotification::findOrFail($id);
        $promotion->title = $request->title;
        $promotion->message = $request->message;
        $promotion->url = $request->url;
        if ($request->hasFile('image')) {
            $request->file('image');
            $profile_image = $request->image->store('promotions', 'public');
            $promotion->image = $profile_image;
        }
        $promotion->save();
        return redirect()->back()->with('notification', 'Notification Sended');
    }

    public function destroy($id)
    {
        $promotions = PromotionNotification::findOrFail($id);
        $promotions->delete();
        return redirect()->back();
    }
}
