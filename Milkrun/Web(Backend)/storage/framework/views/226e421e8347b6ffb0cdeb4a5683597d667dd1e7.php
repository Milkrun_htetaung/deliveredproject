<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.message62'); ?></h3>
                </div>
                <div class="col-md-6 col-12">
                    <?php if(session('notification')): ?>
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo app('translator')->getFromJson('admin.message76'); ?></strong>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="<?php echo e(route('promotions.index')); ?>">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="content-body">
                <section id="basic-tabs-components">
                    <div class="row match-height">
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <ul class="nav nav-tabs nav-top-border no-hover-bg">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="base-tab11" data-toggle="tab"
                                                   aria-controls="tab11" href="#tab11" aria-expanded="true"><i
                                                            class="fa fa-users"></i><?php echo app('translator')->getFromJson('admin.message301'); ?></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content px-1 pt-1">
                                            <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true"
                                                 aria-labelledby="base-tab11">
                                                <form method="POST" class="steps-validation wizard-notification"
                                                      enctype="multipart/form-data" action="<?php echo e(route('promotions.store')); ?>">
                                                    <?php echo csrf_field(); ?>
                                                    <fieldset>


                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="location3"><?php echo app('translator')->getFromJson('admin.message67'); ?> :</label>
                                                                    <select class="form-control" name="application"
                                                                            id="application" onchange="DriverNotification(this.value)" required>
                                                                        <option value="">--<?php echo app('translator')->getFromJson('admin.message72'); ?>--</option>
                                                                        <option value="1"><?php echo app('translator')->getFromJson('admin.message69'); ?></option>
                                                                        <option value="2"><?php echo app('translator')->getFromJson('admin.message70'); ?></option>
                                                                    </select>

                                                                    <?php if($errors->has('application')): ?>
                                                                        <label class="danger"><?php echo e($errors->first('application')); ?></label>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="lastName3">
                                                                        <?php echo app('translator')->getFromJson('admin.message63'); ?> :
                                                                        <span class="danger">*</span>
                                                                    </label>
                                                                    <input type="text" class="form-control" id="title"
                                                                           name="title"
                                                                           placeholder="<?php echo app('translator')->getFromJson('admin.message63'); ?>" required>
                                                                    <?php if($errors->has('title')): ?>
                                                                        <label class="danger"><?php echo e($errors->first('title')); ?></label>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="emailAddress5">
                                                                        <?php echo app('translator')->getFromJson('admin.message64'); ?> :
                                                                        <span class="danger">*</span>
                                                                    </label>
                                                                    <textarea class="form-control" id="message" name="message"
                                                                              rows="3"
                                                                              placeholder="<?php echo app('translator')->getFromJson('admin.message64'); ?>"></textarea>
                                                                    <?php if($errors->has('message')): ?>
                                                                        <label class="danger"><?php echo e($errors->first('message')); ?></label>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="ProfileImage">
                                                                        <?php echo app('translator')->getFromJson('admin.message65'); ?> :
                                                                        <span class="danger">*</span>
                                                                    </label>
                                                                    <input type="file" class="form-control" id="image"
                                                                           name="image"
                                                                           placeholder="<?php echo app('translator')->getFromJson('admin.message65'); ?>" required>
                                                                    <?php if($errors->has('image')): ?>
                                                                        <label class="danger"><?php echo e($errors->first('image')); ?></label>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="emailAddress5">
                                                                        <?php echo app('translator')->getFromJson('admin.message66'); ?> :
                                                                        <span class="danger">*</span>
                                                                    </label>
                                                                    <input type="url" class="form-control" id="url"
                                                                           name="url"
                                                                           placeholder="<?php echo app('translator')->getFromJson('admin.message66'); ?>" required>
                                                                    <?php if($errors->has('url')): ?>
                                                                        <label class="danger"><?php echo e($errors->first('url')); ?></label>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>

                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            

                                                            
                                                            
                                                            
                                                            
                                                            

                                                        </div>

                                                    </fieldset>
                                                    <div class="form-actions right">
                                                        <button type="submit" class="btn btn-primary">
                                                            <i class="fa fa-check-square-o"></i> Save
                                                        </button>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>