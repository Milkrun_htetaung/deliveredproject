<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Onesignal extends Model
{
    protected $guarded = [];

    public static function UserPushMessage($playerid, $data, $message, $type, $single = null)
    {
        $detail = Onesignal::first();
        $content = array(
            "en" => $message,
        );
        $sendField = "include_player_ids";
        $sendField = $type == "2" ? "included_segments" : $sendField;
        $sendField = $single == "1" ? "include_player_ids" : $sendField;
        $fields = array(
            'app_id' => $detail->user_application_key,
            $sendField => $playerid,
            'contents' => $content,
            'data' => array('data' => $data, 'type' => $type),
        );
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $detail->user_rest_key));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public static function DriverPushMessage($playerid, $data, $message, $type, $single = null)
    {
        $detail = Onesignal::first();

        $content = array(
            "en" => $message,
        );
        $sendField = "include_player_ids";
        if ($single == null) {
            $sendField = $type == "2" ? "included_segments" : $sendField;
        }
        $fields = array(
            'app_id' => $detail->driver_application_key,
            $sendField => $playerid,
            'contents' => $content,
            'data' => array('data' => $data, 'type' => $type),
        );
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $detail->driver_rest_key));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public static function RetailerPushMessage($playerid, $data, $message, $type, $single = null)
    {
        $detail = Onesignal::first();

        $content = array(
            "en" => $message,
        );
        $sendField = "include_player_ids";
        if ($single == null) {
            $sendField = $type == "2" ? "included_segments" : $sendField;
        }
        $fields = array(
            'app_id' => $detail->driver_application_key,
            $sendField => $playerid,
            'contents' => $content,
            'data' => array('data' => $data, 'type' => $type),
        );
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $detail->driver_rest_key));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}
