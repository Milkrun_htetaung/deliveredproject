@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.message61')</h3>
                </div>

                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            <a title="@lang('admin.message62')" href="{{route('promotions.create')}}">
                                <button class="btn btn-icon btn-success mr-1"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>

                </div>

            </div>
            <div class="content-body">

                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="">
                                    <form method="post" action="{{ route('promotions.search') }}">
                                        @csrf
                                        <div class="table_search">
                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <input type="text" id="" name="title"
                                                           placeholder="@lang('admin.message374')"
                                                           class="form-control col-md-12 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <select class="form-control" name="application"
                                                            id="application">
                                                        <option value="">--@lang('admin.message72')--</option>
                                                        <option value="1">@lang('admin.message69')</option>
                                                        <option value="2">@lang('admin.message70')</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <input type="text" id="" name="date"
                                                           placeholder="@lang('admin.message375')"
                                                           class="form-control col-md-12 col-xs-12 datepickersearch"
                                                           id="datepickersearch">
                                                </div>
                                            </div>

                                            <div class="col-sm-2  col-xs-12 form-group ">
                                                <button class="btn btn-primary" type="submit" name="seabt12"><i
                                                            class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>@lang('admin.message63')</th>
                                                <th>@lang('admin.message64')</th>
                                                <th>@lang('admin.message65')</th>
                                                <th>@lang('admin.message66')</th>
                                                <th>@lang('admin.message67')</th>
                                                <th>@lang('admin.message71')</th>
                                                <th>@lang('admin.action')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($promotions as $promotion)
                                                <tr>
                                                    <td>
                                                        {{ $promotion->title }}
                                                    </td>
                                                    <td>
                                                        <span class="map_address">{{ $promotion->message }}</span>
                                                    </td>
                                                    <td><img src="{{ asset($promotion->image) }}"
                                                             align="center" width="100px" height="80px"
                                                             class="img-radius"
                                                             alt="Promotion Notification Image"></td>
                                                    <td><a class="map_address address_link" target="_blank"
                                                           href="{{ $promotion->url }}">{{ $promotion->url }}</a>
                                                    </td>

                                                    @switch($promotion->application)
                                                        @case(1)
                                                        <td>@lang('admin.message69')</td>
                                                        @break
                                                        @case(2)
                                                        <td>@lang('admin.message70')</td>
                                                        @break
                                                    @endswitch
                                                    <td>{{ $promotion->created_at->toformatteddatestring() }}</td>
                                                    <td>

                                                        <a href="{{ route('promotions.edit',$promotion->id) }}"
                                                           data-original-title="Edit" data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_edit action_btn"> <i
                                                                    class="fa fa-pencil"></i> </a>

                                                        <a href="{{ route('promotions.delete',$promotion->id) }}"
                                                           data-original-title="@lang('admin.delete')"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_delete action_btn"> <i
                                                                    class="fa fa-trash"></i> </a>

                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1">{{ $promotions->links() }}</div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>
@endsection