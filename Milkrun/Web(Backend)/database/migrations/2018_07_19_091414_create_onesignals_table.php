<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnesignalsTable extends Migration
{
    public function up()
    {
        Schema::create('onesignals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_application_key');
            $table->string('driver_application_key');
            $table->string('user_rest_key');
            $table->string('driver_rest_key');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('onesignals');
    }
}
