package com.milkrun.delivered

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.view.View
import android.view.View.GONE
import android.view.Window
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationSet
import android.view.animation.ScaleAnimation
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.milkrun.delivered.ui.customer.CustomerLoginActivity
import com.milkrun.delivered.ui.deliverer.DelivererLoginActivity
import com.milkrun.delivered.ui.retailer.RetailerLoginActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var shortAnimationDuration: Int = 0

    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main)

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels


        val lpTop = top_cover.layoutParams as ConstraintLayout.LayoutParams
        lpTop.height = (height/1.1).toInt()


        val lpBot = bottom_cover.layoutParams as ConstraintLayout.LayoutParams
        lpBot.height = (height/1.5).toInt()+20

        bottom_cover.layoutParams=lpBot
        top_cover.layoutParams = lpTop

        val moveYT=(height/1.1)-500

        val moveY=(height/2)-250

        Handler().postDelayed(Runnable {

            //logo disappear
            findViewById<View>(R.id.logo).animate()
                .alpha(0f)
                .setDuration(2000)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        findViewById<View>(R.id.logo).visibility = View.GONE
                    }
                })
        },1800)

        Handler().postDelayed(Runnable {

            //slide up top corver
            ObjectAnimator.ofFloat(top_cover, "translationY",-moveYT.toFloat()).apply {
                duration = 1500
                start()
            }
            //slide down bottom corver
            ObjectAnimator.ofFloat(bottom_cover, "translationY",moveY.toFloat()).apply {
                duration = 1500
                start()
            }

        },3000)

//        var animation =  AnimationSet(true);
//        animation.addAnimation( AlphaAnimation(1.0F,0.0F));
//        animation.addAnimation( ScaleAnimation(0.8f, 1f, 0.8f, 1f)); // Change args as desired
//        animation.setDuration(300);
//        findViewById<View>(R.id.logo).startAnimation(animation)
        buttonClickAnimation()
        crossfade()

    }


    private fun crossfade() {
//        findViewById<View>(R.id.logo).apply {
//            // Set the content view to 0% opacity but visible, so that it is visible
//            // (but fully transparent) during the animation.
//            alpha = 0f
//            visibility = View.VISIBLE
//
//            // Animate the content view to 100% opacity, and clear any animation
//            // listener set on the view.
//            animate()
//                .alpha(1f)
//                .setDuration(shortAnimationDuration.toLong())
//                .setListener(null)
//        }
        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)

    }

    override fun onResume() {
        super.onResume()
        //making sure all label are hidden
        hideAllBtnLabel()

    }

    fun hideAllBtnLabel(){
        retailer_label.visibility=GONE
        cust_label.visibility=GONE
        deliverer_label.visibility=GONE

    }

    fun buttonClickAnimation(){
        customer_btn.setOnClickListener {

            cust_label.apply {
                alpha = 0f
                visibility = View.VISIBLE

                // Animate the content view to 100% opacity, and clear any animation
                // listener set on the view.
                animate()
                    .alpha(1f)
                    .setDuration(100)
                    .setListener(null)
            }

            startActivity(CustomerLoginActivity.newIntent(getBaseContext()))


        }

        retailer_btn.setOnClickListener {

            retailer_label.apply {
                alpha = 0f
                visibility = View.VISIBLE

                // Animate the content view to 100% opacity, and clear any animation
                // listener set on the view.
                animate()
                    .alpha(1f)
                    .setDuration(100)
                    .setListener(null)
            }
            startActivity(RetailerLoginActivity.newIntent(getBaseContext()))

        }

        deliverer_btn.setOnClickListener {

            deliverer_label.apply {
                alpha = 0f
                visibility = View.VISIBLE

                // Animate the content view to 100% opacity, and clear any animation
                // listener set on the view.
                animate()
                    .alpha(1f)
                    .setDuration(100)
                    .setListener(null)
            }
            startActivity(DelivererLoginActivity.newIntent(getBaseContext()))

        }
    }


}

