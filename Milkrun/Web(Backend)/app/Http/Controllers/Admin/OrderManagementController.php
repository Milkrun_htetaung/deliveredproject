<?php

namespace App\Http\Controllers\Admin;

use App\Models\OrderManagement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Retailer;

class OrderManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = OrderManagement::latest()->paginate(25);
        return view('admin.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderManagement  $orderManagement
     * @return \Illuminate\Http\Response
     */
    public function show(OrderManagement $orderManagement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderManagement  $orderManagement
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderManagement $orderManagement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderManagement  $orderManagement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderManagement $orderManagement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderManagement  $orderManagement
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderManagement $orderManagement)
    {
        //
    }
}
