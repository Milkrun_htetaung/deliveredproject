<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;

class CmsPage extends Model
{
    protected $guarded = [];

    public function LanguageAny()
    {
        return $this->hasOne(LanguageCmsPage::class);
    }

    public function LanguageSingle()
    {
        return $this->hasOne(LanguageCmsPage::class)->where([['locale', '=', App::getLocale()]]);
    }
}
