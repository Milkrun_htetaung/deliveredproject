<?php
/*
app_type ==> 1 For Retailer
app_type ==> 2 For Deliverer
app_type ==> 3 For User
*/

namespace App\Http\Controllers\API\Account_Module;

use App\Models\User;
use App\Models\Driver;
use App\Models\Customer;
use App\Models\Retailer;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Client;

class AccountController extends Controller
{
    /************ SignUp APIs ************/
    public function Register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'app_type' => 'required'
            ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors['0'], 'data' => []]);
        }

        $app_type = $request->app_type;
        switch ($app_type) {
            case 1:
                return $this->retailer_register($request);

                break;
            case 2:
                return $this->driver_register($request);
                break;
            case 3:
                return $this->user_register($request);
                break;
            default:
                return response()->json(['result' => "0", 'message' => 'Invalid Request', 'data' => []]);
        }

    }

    /*Retailer Register*/
    public function retailer_register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'retailer_shop' => "required",
                'retailer_contact_name' => "required",
                'country_id' => 'required',
                'retailer_email' => ['required', 'max:255', 'unique:retailers,retailer_email'],
                'retailer_phone' => ['required', 'max:255', 'unique:retailers,retailer_phone'],
                'retailer_password' => 'required',
                'retailer_shop_address' => "required",
                'profile_image' => "required",

            ]);


        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors['0'], 'data' => []]);
        }

        
        list($format, $image) = explode(',', $request->profile_image);
        $temp = explode('/', $format);
        list($ext,) = explode(';', $temp[1]);
        $file_name = time() . "_retailer_register." . $ext;
        file_put_contents(public_path() . '/images/retailer/' . $file_name, base64_decode($image));
        $profile_image = env('APP_URL')."/images/retailer/".$file_name;
        $country_code = Country::find($request->country_id)->phonecode;
        
        
        $password = Hash::make($request->retailer_password);
        $retailer = Retailer::create([
            'retailer_shop' => $request->retailer_shop,
            'retailer_shop_address' => $request->retailer_shop_address,
            'retailer_contact_name' => $request->retailer_contact_name,
            'retailer_phone' => $country_code . $request->retailer_phone,
            'retailer_email' => $request->retailer_email,
            'password' => $password,
            'retailer_image' =>$profile_image,
            'country_id' => $request->country_id,
            'login_type' => 1,
            'ReferralCode' => Retailer::GenrateReferCode()
        ]);
        
        \Config::set('auth.guards.api.provider', 'retailers');
        $client = Client::where([['user_id', true], ['password_client', true]])->first();
        $request->request->add([
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => $country_code . $request->retailer_phone,
            'password' => $request->retailer_password,
            'scope' => '',
        ]);
        // Fire off the internal request.
        $token_generation_after_reg = Request::create(
            'oauth/token',
            'POST'
        );

        $collect_response = \Route::dispatch($token_generation_after_reg)->getContent();
        
        $collectArray = json_decode($collect_response);
        if (isset($collectArray->error)) {
            return response()->json(['result' => "0", 'message' => $collectArray->message, 'data' => []]);
        }

        return response()->json(['result' => "1", 'message' => 'Sign Up Succesful', 'data' => ['access_token' => $collectArray->access_token, 'refresh_token' => $collectArray->refresh_token, 'app_type' => $request->app_type]]);
    }

    /*User Register*/
    /*public function user_register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'app_type' => 'required'
            ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors['0'], 'data' => []]);
        }


    }*/
    public function user_register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'customer_name' => 'required',
                'customer_email' => ['required', 'unique:customers,customer_email'],
                'country_id' => 'required',
                'password' => 'required',
                'customer_phone' => ['required', 'max:255', 'unique:customers,customer_phone'],
                'profile_image' => 'required'
            ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors['0'], 'data' => []]);
        }
         
        $country_code = Country::find($request->country_id)->phonecode; 
        
        $password = Hash::make($request->password);
        
        list($format, $image) = explode(',', $request->profile_image);
        $temp = explode('/', $format);
        list($ext,) = explode(';', $temp[1]);
        $file_name = time() . "_register_user." . $ext;
        file_put_contents(public_path() . '/images/customer/' . $file_name, base64_decode($image));
        $profile_image = env('APP_URL')."/images/customer/".$file_name;

//        $profile_image = $request->profile_image->store('customer', 'public');
        $profile_image = env('APP_URL')."/images/retailer/".$file_name;

        $driver = Customer::create([
            'customer_name' => $request->customer_name,
            'customer_email' => $request->customer_email,
            'customer_phone' => $country_code . $request->customer_phone,
            'password' => $password,
            'customer_image' => $profile_image,
            'customer_address' => ''
//            'login_type' => 1,
            //'ReferralCode' => Customer::GenrateReferCode()
        ]);
        \Config::set('auth.guards.api.provider', 'customers');
        $client = Client::where([['user_id', true], ['password_client', true]])->first();
        $request->request->add([
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => $country_code . $request->customer_phone,
            'password' => $request->password,
            'scope' => '',
        ]);
          
        // Fire off the internal request.
        
        $token_generation_after_reg = Request::create(
            'oauth/token',
            'POST'
        );
        //print_r($token_generation_after_reg);die();
        $collect_response = \Route::dispatch($token_generation_after_reg)->getContent();
        //print_r($collect_response);die();
        $collectArray = json_decode($collect_response);
        
        if (isset($collectArray->error)) {
            return response()->json(['result' => "0", 'message' => $collectArray->message, 'data' => []]);
        }
        return response()->json(['result' => "1", 'message' => 'Sign Up Succesful', 'data' => ['access_token' => $collectArray->access_token, 'refresh_token' => $collectArray->refresh_token, 'app_type' => $request->app_type]]);

    }

    /*Driver Register*/
    public function driver_register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'driver_name' => 'required',
                'driver_email' => ['required', 'unique:drivers,driver_email'],
                'country_id' => 'required',
                'driver_phone' => ['required', 'max:255', 'unique:drivers,driver_phone'],
                'driver_password' => 'required',
                'profile_image' => 'required'
            ]);


        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors['0'], 'data' => []]);
        }
        $country_code = Country::find($request->country_id)->phonecode;

        $password = Hash::make($request->driver_password);
        list($format, $image) = explode(',', $request->profile_image);
        $temp = explode('/', $format);
        list($ext,) = explode(';', $temp[1]);
        $file_name = time() . "_register_driver." . $ext;
        file_put_contents(public_path() . '/images/driver/' . $file_name, base64_decode($image));
        $profile_image = env('APP_URL')."/images/driver/".$file_name;

//        $profile_image = $request->profile_image->store('driver', 'public');
        
        $driver = Driver::create([
            'driver_name' => $request->driver_name,
            'driver_email' => $request->driver_email,
            'driver_phone' => $country_code . $request->driver_phone,
            'password' => $password,
            'driver_image' => $profile_image,
//            'login_type' => 1,
            'ReferralCode' => Driver::GenrateReferCode()
        ]);
        \Config::set('auth.guards.api.provider', 'drivers');
        $client = Client::where([['user_id', true], ['password_client', true]])->first();
        $request->request->add([
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => $country_code . $request->driver_phone,
            'password' => $request->driver_password,
            'scope' => '',
        ]);
        // Fire off the internal request.
        $token_generation_after_reg = Request::create(
            'oauth/token',
            'POST'
        );
        $collect_response = \Route::dispatch($token_generation_after_reg)->getContent();
        $collectArray = json_decode($collect_response);

        if (isset($collectArray->error)) {
            return response()->json(['result' => "0", 'message' => $collectArray->message, 'data' => []]);
        }
        return response()->json(['result' => "1", 'message' => 'Sign Up Succesful', 'data' => ['access_token' => $collectArray->access_token, 'refresh_token' => $collectArray->refresh_token, 'app_type' => $request->app_type]]);

    }
    /************ End SignUp APIs ************/


    /************ LOGIN APIS ************/

    public function Login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'app_type' => 'required'
            ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors['0'], 'data' => []]);
        }

        $app_type = $request->app_type;
        switch ($app_type) {
            case 1:
                return $this->retailer_login($request);

                break;
            case 2:
                return $this->driver_login($request);
                break;
            case 3:
                return $this->user_login($request);
                break;
            default:
                return response()->json(['result' => "0", 'message' => 'Invalid Request', 'data' => []]);
        }

    }

    /*Retailer Register*/
    public function retailer_login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'retailer_phone' => ['required',
                    Rule::exists('retailers', 'retailer_phone')],
                'retailer_password' => 'required',
            ]);


        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors['0'], 'data' => []]);
        }
        \Config::set('auth.guards.api.provider', 'retailers');
        $client = Client::where([['user_id', true], ['password_client', true]])->first();

        $request->request->add([
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => $request->retailer_phone,
            'password' => $request->retailer_password,
            'scope' => '',
        ]);
        // Fire off the internal request.
        $token_generation_after_reg = Request::create(
            'oauth/token',
            'POST'
        );

        $collect_response = \Route::dispatch($token_generation_after_reg)->getContent();
        $collectArray = json_decode($collect_response);
        if (isset($collectArray->error)) {
            return response()->json(['result' => "0", 'message' => $collectArray->message, 'data' => []]);
        }
        return response()->json(['result' => "1", 'message' => 'Login Succesful', 'data' => ['access_token' => $collectArray->access_token, 'refresh_token' => $collectArray->refresh_token, 'app_type' => "1"]]);

    }

    /*User Register*/
    public function user_login(Request $request)
    {
        //echo "hi"; die();
        $validator = Validator::make($request->all(),
            [
                'customer_phone' => ['required',
                    Rule::exists('customers', 'customer_phone')],
                'customer_password' => 'required',
            ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors['0'], 'data' => []]);
        }
        
        \Config::set('auth.guards.api.provider', 'customers');
        $client = Client::where([['user_id', true], ['password_client', true]])->first();
        $request->request->add([
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => $request->customer_phone,
            'password' => $request->customer_password,
            'scope' => '',
        ]);
        // Fire off the internal request.
        $token_generation_after_reg = Request::create(
            'oauth/token',
            'POST'
        );

        $collect_response = \Route::dispatch($token_generation_after_reg)->getContent();
        $collectArray = json_decode($collect_response);
        if (isset($collectArray->error)) {
            return response()->json(['result' => "0", 'message' => $collectArray->message, 'data' => []]);
        }
        return response()->json(['result' => "1", 'message' => 'Login Succesful', 'data' => ['access_token' => $collectArray->access_token, 'refresh_token' => $collectArray->refresh_token, 'app_type' => "3"]]);


    }

    /*Driver Register*/
    public function driver_login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'driver_phone' => ['required',
                    Rule::exists('drivers', 'driver_phone')],
                'driver_password' => 'required',
            ]);


        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors['0'], 'data' => []]);
        }

        \Config::set('auth.guards.api.provider', 'drivers');
        $client = Client::where([['user_id', true], ['password_client', true]])->first();
        $request->request->add([
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => $request->driver_phone,
            'password' => $request->driver_password,
            'scope' => '',
        ]);
        // Fire off the internal request.
        $token_generation_after_reg = Request::create(
            'oauth/token',
            'POST'
        );

        $collect_response = \Route::dispatch($token_generation_after_reg)->getContent();
        $collectArray = json_decode($collect_response);
        if (isset($collectArray->error)) {
            return response()->json(['result' => "0", 'message' => $collectArray->message, 'data' => []]);
        }
        return response()->json(['result' => "1", 'message' => 'Login Succesful', 'data' => ['access_token' => $collectArray->access_token, 'refresh_token' => $collectArray->refresh_token, 'app_type' => "2"]]);



    }

    /************ End Login APIS ************/
}
