<?php
/**
 * Created by PhpStorm.
 * User: Yogesh Apporio
 * Date: 20-12-2018
 * Time: 11:34
 */

namespace App\Http\Controllers\API\Profiles\Customer;

use App\Models\User;
use App\Models\Driver;
use App\Models\Retailer;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Client;

class CustomerProfileController extends Controller
{
    /************ ViewProfile APIs ************/
    public function View_Profile(Request $request)
    {
        $driver = $request->user('api_driver');
        return response()->json(['result' => "1", 'message' => 'Driver Profile', 'data' => ['driver' => $driver, 'app_type' => "2"]]);

    }

    public function Edit_Profile(Request $request)
    {
        $driver = $request->user('api_driver');
        $validator = Validator::make($request->all(),
            [
                'driver_name' => 'required',
                'driver_email' => ['required', 'unique:drivers,driver_email'],
            ]);
        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
        }
        if ($request->profile_image) {
            list($format, $image) = explode(',', $request->profile_image);
            $temp = explode('/', $format);
            list($ext,) = explode(';', $temp[1]);
            $file_name = time() . "_driver_edit_profile." . $ext;
            file_put_contents(public_path() . '/images/customer/' . $file_name, base64_decode($image));
            $driver->driver_image = env('APP_URL')."/images/customer/".$file_name;
        }
        $driver->driver_name = $request->driver_name;
        $driver->driver_email = $request->driver_email;
        $driver->save();
        return response()->json(['result' => "1", 'message' => "Profile Updated", 'data' => ['driver' => $driver, 'app_type' => "2"]]);
    }

}