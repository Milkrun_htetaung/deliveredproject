@extends('merchant.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.message237')</h3>

                </div>
                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('retailer.index') }}"><button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i></button></a>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div id="user-profile">
                    <div class="row">
                        <!-- Column -->
                        <div class="col-md-4 col-xs-12">
                            <div class="card">
                                <div class="card-block">
                                    <center class="m-t-10"><img
                                                src="@if ($user->UserProfileImage) {{ asset($user->UserProfileImage) }} @else {{ asset("user.png") }} @endif"
                                                class="img-circle" width="120">
                                        <h4 class="card-title m-t-10">{{ $user->UserName }}</h4>
                                        <h6 class="card-subtitle">{{ $user->UserPhone }}</h6>
                                        <h6 class="card-subtitle">{{ $user->email }}</h6>
                                    </center>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-8 col-xs-12">
                            <div class="col-md-6 col-xs-12" style="float:left;">
                                <div class="white-box">
                                    <ul class="book_details">
                                        <li>
                                            <h4>@lang('admin.RiderType')</h4>
                                            <p>
                                                @if($user->user_type == 1)
                                                    @lang('admin.Corporate')
                                                @else
                                                    @lang('admin.Retail')
                                                @endif
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12" style="float:left;">
                                <div class="white-box">
                                    <ul class="book_details">
                                        <li>
                                            <h4>@lang('admin.ReferralCode')</h4>
                                            <p>{{ $user->ReferralCode }}</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12" style="float:left;">
                                <div class="white-box">
                                    <ul class="book_details">
                                        <li>
                                            <h4>@lang('admin.UserSignupType')</h4>
                                            <p>
                                                @switch($user->UserSignupType)
                                                    @case(1)
                                                    @lang('admin.Normal')
                                                    @break
                                                    @case(2)
                                                    @lang('admin.Google')
                                                    @break
                                                    @case(3)
                                                    @lang('admin.Facebook')
                                                    @break
                                                @endswitch
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12" style="float:left;">
                                <div class="white-box">
                                    <ul class="book_details">
                                        <li>
                                            <h4>@lang('admin.UserSignupFrom')</h4>
                                            <p>
                                                @switch($user->UserSignupFrom)
                                                    @case(1)
                                                    @lang('admin.application')
                                                    @break
                                                    @case(2)
                                                    @lang('admin.admin')
                                                    @break
                                                    @case(3)
                                                    @lang('admin.web')
                                                    @break
                                                @endswitch
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12" style="float:left;">
                                <div class="white-box">
                                    <ul class="book_details">
                                        <li>
                                            <h4>@lang('admin.Registerdate')</h4>
                                            <p>{{ $user->created_at }}</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12" style="float:left;">
                                <div class="white-box">
                                    <ul class="book_details">
                                        <li>
                                            <h4>@lang('admin.lastUpdate')</h4>
                                            <p>{{ $user->updated_at }}</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>@lang('admin.message51')</th>
                                                <th>@lang('admin.AreaName')</th>
                                                <th>@lang('admin.servicetype')</th>
                                                <th>@lang('admin.VehicleType')</th>
                                                <th>@lang('admin.PickupLocation')</th>
                                                <th>@lang('admin.DropLocation')</th>
                                                <th>@lang('admin.created_at')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($bookings as $booking)
                                                <tr>
                                                    <td>
                                                        @if($booking->booking_type == 1)
                                                            @lang('admin.message52')
                                                        @else
                                                            @lang('admin.message53')
                                                        @endif
                                                    </td>
                                                    <td> {{ $booking->CountryArea->AreaName }}</td>
                                                    <td> {{ $booking->ServiceType->serviceName }}</td>
                                                    <td> {{ $booking->VehicleType->vehicleTypeName }}</td>
                                                    <td> {{ $booking->pickup_location }}</td>
                                                    <td> {{ $booking->drop_location }}</td>
                                                    <td>
                                                        {{ $booking->created_at }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1">{{ $bookings->links() }}</div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection
