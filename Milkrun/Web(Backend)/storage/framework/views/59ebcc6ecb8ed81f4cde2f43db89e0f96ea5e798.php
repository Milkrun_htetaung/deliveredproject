<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.melvin_measurement_edit_message1'); ?></h3>
                </div>
                <div class="col-md-6 col-12">
                    <?php if(session('measureupdated')): ?>
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo app('translator')->getFromJson('admin.melvin_measurement_edit_message9'); ?></strong>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="<?php echo e(route('measurements.index')); ?>">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data"
                                              action="<?php echo e(route('measurements.update', $measurement->id)); ?>">
                                            <?php echo e(method_field('PUT')); ?>

                                            <?php echo csrf_field(); ?>
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="form-group">
                                                            <label for="measure_unit_name">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_measurement_edit_message3'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="measure_unit_name"
                                                                   name="measure_unit_name" value="<?php echo e($measurement->measure_unit_name); ?>"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_measurement_edit_message3'); ?>" required>
                                                            <?php if($errors->has('measure_unit_name')): ?>
                                                                <label class="danger"><?php echo e($errors->first('measure_unit_name')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-9">
                                                        <div class="form-group">
                                                            <label for="measure_unit">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_measurement_edit_message8'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="measure_unit"
                                                                   name="measure_unit" value="<?php echo e($measurement->measure_unit); ?>"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_measurement_edit_message8'); ?>" required>
                                                            <?php if($errors->has('measure_unit')): ?>
                                                                <label class="danger"><?php echo e($errors->first('measure_unit')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="form-group">
                                                            <label for="measure_unit_value">
                                                                <?php echo app('translator')->getFromJson('admin.melvin_measurement_edit_message4'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="measure_unit_value"
                                                                   name="measure_unit_value"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.melvin_measurement_edit_message4'); ?>"
                                                                   value="<?php echo e($measurement->measure_unit_value); ?>" required>
                                                            <?php if($errors->has('measure_unit_value')): ?>
                                                                <label class="danger"><?php echo e($errors->first('measure_unit_value')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> <?php echo app('translator')->getFromJson('admin.melvin_retailer_edit_message11'); ?>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script>
        function EditPassword() {
            if (document.getElementById("edit_password").checked = true) {
                document.getElementById('password').disabled = false;
            }
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>