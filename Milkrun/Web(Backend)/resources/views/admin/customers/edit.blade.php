@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.melvin_customer_edit_message1')</h3>
                </div>
                <div class="col-md-6 col-12">
                    @if(session('rideradded'))
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>@lang('admin.melvin_retailer_edit_message2')</strong>
                        </div>
                    @endif
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('customers.index') }}">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data"
                                              action="{{route('customers.update', $customer->id)}}">
                                            {{method_field('PUT')}}
                                            @csrf
                                            <fieldset>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label
                                                                for="location3">@lang('admin.melvin_customer_add_message2')
                                                                :</label>
                                                            <select class="form-control" name="retailer_id"
                                                                    id="retailer_id"
                                                                    required>
                                                                <option
                                                                    value="">@lang('admin.melvin_customer_add_message3')</option>
                                                                @foreach($retailers  as $retailer)
                                                                    <option data-min=""
                                                                            data-max=""
                                                                            value="{{ $retailer->id }}">{{$retailer->retailer_shop  }}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('retailer_id'))
                                                                <label
                                                                    class="danger">{{ $errors->first('retailer_id') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                @lang('admin.melvin_customer_add_message4') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   id="customer_name"
                                                                   name="customer_name"
                                                                   value="{{ $customer->customer_name }}"
                                                                   placeholder="@lang('admin.melvin_customer_add_message4')"
                                                                   required>
                                                            @if ($errors->has('customer_name'))
                                                                <label
                                                                    class="danger">{{ $errors->first('customer_name') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_email">
                                                                @lang('admin.melvin_customer_add_message5') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="email" class="form-control" id="customer_email"
                                                                   name="customer_email"
                                                                   value="{{ $customer->customer_email }}"
                                                                   placeholder="@lang('admin.melvin_customer_add_message5')"
                                                                   required>
                                                            @if ($errors->has('customer_email'))
                                                                <label
                                                                    class="danger">{{ $errors->first('customer_email') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_address">
                                                                @lang('admin.melvin_customer_add_message6') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   id="customer_address"
                                                                   name="customer_address"
                                                                   value="{{ $customer->customer_address }}"
                                                                   placeholder="@lang('admin.melvin_customer_add_message6')"
                                                                   required>
                                                            @if ($errors->has('customer_address'))
                                                                <label
                                                                    class="danger">{{ $errors->first('customer_address') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>



                                                </div>


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label
                                                                for="location3">@lang('admin.melvin_retailer_create_message7')
                                                                :</label>
                                                            <select class="form-control" name="country" id="country"
                                                                    required>
                                                                <option
                                                                    value="">@lang('admin.melvin_retailer_create_message8')</option>
                                                                @foreach($countries  as $country)
                                                                    <option data-min=""
                                                                            data-max=""
                                                                            value="{{ $country->phonecode }}">{{$country->name  }}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('country'))
                                                                <label
                                                                    class="danger">{{ $errors->first('country') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="lastName3">
                                                                @lang('admin.melvin_customer_add_message7') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="customer_phone"
                                                                   name="customer_phone"
                                                                   value="{{ $customer->customer_phone }}"
                                                                   placeholder="@lang('admin.melvin_customer_add_message7')"
                                                                   required>
                                                            @if ($errors->has('customer_phone'))
                                                                <label
                                                                    class="danger">{{ $errors->first('customer_phone') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ProfileImage">
                                                                @lang('admin.melvin_retailer_create_message10') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="file" class="form-control" id="profile"
                                                                   name="profile"
                                                                   placeholder="@lang('admin.melvin_retailer_create_message10')"
                                                                   required>
                                                            @if ($errors->has('profile'))
                                                                <label
                                                                    class="danger">{{ $errors->first('profile') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="customer_password">
                                                                @lang('admin.melvin_customer_add_message8') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="password" class="form-control" id="customer_password"
                                                                   name="customer_password"
                                                                   value="{{ $customer->customer_password }}"
                                                                   placeholder="@lang('admin.melvin_customer_add_message8')"
                                                                   required disabled>
                                                            @if ($errors->has('customer_password'))
                                                                <label
                                                                    class="danger">{{ $errors->first('customer_password') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                </div>

                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> @lang('admin.melvin_retailer_edit_message11')
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script>
        function EditPassword() {
            if (document.getElementById("edit_password").checked = true) {
                document.getElementById('password').disabled = false;
            }
        }

    </script>
@endsection
