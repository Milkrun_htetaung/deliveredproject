<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.melvin_order_message1'); ?></h3>
                </div>

                <div class="col-md-6 col-12">
                    <?php if(session('moneyAdded')): ?>
                        <div class="col-md-8 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo app('translator')->getFromJson('admin.message430'); ?></strong>
                        </div>
                    <?php endif; ?>
                </div>


                
                    
                        
                            
                                
                                        
                                
                            
                        
                    
                

            </div>
            <div class="content-body">
                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="">
                                    <form action="" method="post">
                                        <?php echo csrf_field(); ?>
                                        <div class="table_search">
                                            <div class="col-md-2 col-xs-4 form-group ">
                                                <div class="input-group">
                                                    <select class="form-control" name="parameter"
                                                            id="parameter"
                                                            required>
                                                        <option value="1"><?php echo app('translator')->getFromJson('admin.RiderName'); ?></option>
                                                        <option value="2"><?php echo app('translator')->getFromJson('admin.email'); ?></option>
                                                        <option value="3"><?php echo app('translator')->getFromJson('admin.RiderPhone'); ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-xs-6 form-group ">
                                                <div class="input-group">
                                                    <input type="text" name="keyword"
                                                           placeholder="<?php echo app('translator')->getFromJson('admin.message337'); ?>"
                                                           class="form-control col-md-12 col-xs-12" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-2  col-xs-12 form-group ">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"
                                                                                                 aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table
                                            class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message2'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message3'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message4'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message5'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message6'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message7'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message8'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message9'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message10'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message11'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message12'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message13'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message14'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message15'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_order_message16'); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>


                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1"><?php echo e($orders->links()); ?></div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>