<?php

return [


    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],
    //all guards
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],
        'admin' => [
            'driver' => 'session',
            'provider' => 'admin',
        ],
        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],
        'api_retailer' => [
            'driver' => 'passport',
            'provider' => 'retailers',
        ],
        'api_driver' => [
            'driver' => 'passport',
            'provider' => 'drivers',
        ],
        'api_customer' => [
            'driver' => 'passport',
            'provider' => 'customers',
        ],
    ],
    //all providers
    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\User::class,
        ],
        'admin' => [
            'driver' => 'eloquent',
            'model' => App\Models\Admin::class,
        ],
        'retailers'=>[
            'driver' => 'eloquent',
            'model' => App\Models\Retailer::class,
        ],
        'drivers' => [
            'driver' => 'eloquent',
            'model' => App\Models\Driver::class,
        ],
        'customers' => [
            'driver' => 'eloquent',
            'model' => App\Models\Customer::class,
        ],
    ],



    'passwords' => [
        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
        ],
    ],

];
