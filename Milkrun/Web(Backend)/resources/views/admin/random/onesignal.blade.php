@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.message85')</h3>

                </div>
            </div>
            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        @if(session('onesignal'))
                                            <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2"
                                                 role="alert">
                                                <span class="alert-icon"><i class="fa fa-info"></i></span>
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <strong>@lang('admin.message86')</strong>
                                            </div>
                                        @endif
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data"
                                              action="{{ route('onesignal.store') }}">
                                            @csrf
                                            <fieldset>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                @lang('admin.message87') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   id="user_application_key" name="user_application_key"
                                                                   placeholder="@lang('admin.message87')"
                                                                   value="@if(!empty($onesignal)){{ $onesignal->user_application_key }}@endif"
                                                                   required>
                                                            @if ($errors->has('user_application_key'))
                                                                <label class="danger">{{ $errors->first('user_application_key') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                @lang('admin.message88') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="user_rest_key"
                                                                   name="user_rest_key"
                                                                   placeholder="@lang('admin.message88')"
                                                                   value="@if(!empty($onesignal)){{ $onesignal->user_rest_key }}@endif" required>
                                                            @if ($errors->has('user_rest_key'))
                                                                <label class="danger">{{ $errors->first('user_rest_key') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                @lang('admin.message89') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   id="driver_application_key"
                                                                   name="driver_application_key"
                                                                   placeholder="@lang('admin.message89')"
                                                                   value="@if(!empty($onesignal)){{ $onesignal->driver_application_key }}@endif"
                                                                   required>
                                                            @if ($errors->has('driver_application_key'))
                                                                <label class="danger">{{ $errors->first('driver_application_key') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                @lang('admin.message90') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="driver_rest_key"
                                                                   name="driver_rest_key"
                                                                   placeholder="@lang('admin.message90')"
                                                                   value="@if(!empty($onesignal)){{ $onesignal->driver_rest_key }}@endif" required>
                                                            @if ($errors->has('driver_rest_key'))
                                                                <label class="danger">{{ $errors->first('driver_rest_key') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                </div>

                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>

@endsection