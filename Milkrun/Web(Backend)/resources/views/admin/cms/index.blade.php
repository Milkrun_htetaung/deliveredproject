@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.message208')</h3>
                </div>

                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            <a title="@lang('admin.message209')" href="{{route('cms.create')}}">
                                <button class="btn btn-icon btn-success mr-1"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">

                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="">
                                    <form method="post" action="{{ route('admin.cms.search') }}">
                                        @csrf
                                        <div class="table_search">
                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <input type="text" id="" name="pagetitle"
                                                           placeholder="@lang('admin.message373')"
                                                           class="form-control col-md-12 col-xs-12">
                                                </div>
                                            </div>


                                            <div class="col-sm-2  col-xs-12 form-group ">
                                                <button class="btn btn-primary" type="submit" name="seabt12"><i
                                                            class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>@lang('admin.message213')</th>
                                                <th>@lang('admin.message212')</th>
                                                <th>@lang('admin.message214')</th>
                                                <th>@lang('admin.action')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($cmspages as $cmspage)
                                                <tr>
                                                    <td>
                                                        @if($cmspage->application == 1)
                                                            User
                                                        @else
                                                            Driver
                                                        @endif

                                                    </td>
                                                    <td>@if(empty($cmspage->LanguageSingle))
                                                            <span style="color:red">{{ trans('admin.name-not-given') }}</span>
                                                            <span class="text-primary">( In {{ $cmspage->LanguageAny->LanguageName->name }}
                                                                : {{ $cmspage->LanguageAny->title }}
                                                                )</span>
                                                        @else
                                                            {{ $cmspage->LanguageSingle->title }}
                                                        @endif
                                                    </td>
                                                    <td>@if(empty($cmspage->LanguageSingle))
                                                            <span style="color:red">{{ trans('admin.name-not-given') }}</span>
                                                            <span class="text-primary">( In {{ $cmspage->LanguageAny->LanguageName->name }}
                                                                : {{ substr($cmspage->LanguageAny->description, 0, 70) }}
                                                                )</span>
                                                        @else
                                                            {{ substr($cmspage->LanguageSingle->description, 0, 70) }}
                                                        @endif
                                                    </td>

                                                    <td>
                                                        <a href="{{ route('cms.edit',$cmspage->id) }}"
                                                           data-original-title="Edit" data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_edit action_btn"> <i
                                                                    class="fa fa-pencil"></i> </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>
@endsection