package com.milkrun.delivered;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.milkrun.delivered.repository.RemoteCustomerRepository;
import com.milkrun.delivered.repository.RemoteDriverRepository;
import com.milkrun.delivered.repository.RemoteRetailerRepository;
import com.milkrun.delivered.ui.customer.customerlogin.CustomerLoginViewModel;
import com.milkrun.delivered.ui.customer.signup.CustomerSignUpViewModel;
import com.milkrun.delivered.ui.deliverer.delivererlogin.DelivererLoginViewModel;
import com.milkrun.delivered.ui.deliverer.jobs.DelivererJobsViewModel;
import com.milkrun.delivered.ui.deliverer.signup.DelivererSignUpViewModel;
import com.milkrun.delivered.ui.retailer.addCustomer.AddCustomerViewModel;
import com.milkrun.delivered.ui.retailer.jobs.JobsListViewModel;
import com.milkrun.delivered.ui.retailer.login.LoginViewModel;
import com.milkrun.delivered.ui.retailer.postdelivery.PostDeliveryViewModel;
import com.milkrun.delivered.ui.retailer.sign_up.RetailerSignUpViewModel;

/**
 * this class will be used for getting view model objects as singletons for this project
 * MasterDetailViewModel will be shared between two fragments until activity finish
 */
public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {



    private RemoteRetailerRepository remoteRetailerRepository;
    private RemoteDriverRepository remoteDriverRepository;
    private RemoteCustomerRepository remoteCustomerRepository;

    public static JobsListViewModel jobsListViewModel;
    public static DelivererJobsViewModel delivererJobsViewModel;

    public ViewModelFactory(RemoteRetailerRepository remoteRetailerRepository,RemoteDriverRepository remoteDriverRepository,RemoteCustomerRepository remoteCustomerRepository) {
        this.remoteRetailerRepository = remoteRetailerRepository;
        this.remoteDriverRepository=remoteDriverRepository;
        this.remoteCustomerRepository=remoteCustomerRepository;
    }



    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(RetailerSignUpViewModel.class)) {

            return (T)new RetailerSignUpViewModel(remoteRetailerRepository);

        }else if (modelClass.isAssignableFrom(LoginViewModel.class)) {

            return (T)new LoginViewModel(remoteRetailerRepository);

        } else if (modelClass.isAssignableFrom(PostDeliveryViewModel.class)) {

            return (T)new PostDeliveryViewModel(remoteRetailerRepository);

        }
        else if (modelClass.isAssignableFrom(DelivererSignUpViewModel.class)) {

            return (T)new DelivererSignUpViewModel(remoteDriverRepository);

        }
        else if (modelClass.isAssignableFrom(DelivererLoginViewModel.class)) {

            return (T)new DelivererLoginViewModel(remoteDriverRepository);

        }else if (modelClass.isAssignableFrom(CustomerSignUpViewModel.class)) {

            return (T)new CustomerSignUpViewModel(remoteCustomerRepository);

        }
        else if (modelClass.isAssignableFrom(CustomerLoginViewModel.class)) {

            return (T)new CustomerLoginViewModel(remoteCustomerRepository);

        }
        else if (modelClass.isAssignableFrom(AddCustomerViewModel.class)) {

            return (T)new AddCustomerViewModel(remoteRetailerRepository);

        }
        else if (modelClass.isAssignableFrom(DelivererJobsViewModel.class)) {

            if(delivererJobsViewModel==null){
                delivererJobsViewModel=new DelivererJobsViewModel(remoteDriverRepository);
            }
            return (T)delivererJobsViewModel;

        }
        else if (modelClass.isAssignableFrom(JobsListViewModel.class)) {

            if(jobsListViewModel==null){
                jobsListViewModel=new JobsListViewModel(remoteRetailerRepository);
            }
            return (T)jobsListViewModel;

        }

        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }


}
