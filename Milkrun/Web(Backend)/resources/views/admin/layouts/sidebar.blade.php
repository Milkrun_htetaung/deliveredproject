<div class="main-menu menu-fixed menu-dark menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="nav-item"><a href="{{ route('admin.dashboard') }}"><i class="icon-globe"></i><span
                        class="menu-title">@lang('admin.message43')</span></a></li>

            <li class=" nav-item"><a href="#"><i class="fa fa-user-secret"></i><span class="menu-title"
                                                                                     data-i18n="nav.adminMangement.main">@lang('admin.sub-admin')</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="#"
                           data-i18n="nav.active_ride.1_column">@lang('admin.message401')</a>
                    </li>


                    <li><a class="menu-item" href="#"
                           data-i18n="nav.active_ride.1_column">@lang('admin.role')</a>
                    </li>

                </ul>
            </li>


            <li class="nav-item"><a
                    href="{{ route('drivers.index') }}"><i class="icon-key"></i><span
                        class="menu-title">@lang('admin.DriverManagement')</span></a>
            </li>

            <li class="nav-item"><a
                    href="{{ route('customers.index') }}"><i class="icon-graduation"></i><span
                        class="menu-title">@lang('admin.melvin_customer_message1')</span></a>
            </li>

            <li class="nav-item"><a
                    href="{{ route('retailer.index') }}"><i class="icon-user"></i><span
                        class="menu-title">@lang('admin.melvin_sidebar_message1')</span></a>
            </li>

            <li class="nav-item"><a
                    href="{{ route('orders.index') }}"><i class="icon-organization"></i><span
                        class="menu-title">@lang('admin.melvin_order_message1')</span></a>
            </li>

            <li class="nav-item"><a
                    href="{{ route('stations.index') }}"><i class="fa fa-bell-o"></i><span
                        class="menu-title">@lang('admin.melvin_station_message1')</span></a></li>

            <li class="nav-item"><a
                    href="{{ route('measurements.index') }}"><i class="icon-settings"></i><span
                        class="menu-title">@lang('admin.melvin_measurement_message1')</span></a></li>

            <li class="nav-item"><a href="{{ route('documents.index') }}"><i class="fa fa-file"></i><span
                        class="menu-title">@lang('admin.melvin_sidebar_message2')</span></a></li>

            <li class="nav-item"><a
                    href="{{ route('onesignal.index') }}"><i class="fa fa-bell-o"></i><span
                        class="menu-title">@lang('admin.message85')</span></a></li>

            <li class="nav-item"><a
                    href="{{ route('promotions.index') }}"><i class="fa fa-bell-o"></i><span
                        class="menu-title">@lang('admin.message60')</span></a></li>

            <li class="nav-item"><a
                    href="{{ route('customer_support.index') }}"><i class="fa fa-headphones"></i><span
                        class="menu-title">@lang('admin.message379')</span></a></li>

            <li class="nav-item"><a href="{{ route('cms.index') }}"><i class="fa fa-columns"></i><span
                        class="menu-title">@lang('admin.message208')</span></a></li>


        </ul>
    </div>
</div>
