<?php

namespace App\Http\Controllers\Admin;

use App\Models\Document;
use Auth;
use App;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class DocumentController extends Controller
{
    public function index()
    {
        $documents = Document::latest()->paginate(25);
        return view('admin.document.index', compact('documents'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'documentname' => ['required',
                Rule::unique('language_documents', 'documentname')->where(function ($query) {
                    return $query->where([['locale', '=', App::getLocale()]]);
                })],
        ]);
        $doc = Document::create([
            'documentStatus' => 1,
        ]);
        $this->SaveLanguageDoc($doc->id, $request->documentname);
        return redirect()->back()->with('documentadded', 'Document Added');
    }

    public function SaveLanguageDoc($id, $documentname)
    {
        App\Models\LanguageDocument::updateOrCreate([
            'locale' => App::getLocale(), 'document_id' => $id
        ], [
            'documentname' => $documentname,
        ]);
    }

    public function update(Request $request)
    {
        $id = $request->docId;
        $this->validate($request, [
            'docId' => 'required',
            'documentname' => ['required',
                Rule::unique('language_documents', 'documentname')->where(function ($query) use ($id) {
                    return $query->where([['locale', '=', App::getLocale()], ['document_id', '!=', $id]]);
                })],
        ]);
        $doc = Document::findOrFail($id);
        $this->SaveLanguageDoc($doc->id, $request->documentname);
        return redirect()->back()->with('documentadded', 'Document Added');
    }

}
