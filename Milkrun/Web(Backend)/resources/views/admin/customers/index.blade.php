@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.melvin_customer_message1')</h3>
                </div>

                <div class="col-md-6 col-12">
                    @if(session('moneyAdded'))
                        <div class="col-md-8 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>@lang('admin.message430')</strong>
                        </div>
                    @endif
                </div>


                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            <a href="{{route('customers.create')}}">
                                <button type="button" class="btn btn-icon btn-success mr-1"><i
                                        class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="">
                                    <form action="{{ route('customer.search') }}" method="post">
                                        @csrf
                                        <div class="table_search">
                                            <div class="col-md-2 col-xs-4 form-group ">
                                                <div class="input-group">
                                                    <select class="form-control" name="parameter"
                                                            id="parameter"
                                                            required>
                                                        <option value="1">@lang('admin.RiderName')</option>
                                                        <option value="2">@lang('admin.email')</option>
                                                        <option value="3">@lang('admin.RiderPhone')</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-xs-6 form-group ">
                                                <div class="input-group">
                                                    <input type="text" name="keyword"
                                                           placeholder="@lang('admin.message337')"
                                                           class="form-control col-md-12 col-xs-12" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-2  col-xs-12 form-group ">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"
                                                                                                 aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table
                                            class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>@lang('admin.melvin_customer_message2')</th>
                                                <th>@lang('admin.melvin_customer_message11')</th>
                                                <th>@lang('admin.melvin_customer_message12')</th>
                                                <th>@lang('admin.melvin_customer_message3')</th>
                                                <th>@lang('admin.melvin_customer_message4')</th>
                                                <th>@lang('admin.melvin_customer_message5')</th>
                                                <th>@lang('admin.melvin_customer_message6')</th>
                                                <th>@lang('admin.melvin_customer_message7')</th>
                                                <th>@lang('admin.melvin_customer_message8')</th>
                                                <th>@lang('admin.melvin_customer_message9')</th>
                                                <th>@lang('admin.melvin_customer_message10')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                
                                            @foreach($customers as $customer)
                                                <tr>
                                                    <td>{{ $customer->id }}</td>
                                                    <td>@if(!empty($customer->retailer[0])) {{ $customer->retailer[0]->id }} @endif</td>
                                                    <td>@if(!empty($customer->retailer[0])) {{ $customer->retailer[0]->retailer_shop }}  @endif</td>
                                                    <td>{{ $customer->customer_name }}</td>
                                                    <td>{{ $customer->customer_email }}</td>
                                                    <td>{{ $customer->customer_phone }}</td>
                                                    <td>{{ $customer->created_at->toformatteddatestring() }}</td>
                                                    <td><a href="{{ route('customer.wallet',$customer->id) }}" j>
                                                            @if($customer->wallet_balance)
                                                                {{ $customer->wallet_balance }}
                                                            @else
                                                                0.00
                                                            @endif
                                                        </a>
                                                    </td>
                                                    <td>
                                                        @if ($customer->rating == "0.0")
                                                            @lang('admin.message278')
                                                        @else
                                                            @while($customer->rating>0)
                                                                @if($customer->rating >0.5)
                                                                    <img src="{{ asset("star.png") }}"
                                                                         alt='Whole Star'>
                                                                @else
                                                                    <img src="{{ asset('halfstar.png') }}"
                                                                         alt='Half Star'>
                                                                @endif
                                                                @php $customer->rating--; @endphp
                                                            @endwhile
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($customer->customer_status == 1)
                                                            <label class="label_success">@lang('admin.active')</label>
                                                        @else
                                                            <label class="label_danger">@lang('admin.deactive')</label>
                                                        @endif
                                                    </td>

                                                    <td>
                                                            <span data-target="#sendNotificationModelUser"
                                                                  data-toggle="modal" id="{{ $customer->id }}"><a
                                                                    data-original-title="Send Notification"
                                                                    data-toggle="tooltip"
                                                                    id="{{ $customer->id }}" data-placement="top"
                                                                    class="btn menu-icon btn_detail action_btn"> <i
                                                                        class="fa fa-bell"></i> </a></span>

                                                        <span data-target="#addMoneyModel"
                                                              data-toggle="modal" id="{{ $customer->id }}"><a
                                                                data-original-title="Add Money"
                                                                data-toggle="tooltip"
                                                                id="{{ $customer->id }}" data-placement="top"
                                                                class="btn menu-icon btn_detail action_btn"> <i
                                                                    class="fa fa-money"></i> </a></span>

                                                        <a href="{{ route('customers.edit',$customer->id) }}"
                                                           data-original-title="Edit" data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_edit action_btn"> <i
                                                                class="fa fa-pencil"></i> </a>

                                                        {{--<a href="{{ route('customer.wallet',$customer->id) }}"--}}
                                                        {{--data-original-title="User Wallet" data-toggle="tooltip"--}}
                                                        {{--data-placement="top"--}}
                                                        {{--class="btn menu-icon btn_money action_btn"> <i--}}
                                                        {{--class="fa fa-money"></i> </a>--}}


                                                        {{--<a href="{{ route('customer.show',$customer->id) }}"--}}
                                                        {{--class="btn menu-icon btn_delete action_btn"--}}
                                                        {{--data-original-title="@lang('admin.message68')"--}}
                                                        {{--data-toggle="tooltip"--}}
                                                        {{--data-placement="top"><span--}}
                                                        {{--class="fa fa-user"></span></a>--}}

                                                        @if($customer->customer_status == 1)
                                                            <a href="{{ route('customer.active-deactive',['id'=>$customer->id,'status'=>2]) }}"
                                                               data-original-title="Inactive" data-toggle="tooltip"
                                                               data-placement="top"
                                                               class="btn menu-icon btn_eye_dis action_btn"> <i
                                                                    class="fa fa-eye-slash"></i> </a>
                                                        @else
                                                            <a href="{{ route('customer.active-deactive',['id'=>$customer->id,'status'=>1]) }}"
                                                               data-original-title="Active" data-toggle="tooltip"
                                                               data-placement="top"
                                                               class="btn menu-icon btn_eye action_btn"> <i
                                                                    class="fa fa-eye"></i> </a>
                                                        @endif
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1">{{ $customers->links() }}</div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="sendNotificationModelUser" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel33"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33">@lang('admin.message299')</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="#" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="modal-body">
                        <label>@lang('admin.message63'): </label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="title"
                                   name="title"
                                   placeholder="@lang('admin.message63')" required>
                        </div>

                        <label>@lang('admin.message64'): </label>
                        <div class="form-group">
                           <textarea class="form-control" id="message" name="message"
                                     rows="3"
                                     placeholder="@lang('admin.message64')"></textarea>
                        </div>

                        <label>@lang('admin.message65'): </label>
                        <div class="form-group">
                            <input type="file" class="form-control" id="image"
                                   name="image"
                                   placeholder="@lang('admin.message65')" required>
                            <input type="hidden" name="persion_id" id="persion_id">
                        </div>

                        <label>@lang('admin.message66'): </label>
                        <div class="form-group">
                            <input type="url" class="form-control" id="url"
                                   name="url"
                                   placeholder="@lang('admin.message66')" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                        <input type="submit" class="btn btn-outline-primary btn-lg" value="Send">
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--add money in user wallet--}}
    <div class="modal fade text-left" id="addMoneyModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33">@lang('admin.message200')</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post">
                    @csrf
                    <div class="modal-body">
                        <label>@lang('admin.message203'): </label>
                        <div class="form-group">
                            <select class="form-control" name="payment_method" id="payment_method" required>
                                <option value="1">@lang('admin.message201')</option>
                                <option value="2">@lang('admin.message202')</option>
                            </select>
                        </div>

                        <label>@lang('admin.message205'): </label>
                        <div class="form-group">
                            <input type="text" name="amount" placeholder="@lang('admin.message205')"
                                   class="form-control" required>
                            <input type="hidden" name="add_money_user_id" id="add_money_driver_id">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                        <input type="submit" class="btn btn-outline-primary btn-lg" value="Add">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
