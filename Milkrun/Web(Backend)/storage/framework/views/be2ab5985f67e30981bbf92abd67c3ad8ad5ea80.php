
<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.message61'); ?></h3>
                </div>

                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            <a title="<?php echo app('translator')->getFromJson('admin.message62'); ?>" href="<?php echo e(route('promotions.create')); ?>">
                                <button class="btn btn-icon btn-success mr-1"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>

                </div>

            </div>
            <div class="content-body">

                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="">
                                    <form method="post" action="<?php echo e(route('promotions.search')); ?>">
                                        <?php echo csrf_field(); ?>
                                        <div class="table_search">
                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <input type="text" id="" name="title"
                                                           placeholder="<?php echo app('translator')->getFromJson('admin.message374'); ?>"
                                                           class="form-control col-md-12 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <select class="form-control" name="application"
                                                            id="application">
                                                        <option value="">--<?php echo app('translator')->getFromJson('admin.message72'); ?>--</option>
                                                        <option value="1"><?php echo app('translator')->getFromJson('admin.message69'); ?></option>
                                                        <option value="2"><?php echo app('translator')->getFromJson('admin.message70'); ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <input type="text" id="" name="date"
                                                           placeholder="<?php echo app('translator')->getFromJson('admin.message375'); ?>"
                                                           class="form-control col-md-12 col-xs-12 datepickersearch"
                                                           id="datepickersearch">
                                                </div>
                                            </div>

                                            <div class="col-sm-2  col-xs-12 form-group ">
                                                <button class="btn btn-primary" type="submit" name="seabt12"><i
                                                            class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th><?php echo app('translator')->getFromJson('admin.message63'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message64'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message65'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message66'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message67'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message71'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.action'); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $__currentLoopData = $promotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promotion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td>
                                                        <?php echo e($promotion->title); ?>

                                                    </td>
                                                    <td>
                                                        <span class="map_address"><?php echo e($promotion->message); ?></span>
                                                    </td>
                                                    <td><img src="<?php echo e(asset($promotion->image)); ?>"
                                                             align="center" width="100px" height="80px"
                                                             class="img-radius"
                                                             alt="Promotion Notification Image"></td>
                                                    <td><a class="map_address address_link" target="_blank"
                                                           href="<?php echo e($promotion->url); ?>"><?php echo e($promotion->url); ?></a>
                                                    </td>

                                                    <?php switch($promotion->application):
                                                        case (1): ?>
                                                        <td><?php echo app('translator')->getFromJson('admin.message69'); ?></td>
                                                        <?php break; ?>
                                                        <?php case (2): ?>
                                                        <td><?php echo app('translator')->getFromJson('admin.message70'); ?></td>
                                                        <?php break; ?>
                                                    <?php endswitch; ?>
                                                    <td><?php echo e($promotion->created_at->toformatteddatestring()); ?></td>
                                                    <td>

                                                        <a href="<?php echo e(route('promotions.edit',$promotion->id)); ?>"
                                                           data-original-title="Edit" data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_edit action_btn"> <i
                                                                    class="fa fa-pencil"></i> </a>

                                                        <a href="<?php echo e(route('promotions.delete',$promotion->id)); ?>"
                                                           data-original-title="<?php echo app('translator')->getFromJson('admin.delete'); ?>"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_delete action_btn"> <i
                                                                    class="fa fa-trash"></i> </a>

                                                    </td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1"><?php echo e($promotions->links()); ?></div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>