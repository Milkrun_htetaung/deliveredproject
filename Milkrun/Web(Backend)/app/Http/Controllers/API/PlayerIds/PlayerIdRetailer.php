<?php
/*
app_type ==> 1 For Retailer
app_type ==> 2 For Deliverer
app_type ==> 3 For User
*/

namespace App\Http\Controllers\API\PlayerIds;

use App\Models\User;
use App\Models\Driver;
use App\Models\Retailer;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Client;

class  PlayerIdRetailer extends Controller
{
    public function Update_Player_Id(Request $request)
    {
        $retailer = $request->user('api_retailer');
        $validator = Validator::make($request->all(),
            [
                'player_id' => 'required',
            ]);
        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
        }
        $retailer->player_id = $request->player_id;
        $retailer->save();
        return response()->json(['result' => "1", 'message' => "Player Id Updated", 'data' => []]);
    }
}
