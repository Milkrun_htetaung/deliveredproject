<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetailersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retailers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('retailer_shop')->unique();
            $table->string('retailer_contact_name');
            $table->string('retailer_email')->index();
            $table->string('retailer_phone')->nullable();
            $table->string('retailer_image')->nullable();
            $table->string('retailer_password');
            $table->string('retailer_shop_address')->unique();
            $table->string('ReferralCode');
            $table->tinyInteger('rating')->default(0);
            $table->unsignedInteger( 'total_trips' )->default( 0 );
            $table->unsignedInteger( 'total_earnings' )->default( 0 );
            $table->unsignedInteger('country_id')->nullable();
            $table->tinyInteger('login_type')->default(0);
            $table->tinyInteger( 'login_logout' )->default( 0 );
            $table->tinyInteger( 'online_offline' )->default( 0 );
            $table->tinyInteger( 'retailer_verify_status' )->default( 0 );
            $table->tinyInteger('retailer_status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retailers');
    }
}
