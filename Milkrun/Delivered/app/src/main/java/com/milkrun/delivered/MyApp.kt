package com.milkrun.delivered

import android.app.Activity
import android.app.Application
import com.milkrun.delivered.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class MyApp : Application() , HasActivityInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingActivityInjector

    override fun onCreate() {
        super.onCreate()
        //setting font for the whole app
        FontsOverride.setDefaultFont(this, "DEFAULT", "avantn.ttf")


        //initializing dagger
        DaggerAppComponent.builder().create(this).inject(this)
    }
}
