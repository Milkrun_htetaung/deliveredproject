@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.documents')</h3>
                </div>
                <div class="col-md-6 col-12">
                    @if(session('documentadded'))
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>@lang('admin.documentadded')</strong>
                        </div>
                    @endif
                </div>

                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            <button type="button" class="btn btn-icon btn-success mr-1"
                                    title="@lang('admin.Add') @lang('admin.document')" data-toggle="modal"
                                    data-target="#inlineForm">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>


            </div>

            <div class="content-body">

                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>@lang('admin.documentname')</th>
                                                <th>@lang('admin.Status')</th>
                                                <th>@lang('admin.action')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php $sr = $documents->firstItem() @endphp
                                            @foreach($documents as $document)
                                                <tr>
                                                    <td>{{ $sr }}</td>
                                                    <td>@if(empty($document->LanguageSingle))
                                                            <span style="color:red">{{ trans('admin.name-not-given') }}</span>
                                                            <span class="text-primary">( In {{ $document->LanguageAny->LanguageName->name }}
                                                                : {{ $document->LanguageAny->documentname }}
                                                                )</span>
                                                        @else
                                                            {{ $document->LanguageSingle->documentname }}
                                                        @endif
                                                    </td>

                                                    <td>
                                                        @if($document->documentStatus == 1)
                                                            <label class="label_success">@lang('admin.active')</label>
                                                        @else
                                                            <label class="label_danger">@lang('admin.deactive')</label>
                                                        @endif
                                                    </td>
                                                    <td>

                                                        <button type="button"
                                                                class="btn menu-icon btn_edit action_btn"
                                                                onclick="EditDoc(this)"
                                                                data-ID="{{ $document->id }}"
                                                                data-Name="@if($document->LanguageSingle) {{ $document->LanguageSingle->documentname }}@endif">
                                                            <i class="fa fa-pencil"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @php $sr++  @endphp
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1">{{ $documents->links() }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>



    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600"
                           id="myModalLabel33"><b> @lang('admin.Add')</b> @lang('admin.document')</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" enctype="multipart/form-data" action="{{ route('documents.store') }}">
                    @csrf
                    <div class="modal-body">

                        <label>@lang('admin.documentname') : <span class="danger">*</span></label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" name="documentname"
                                   placeholder="@lang('admin.documentname')" required>
                            @if ($errors->has('documentname'))
                                <label class="danger">{{ $errors->first('documentname') }}</label>
                            @endif
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal"
                               value="@lang('admin.message366')">
                        <input type="submit" class="btn btn-outline-primary btn-lg" value="@lang('admin.message365')">
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade text-left" id="EditDOc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600"
                           id="myModalLabel33"><b> @lang('admin.message417')</b></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" enctype="multipart/form-data" action="{{ route('doc.update') }}">
                    @csrf
                    <div class="modal-body">

                        <label>@lang('admin.documentname') : <span class="danger">*</span></label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" name="documentname"
                                   placeholder="@lang('admin.documentname')" required>
                            <input type="hidden" id="docId" name="docId">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal"
                               value="@lang('admin.message366')">
                        <input type="submit" class="btn btn-outline-primary btn-lg" value="@lang('admin.update')">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        function EditDoc(obj) {
            let ID = obj.getAttribute('data-ID');
            let Name = obj.getAttribute('data-Name');
            $(".modal-body #name").val(Name);
            $(".modal-body #docId").val(ID);
            $('#EditDOc').modal('show');
        }
    </script>
@endsection