<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $hidden = array('pivot');
    protected $guarded = [];

    public function LanguageAny()
    {
        return $this->hasOne(LanguageDocument::class);
    }

    public function LanguageSingle()
    {
        return $this->hasOne(LanguageDocument::class)->where([['locale', '=', App::getLocale()]]);
    }
}
