<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.melvin_measurement_message1'); ?></h3>
                </div>

                <div class="col-md-6 col-12">
                    <?php if(session('moneyAdded')): ?>
                        <div class="col-md-8 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo app('translator')->getFromJson('admin.message430'); ?></strong>
                        </div>
                    <?php endif; ?>
                </div>


                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            <a href="<?php echo e(route('retailer.create')); ?>">
                                <button type="button" class="btn btn-icon btn-success mr-1"><i
                                        class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table
                                            class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_measurement_message2'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_measurement_message3'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_measurement_message8'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_measurement_message4'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_measurement_message5'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.melvin_measurement_message7'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.created_at'); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $__currentLoopData = $measurements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $measurement): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($measurement->id); ?></td>
                                                    <td><?php echo e($measurement->measure_unit_name); ?></td>
                                                    <td><?php echo e($measurement->measure_unit); ?></td>
                                                    <td><?php echo e($measurement->measure_unit_value); ?></td>
                                                    <td>
                                                        <?php if($measurement->measure_unit_status == 1): ?>
                                                            <label class="label_success"><?php echo app('translator')->getFromJson('admin.active'); ?></label>
                                                        <?php else: ?>
                                                            <label class="label_danger"><?php echo app('translator')->getFromJson('admin.deactive'); ?></label>
                                                        <?php endif; ?>
                                                    </td>

                                                    <td>

                                                        <a href="<?php echo e(route('measurements.edit',$measurement->id)); ?>"
                                                           data-original-title="Edit" data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_edit action_btn"> <i
                                                                class="fa fa-pencil"></i> </a>

                                                        <?php if($measurement->measure_unit_status == 1): ?>
                                                            <a href="<?php echo e(route('measurement.active-deactive',['id'=>$measurement->id,'status'=>2])); ?>"
                                                               data-original-title="Inactive" data-toggle="tooltip"
                                                               data-placement="top"
                                                               class="btn menu-icon btn_eye_dis action_btn"> <i
                                                                    class="fa fa-eye-slash"></i> </a>
                                                        <?php else: ?>
                                                            <a href="<?php echo e(route('measurement.active-deactive',['id'=>$measurement->id,'status'=>1])); ?>"
                                                               data-original-title="Active" data-toggle="tooltip"
                                                               data-placement="top"
                                                               class="btn menu-icon btn_eye action_btn"> <i
                                                                    class="fa fa-eye"></i> </a>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td><?php echo e($measurement->created_at->toformatteddatestring()); ?></td>
                                                </tr>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                
                                    
                                
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>