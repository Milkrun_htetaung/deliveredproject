<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Measurement;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MeasurementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $measurements = Measurement::get();
        return view('admin.measurements.index', compact('measurements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Measurement  $measurement
     * @return \Illuminate\Http\Response
     */
    public function show(Measurement $measurement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Measurement  $measurement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $measurement = Measurement::findOrFail($id);
        return view('admin.measurements.edit', compact('measurement'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Measurement  $measurement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'measure_unit_name' => "required",
            'measure_unit' => "required",
            'measure_unit_value' => "required",
        ]);
        $measurement = Measurement::findOrFail($id);
        $measurement->measure_unit_name = $request->measure_unit_name;
        $measurement->measure_unit = $request->measure_unit;
        $measurement->measure_unit_value = $request->measure_unit_value;
        $measurement->save();
        return redirect()->back()->with('measureupdated', 'Details Updated Added');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Measurement  $measurement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Measurement $measurement)
    {
        //
    }

    public function ChangeStatus($id, $status)
    {
        $validator = Validator::make(
            [
                'id' => $id,
                'status' => $status,
            ],
            [
                'id' => ['required'],
                'status' => ['required', 'integer', 'between:1,2'],
            ]);
        if ($validator->fails()) {
            return redirect()->back();
        }
        $measurement = Measurement::findOrFail($id);
        $measurement->measure_unit_status = $status;
        $measurement->save();
        return redirect()->back();
    }


}
