<?php
/*
app_type ==> 1 For Retailer
app_type ==> 2 For Deliverer
app_type ==> 3 For User
*/

namespace App\Http\Controllers\API\Common_APIs;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CommonController extends Controller
{
    public function Country_Data(Request $request)
    {
        $countries = Country::all();
        if (!empty($countries)):
            return response()->json(['result' => "1", 'message' => 'Country Data', 'data' => $countries]);
        else:
            return response()->json(['result' => "0", 'message' => 'No Data Found', 'data' => []]);
        endif;

    }

    public function Send_Otp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'app_type' => 'required|integer',
            'phone' => 'required|regex:/^[0-9+]+$/',
        ]);
        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
        }
        if ($request->app_type == 1) {
            $validator = Validator::make($request->all(),
                [
                    'phone' => ['required', 'unique:retailers,retailer_phone'],
                ]);
            if ($validator->fails()) {
                $errors = $validator->messages()->all();
                return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
            }
            $otp = "2018";
            return response()->json(['result' => "1", 'message' => "OTP Sent", 'data' => ['otp_code' => $otp , 'app_type' => "1"]]);
        } else {
            $validator = Validator::make($request->all(),
                [
                        'phone' => ['required', 'unique:drivers,driver_phone'],
                ]);
            if ($validator->fails()) {
                $errors = $validator->messages()->all();
                return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
            } else {
                $otp = "2018";
                return response()->json(['result' => "1", 'message' => "OTP Sent", 'data' => ['otp_code' => $otp , 'app_type' => "2"]]);
            }
        }
    }
}
