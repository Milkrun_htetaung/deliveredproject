@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.message62')</h3>
                </div>
                <div class="col-md-6 col-12">
                    @if(session('notification'))
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>@lang('admin.message76')</strong>
                        </div>
                    @endif
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('promotions.index') }}">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="content-body">
                <section id="basic-tabs-components">
                    <div class="row match-height">
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <ul class="nav nav-tabs nav-top-border no-hover-bg">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="base-tab11" data-toggle="tab"
                                                   aria-controls="tab11" href="#tab11" aria-expanded="true"><i
                                                            class="fa fa-users"></i>@lang('admin.message301')</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content px-1 pt-1">
                                            <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true"
                                                 aria-labelledby="base-tab11">
                                                <form method="POST" class="steps-validation wizard-notification"
                                                      enctype="multipart/form-data" action="{{ route('promotions.store') }}">
                                                    @csrf
                                                    <fieldset>


                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="location3">@lang('admin.message67') :</label>
                                                                    <select class="form-control" name="application"
                                                                            id="application" onchange="DriverNotification(this.value)" required>
                                                                        <option value="">--@lang('admin.message72')--</option>
                                                                        <option value="1">@lang('admin.message69')</option>
                                                                        <option value="2">@lang('admin.message70')</option>
                                                                    </select>

                                                                    @if ($errors->has('application'))
                                                                        <label class="danger">{{ $errors->first('application') }}</label>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="lastName3">
                                                                        @lang('admin.message63') :
                                                                        <span class="danger">*</span>
                                                                    </label>
                                                                    <input type="text" class="form-control" id="title"
                                                                           name="title"
                                                                           placeholder="@lang('admin.message63')" required>
                                                                    @if ($errors->has('title'))
                                                                        <label class="danger">{{ $errors->first('title') }}</label>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="emailAddress5">
                                                                        @lang('admin.message64') :
                                                                        <span class="danger">*</span>
                                                                    </label>
                                                                    <textarea class="form-control" id="message" name="message"
                                                                              rows="3"
                                                                              placeholder="@lang('admin.message64')"></textarea>
                                                                    @if ($errors->has('message'))
                                                                        <label class="danger">{{ $errors->first('message') }}</label>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="ProfileImage">
                                                                        @lang('admin.message65') :
                                                                        <span class="danger">*</span>
                                                                    </label>
                                                                    <input type="file" class="form-control" id="image"
                                                                           name="image"
                                                                           placeholder="@lang('admin.message65')" required>
                                                                    @if ($errors->has('image'))
                                                                        <label class="danger">{{ $errors->first('image') }}</label>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="emailAddress5">
                                                                        @lang('admin.message66') :
                                                                        <span class="danger">*</span>
                                                                    </label>
                                                                    <input type="url" class="form-control" id="url"
                                                                           name="url"
                                                                           placeholder="@lang('admin.message66')" required>
                                                                    @if ($errors->has('url'))
                                                                        <label class="danger">{{ $errors->first('url') }}</label>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            {{--<div class="col-md-6">--}}
                                                            {{--<div class="form-group">--}}
                                                            {{--<label for="location3">@lang('admin.message73') :</label>--}}
                                                            {{--<select class="form-control" name="application"--}}
                                                            {{--id="application" required>--}}
                                                            {{--<option value="1">@lang('admin.message74')</option>--}}
                                                            {{--<option value="2">@lang('admin.message75')</option>--}}
                                                            {{--</select>--}}

                                                            {{--@if ($errors->has('application'))--}}
                                                            {{--<label class="danger">{{ $errors->first('application') }}</label>--}}
                                                            {{--@endif--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}

                                                        </div>

                                                    </fieldset>
                                                    <div class="form-actions right">
                                                        <button type="submit" class="btn btn-primary">
                                                            <i class="fa fa-check-square-o"></i> Save
                                                        </button>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection