<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RetailerRequest;
use App\Models\Onesignal;
use App\Models\Retailer;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class RetailerController extends Controller
{
    public function index()
    {
        $retailers = Retailer::latest()->paginate(25);
        return view('admin.retailer.index', compact('retailers'));
    }

    public function create()
    {

        $countries = Country::get();
        return view('admin.retailer.create', compact('countries'));
    }

    public function store(RetailerRequest $request)
    {
        $password = Hash::make($request->retailer_password);
        $request->file('profile');
        $profile_image = $request->profile->store('retailer', 'public');
        $user = Retailer::create([
            'retailer_shop' => $request->retailer_shop,
            'retailer_shop_address' => $request->retailer_shop_address,
            'retailer_contact_name' => $request->retailer_contact_name,
            'retailer_phone' => $request->country . $request->retailer_phone,
            'retailer_email' => $request->retailer_email,
            'retailer_password' => $password,
            'retailer_image' => $profile_image,
            'login_type' => 2,
            'ReferralCode' => Retailer::GenrateReferCode(),
        ]);
        return redirect()->back()->with('rideradded', 'Rider Added');
    }

    public function show($id)
    {
        $user = Retailer::findOrFail($id);
        $bookings = Booking::where([['user_id', '=', $id]])->whereIn('booking_status', [1005])->paginate(5);
        return view('admin.retailer.show', compact('user', 'bookings'));
    }

    public function edit($id)
    {
        $retailer = Retailer::findOrFail($id);
        return view('admin.retailer.edit', compact('retailer'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'retailer_shop' => "required",
            'retailer_contact_name' => "required",
            'retailer_email' => ['required', 'max:255', 'unique:retailers,retailer_email'],
            'retailer_phone' => ['required', 'max:255', 'unique:retailers,retailer_phone'],
            'retailer_password' => 'required_if:edit_password,1',
            'retailer_shop_address' => "required"
        ]);
        $retailer = Retailer::findOrFail($id);
        $retailer->retailer_shop = $request->retailer_shop;
        $retailer->retailer_contact_name = $request->retailer_contact_name;
        $retailer->retailer_email = $request->retailer_email;
        $retailer->retailer_shop_address = $request->retailer_shop_address;
        if ($request->retailer_password == 1) {
            $password = Hash::make($request->retailer_password);
            $retailer->retailer_password = $password;
        }
        if ($request->hasFile('profile')) {
            $request->file('profile');
            $profile_image = $request->profile->store('user', 'public');
            $retailer->retailer_image = $profile_image;
        }
        $retailer->save();
        return redirect()->back()->with('rideradded', 'Rider Added');
    }

    public function ChangeStatus($id, $status)
    {
        $validator = Validator::make(
            [
                'id' => $id,
                'status' => $status,
            ],
            [
                'id' => ['required'],
                'status' => ['required', 'integer', 'between:1,2'],
            ]);
        if ($validator->fails()) {
            return redirect()->back();
        }
        $retailer = Retailer::findOrFail($id);
        $retailer->retailer_status = $status;
        $retailer->save();
        return redirect()->back();
    }

    public function Wallet($id)
    {
        $user = Retailer::findOrFail($id);
        $wallet_transactions = UserWalletTransaction::where([['user_id', '=', $id]])->paginate(25);
        return view('admin.retailer.wallet', compact('wallet_transactions', 'user'));
    }

    public function AddWalletMoney(Request $request)
    {
        $request->validate([
            'add_money_user_id' => 'required',
            'amount' => 'required|numeric',
        ]);
        $user = Retailer::findOrFail($request->add_money_user_id);
        $wallet = $user->wallet_balance;
        $user->wallet_balance = $wallet + $request->amount;
        $user->save();
        UserWalletTransaction::create([
            'merchant_id' => $merchant_id,
            'user_id' => $request->add_money_user_id,
            'platfrom' => 1,
            'amount' => $request->amount,
            'type' => 1,
        ]);
        return redirect()->back()->with('moneyAdded', 'Money Added In Driver Wallet');
    }

    public function Serach(Request $request)
    {
        $request->validate([
            'keyword' => "required",
            'parameter' => "required|integer|between:1,3",
        ]);
        switch ($request->parameter) {
            case "1":
                $parameter = "retailer_shop";
                break;
            case "2":
                $parameter = "retailer_email";
                break;
            case "3":
                $parameter = "retailer_phone";
                break;
        }
        $query = Retailer::where([['created_at', '!=', ""]]);
        if ($request->keyword) {
            $query->where($parameter, 'like', '%' . $request->keyword . '%');
        }
        $retailers = $query->paginate(25);
        return view('admin.retailer.index', compact('retailers'));
    }
}
