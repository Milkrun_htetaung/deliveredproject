<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="ApporioTaxi">
    <meta name="keywords" content="ApporioTaxi">
    <title><?php echo e(Auth::user()->name); ?></title>
    <link rel="icon" href="<?php echo e(asset('images/favicon.png')); ?>">
    <link rel="icon" type="image/x-icon" href="<?php echo e(asset('images/favicon.png')); ?>">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,400,500,700"
          rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/vendor.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/themeapp.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/menu.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/gradient.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('fonts/meteocons/style.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/datatables.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/select2.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/datedropper.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/style.css')); ?>">
</head>
<div id="loader1" class="loader1" style="display: none"></div>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click"
      data-menu="vertical-menu" data-col="2-columns">
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a
                            class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                                class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item"><a class="navbar-brand" href="<?php echo e(route('admin.dashboard')); ?>"><img class="brand-logo"
                                                                                                      alt="admin logo"
                                                                                                      src="<?php echo e(asset('images/logo-1.png')); ?>">
                        <h3 class="brand-text"><?php echo e(Auth::user()->name); ?></h3></a></li>
                <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse"
                                                  data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a></li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs"
                                                              href="#"><i class="ft-menu"> </i></a></li>

                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i
                                    class="ficon ft-maximize"></i></a></li>
                </ul>

                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-language nav-item">

                        <a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"><span><?php echo app('translator')->getFromJson('admin.language'); ?>
                                (<?php echo e(strtoupper(App::getLocale())); ?>)</span><span
                                    class="selected-language"></span></a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                            <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <a class="dropdown-item selected-language"
                                   href="<?php echo e(route('admin.language',$language->locale)); ?>"><?php echo e($language->name); ?></a>
                                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>


                    </li>
                    <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link"
                                                                   href="#" data-toggle="dropdown"><span
                                    class="avatar avatar-online"><img
                                        src="<?php echo e(asset('images/logo-1.png')); ?>"
                                        alt="avatar"><i></i></span><span
                                    class="user-name"><?php echo e(Auth::user()->name); ?></span></a>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#"><i
                                        class="ft-user"></i> Edit Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?php echo e(route('admin.logout')); ?>"><i class="ft-power"></i>
                                Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
