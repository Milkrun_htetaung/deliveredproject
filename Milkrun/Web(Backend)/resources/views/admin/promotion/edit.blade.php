@extends('merchant.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.message422')</h3>
                </div>
                <div class="col-md-6 col-12">
                    @if(session('notification'))
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>@lang('admin.message423')</strong>
                        </div>
                    @endif
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('promotions.index') }}">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data"
                                              action="{{route('promotions.update', $promotion->id)}}">
                                            {{method_field('PUT')}}
                                            @csrf
                                            <fieldset>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="lastName3">
                                                                @lang('admin.message63') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="title"
                                                                   name="title"
                                                                   placeholder="@lang('admin.message63')" value="{{ $promotion->title }}" required>
                                                            @if ($errors->has('title'))
                                                                <label class="danger">{{ $errors->first('title') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                @lang('admin.message64') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <textarea class="form-control" id="message" name="message"
                                                                      rows="3"
                                                                      placeholder="@lang('admin.message64')">{{ $promotion->message }}</textarea>
                                                            @if ($errors->has('message'))
                                                                <label class="danger">{{ $errors->first('message') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ProfileImage">
                                                                @lang('admin.message65') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="file" class="form-control" id="image"
                                                                   name="image"
                                                                   placeholder="@lang('admin.message65')">
                                                            @if ($errors->has('image'))
                                                                <label class="danger">{{ $errors->first('image') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                @lang('admin.message66') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="url" class="form-control" id="url"
                                                                   name="url"
                                                                   placeholder="@lang('admin.message66')" value="{{ $promotion->url }}" required>
                                                            @if ($errors->has('url'))
                                                                <label class="danger">{{ $errors->first('url') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                </div>


                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection