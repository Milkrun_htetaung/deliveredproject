@extends('admin.layouts.main')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/summernote.css') }}">
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 ">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.message209') (@lang('admin.message459') {{ strtoupper(Config::get('app.locale')) }})</h3>
                </div>
                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('cms.index') }}">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data" action="{{ route('cms.store') }}">
                                            @csrf
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                @lang('admin.message210') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <select class="form-control" name="page" id="page" required>
                                                                <option value="">--@lang('admin.message211')--</option>
                                                                @foreach($pages as $page)
                                                                    <option value="{{ $page->slug }}">{{ $page->page }}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('page'))
                                                                <label class="danger">{{ $errors->first('page') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                @lang('admin.message213') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <select class="form-control" name="application"
                                                                    id="application" required>
                                                                <option value="2">Driver</option>
                                                                <option value="1">User</option>
                                                            </select>
                                                            @if ($errors->has('application'))
                                                                <label class="danger">{{ $errors->first('application') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="lastName3">
                                                                @lang('admin.message212') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="title"
                                                                   name="title" placeholder="@lang('admin.message212')"
                                                                   required>
                                                            @if ($errors->has('title'))
                                                                <label class="danger">{{ $errors->first('title') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                @lang('admin.message214') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <textarea class="summernote" id="description"
                                                                      name="description" rows="3"
                                                                      placeholder="@lang('admin.description')"></textarea>
                                                            @if ($errors->has('description'))
                                                                <label class="danger">{{ $errors->first('description') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>


                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection