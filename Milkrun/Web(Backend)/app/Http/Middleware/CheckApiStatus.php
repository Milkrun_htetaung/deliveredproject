<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Admin;

class CheckApiStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
       $public_key = $request->header('public_key');
       $secret_key = $request->header('secret_key');
       $Admin_setting = Admin::first();
       if( $Admin_setting->public_key != $public_key || $Admin_setting->secret_key  != $secret_key )
       {
        return response()->json(['result' => "0", 'message' => "unauthorised request", 'data' => []]);
       }
       return $next($request);
    }
}
