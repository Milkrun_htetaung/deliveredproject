
<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/summernote.css')); ?>">
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 ">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.message209'); ?> (<?php echo app('translator')->getFromJson('admin.message459'); ?> <?php echo e(strtoupper(Config::get('app.locale'))); ?>)</h3>
                </div>
                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <a href="<?php echo e(route('cms.index')); ?>">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data" action="<?php echo e(route('cms.store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                <?php echo app('translator')->getFromJson('admin.message210'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <select class="form-control" name="page" id="page" required>
                                                                <option value="">--<?php echo app('translator')->getFromJson('admin.message211'); ?>--</option>
                                                                <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="<?php echo e($page->slug); ?>"><?php echo e($page->page); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                            <?php if($errors->has('page')): ?>
                                                                <label class="danger"><?php echo e($errors->first('page')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                <?php echo app('translator')->getFromJson('admin.message213'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <select class="form-control" name="application"
                                                                    id="application" required>
                                                                <option value="2">Driver</option>
                                                                <option value="1">User</option>
                                                            </select>
                                                            <?php if($errors->has('application')): ?>
                                                                <label class="danger"><?php echo e($errors->first('application')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="lastName3">
                                                                <?php echo app('translator')->getFromJson('admin.message212'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="title"
                                                                   name="title" placeholder="<?php echo app('translator')->getFromJson('admin.message212'); ?>"
                                                                   required>
                                                            <?php if($errors->has('title')): ?>
                                                                <label class="danger"><?php echo e($errors->first('title')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                <?php echo app('translator')->getFromJson('admin.message214'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <textarea class="summernote" id="description"
                                                                      name="description" rows="3"
                                                                      placeholder="<?php echo app('translator')->getFromJson('admin.description'); ?>"></textarea>
                                                            <?php if($errors->has('description')): ?>
                                                                <label class="danger"><?php echo e($errors->first('description')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>


                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>