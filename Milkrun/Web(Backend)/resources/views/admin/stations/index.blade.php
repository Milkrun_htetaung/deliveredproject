@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.melvin_station_message2')</h3>
                </div>

                <div class="col-md-6 col-12">
                    @if(session('moneyAdded'))
                        <div class="col-md-8 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>@lang('admin.message430')</strong>
                        </div>
                    @endif
                </div>

                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            <a href="{{route('stations.create')}}">
                                <button type="button" class="btn btn-icon btn-success mr-1"><i
                                        class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="">
                                    <form action="{{ route('retailer.search') }}" method="post">
                                        @csrf
                                        <div class="table_search">
                                            <div class="col-md-2 col-xs-4 form-group ">
                                                <div class="input-group">
                                                    <select class="form-control" name="parameter"
                                                            id="parameter"
                                                            required>
                                                        <option value="1">@lang('admin.RiderName')</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-xs-6 form-group ">
                                                <div class="input-group">
                                                    <input type="text" name="keyword"
                                                           placeholder="@lang('admin.message337')"
                                                           class="form-control col-md-12 col-xs-12" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-2  col-xs-12 form-group ">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"
                                                                                                 aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table
                                            class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>@lang('admin.melvin_station_message3')</th>
                                                <th>@lang('admin.melvin_station_message4')</th>
                                                <th>@lang('admin.melvin_station_message5')</th>
                                                <th>@lang('admin.melvin_station_message6')</th>
                                                <th>@lang('admin.Registerdate')</th>
                                                <th>@lang('admin.lastUpdate')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($stations as $station)
                                                <tr>
                                                    <td>{{ $station->id }}</td>
                                                    <td>{{ $station->station_name }}</td>
                                                    <td>{{ $station->station_address }}</td>
                                                    <td>
                                                        @if($station->station_status == 1)
                                                            <label class="label_success">@lang('admin.active')</label>
                                                        @else
                                                            <label class="label_danger">@lang('admin.deactive')</label>
                                                        @endif
                                                    </td>
                                                    <td>{{ $station->created_at->toformatteddatestring() }}</td>
                                                    <td>{{ $station->updated_at->toformatteddatestring() }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1">{{ $stations->links() }}</div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>


@endsection
