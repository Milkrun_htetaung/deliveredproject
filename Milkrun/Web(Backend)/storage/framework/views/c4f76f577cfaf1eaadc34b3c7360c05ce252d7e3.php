
<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.documents'); ?></h3>
                </div>
                <div class="col-md-6 col-12">
                    <?php if(session('documentadded')): ?>
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo app('translator')->getFromJson('admin.documentadded'); ?></strong>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            <button type="button" class="btn btn-icon btn-success mr-1"
                                    title="<?php echo app('translator')->getFromJson('admin.Add'); ?> <?php echo app('translator')->getFromJson('admin.document'); ?>" data-toggle="modal"
                                    data-target="#inlineForm">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>


            </div>

            <div class="content-body">

                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo app('translator')->getFromJson('admin.documentname'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.Status'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.action'); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $sr = $documents->firstItem() ?>
                                            <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($sr); ?></td>
                                                    <td><?php if(empty($document->LanguageSingle)): ?>
                                                            <span style="color:red"><?php echo e(trans('admin.name-not-given')); ?></span>
                                                            <span class="text-primary">( In <?php echo e($document->LanguageAny->LanguageName->name); ?>

                                                                : <?php echo e($document->LanguageAny->documentname); ?>

                                                                )</span>
                                                        <?php else: ?>
                                                            <?php echo e($document->LanguageSingle->documentname); ?>

                                                        <?php endif; ?>
                                                    </td>

                                                    <td>
                                                        <?php if($document->documentStatus == 1): ?>
                                                            <label class="label_success"><?php echo app('translator')->getFromJson('admin.active'); ?></label>
                                                        <?php else: ?>
                                                            <label class="label_danger"><?php echo app('translator')->getFromJson('admin.deactive'); ?></label>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>

                                                        <button type="button"
                                                                class="btn menu-icon btn_edit action_btn"
                                                                onclick="EditDoc(this)"
                                                                data-ID="<?php echo e($document->id); ?>"
                                                                data-Name="<?php if($document->LanguageSingle): ?> <?php echo e($document->LanguageSingle->documentname); ?><?php endif; ?>">
                                                            <i class="fa fa-pencil"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <?php $sr++  ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1"><?php echo e($documents->links()); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>



    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600"
                           id="myModalLabel33"><b> <?php echo app('translator')->getFromJson('admin.Add'); ?></b> <?php echo app('translator')->getFromJson('admin.document'); ?></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" enctype="multipart/form-data" action="<?php echo e(route('documents.store')); ?>">
                    <?php echo csrf_field(); ?>
                    <div class="modal-body">

                        <label><?php echo app('translator')->getFromJson('admin.documentname'); ?> : <span class="danger">*</span></label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" name="documentname"
                                   placeholder="<?php echo app('translator')->getFromJson('admin.documentname'); ?>" required>
                            <?php if($errors->has('documentname')): ?>
                                <label class="danger"><?php echo e($errors->first('documentname')); ?></label>
                            <?php endif; ?>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal"
                               value="<?php echo app('translator')->getFromJson('admin.message366'); ?>">
                        <input type="submit" class="btn btn-outline-primary btn-lg" value="<?php echo app('translator')->getFromJson('admin.message365'); ?>">
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade text-left" id="EditDOc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600"
                           id="myModalLabel33"><b> <?php echo app('translator')->getFromJson('admin.message417'); ?></b></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" enctype="multipart/form-data" action="<?php echo e(route('doc.update')); ?>">
                    <?php echo csrf_field(); ?>
                    <div class="modal-body">

                        <label><?php echo app('translator')->getFromJson('admin.documentname'); ?> : <span class="danger">*</span></label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" name="documentname"
                                   placeholder="<?php echo app('translator')->getFromJson('admin.documentname'); ?>" required>
                            <input type="hidden" id="docId" name="docId">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal"
                               value="<?php echo app('translator')->getFromJson('admin.message366'); ?>">
                        <input type="submit" class="btn btn-outline-primary btn-lg" value="<?php echo app('translator')->getFromJson('admin.update'); ?>">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        function EditDoc(obj) {
            let ID = obj.getAttribute('data-ID');
            let Name = obj.getAttribute('data-Name');
            $(".modal-body #name").val(Name);
            $(".modal-body #docId").val(ID);
            $('#EditDOc').modal('show');
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>