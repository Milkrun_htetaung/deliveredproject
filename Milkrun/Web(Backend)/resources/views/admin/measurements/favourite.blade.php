@extends('merchant.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">{{ $user->UserName }} @lang('admin.message5')</h3>

                </div>
                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('retailer.index') }}">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="content-body">

                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>@lang('admin.message5')</th>
                                                <th>@lang('admin.message6')</th>
                                                <th>@lang('admin.message12')</th>
                                                <th>@lang('admin.message7')</th>
                                                <th>@lang('admin.message8')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($user->FavouriteLocation as $location)
                                                <tr>
                                                    <td>{{ $location->location }}</td>
                                                    @switch($location->category)
                                                        @case(1)
                                                        <td>@lang('admin.message9')</td>
                                                        @break
                                                        @case(2)
                                                        <td>@lang('admin.message10')</td>
                                                        @break
                                                        @case(3)
                                                        <td>@lang('admin.message11')</td>
                                                        @break
                                                    @endswitch
                                                    <td>
                                                        @if($location->other_name)
                                                           {{ $location->other_name }}
                                                        @else
                                                            ------
                                                        @endif
                                                    </td>
                                                    <td>{{ $location->created_at }}</td>
                                                    <td>{{ $location->updated_at }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection
