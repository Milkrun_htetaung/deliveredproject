//
//  ViewController.swift
//  Delivered
//
//  Created by Htet Aung on 29/4/19.
//  Copyright © 2019 Milkrun. All rights reserved.
//

import UIKit
import MaterialComponents
class ViewController: UIViewController {

    @IBOutlet weak var delivererLbl: UILabel!
    @IBOutlet weak var delivererBtn: UIView!
    //will be used for expending width when delivererBtn was clicked
    @IBOutlet weak var delivererBtnWidthConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var customerLbl: UILabel!
    @IBOutlet weak var customerBtn: UIView!
    //will be used for expending width when customerbtn was clicked
    @IBOutlet weak var customerBtnWidthConstraint: NSLayoutConstraint!
    
    
    /*
     variable for retailer btn
     */
    @IBOutlet weak var retailerlbl: UILabel!
    @IBOutlet weak var retailerBtn: UIView!
    //will be used for expending width when retailerbtn was clicked
    @IBOutlet weak var retailerBtnWidthConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
         To listen touch event from UIView for retailer btn
        */
        let retailerGesture = UITapGestureRecognizer(target: self, action:  #selector (self.onRetailerClick(_:)))
        retailerBtn.addGestureRecognizer(retailerGesture)

    
        /**
         To listen touch event from UIView for retailer btn
         */
        let customerGesture = UITapGestureRecognizer(target: self, action:  #selector (self.onCustomerClick(_:)))
        customerBtn.addGestureRecognizer(customerGesture)

        /**
         To listen touch event from UIView for retailer btn
         */
        let delivererGesture = UITapGestureRecognizer(target: self, action:  #selector (self.onDelivererClick(_:)))
        delivererBtn.addGestureRecognizer(delivererGesture)

    }
  
    //handling when retailer btn  clicked
    @objc func onRetailerClick(_ sender:UITapGestureRecognizer){
        //changing the width of parent view
        retailerBtnWidthConstraint.constant=200
        //making  the label  visible
        retailerlbl.isHidden=false
        
    }
    
    //handling when customerbtn  clicked
    @objc func onCustomerClick(_ sender:UITapGestureRecognizer){
        //changing the width of parent view
        customerBtnWidthConstraint.constant=200
        //making  the label  visible
        customerLbl.isHidden=false
        
    }
    
    //handling when delivererbtn  clicked
    @objc func onDelivererClick(_ sender:UITapGestureRecognizer){
        //changing the width of parent view
        delivererBtnWidthConstraint.constant=200
        //making  the label  visible
        delivererLbl.isHidden=false
        
    }
  
}

