<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Retailer extends Authenticatable
{
    use Notifiable, HasApiTokens;
    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token','pivot'
    ];


    public function findForPassport($user_cred = null)
    {
        return Retailer::where([['retailer_phone', '=', $user_cred],['retailer_status', '=', '1']])->first();

    }

    public static function GenrateReferCode()
    {
        $code = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 5);
        $referCode = strtoupper($code);
        if (static::CheckReferCode($referCode)) {
            return static::GenrateReferCode();
        }
        return $referCode;
    }

    public static function CheckReferCode($referCode)
    {
        return static::where([['ReferralCode', '=', $referCode]])->exists();
    }

    public function Customers()
    {
        return $this->belongsToMany(Customer::class);
    }

    public function PendingOrderManagements()
    {
        return $this->HasMany(OrderManagement::class)->with(['Customer','Driver'])->whereIn('order_status', array(1));
    }

    public function AcceptedOrderManagements()
    {
        return $this->HasMany(OrderManagement::class)->with(['Customer','Driver'])->whereIn('order_status', array(2,3,4));;
    }

    public function CompletedOrdeManagements()
    {
        return $this->HasMany(OrderManagement::class)->with(['Customer','Driver'])->whereIn('order_status', array(5,6));;
    }
}
