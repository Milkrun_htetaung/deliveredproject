<?php

namespace App\Http\Controllers\Admin;
use Auth;
use App\Models\Driver;
use App\Models\Retailer;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class DriverController extends Controller
{
    public function index()
    {
        $drivers = Driver::latest()->paginate(25);
        $pendingdrivers=0;
        return view('admin.driver.index', compact('drivers', 'pendingdrivers'));
    }

    public function create()
    {
        $retailer = Retailer::get();
        $countries = Country::get();
        return view('admin.driver.create', compact('retailer','countries'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'driver_name' => 'required',
            'driver_email' => 'required|email',
            'driver_phone' => ['required', 'max:255', 'unique:drivers,driver_phone'],
            'driver_password' => 'required',
            'image' => 'required|file'
        ]);
        $request->file('image');
        $image = $request->image->store('driver', 'public');
        $driver = Driver::create([
            'driver_name' => $request->driver_name,
            'driver_email' => $request->driver_email,
            'driver_phone' => $request->country . $request->driver_phone,
            'driver_password' => Hash::make($request->driver_password),
            'driver_image' => $image
        ]);
        //return redirect()->route('admin.driver.document.show', [$driver->id]);
        return redirect()->back()->with('driveradded', 'Driver Added');
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

 public function AddMoney(Request $request)
    {
        $merchant_id = Auth::user()->id;
        $request->validate([
            'payment_method' => 'required|integer|between:1,2',
            'receipt_number' => 'required|string',
            'amount' => 'required|numeric',
            'description' => 'required|string',
            'add_money_driver_id' => 'required|exists:drivers,id'
        ]);
        DriverWalletTransaction::create([
            'merchant_id' => $merchant_id,
            'driver_id' => $request->add_money_driver_id,
            'transaction_type' => 1,
            'payment_method' => $request->payment_method,
            'receipt_number' => $request->receipt_number,
            'amount' => $request->amount,
            'platform' => 1,
            'description' => $request->description,
            'narration' => 1,
        ]);
        $driver = Driver::find($request->add_money_driver_id);
        $wallet_money = $driver->wallet_money;
        $driver->wallet_money = $wallet_money + $request->amount;
        $driver->save();
        return redirect()->back()->with('moneyAdded', 'Money Added In Driver Wallet');
    }

    public function ChangeStatus($id, $status)
    {
        $validator = Validator::make(
            [
                'id' => $id,
                'status' => $status,
            ],
            [
                'id' => ['required'],
                'status' => ['required', 'integer', 'between:1,2'],
            ]);
        if ($validator->fails()) {
            return redirect()->back();
        }
        $driver = Driver::findOrFail($id);
        $driver->driver_admin_status = $status;
        $driver->save();
        $access_token_id = $driver->access_token_id;
        if (!empty($access_token_id)) {
            $access_token_id = $driver->access_token_id;
            \DB::table('oauth_access_tokens')->where('id', '=', $access_token_id)->delete();
            \DB::table('oauth_refresh_tokens')->where('access_token_id', '=', $access_token_id)->delete();
        }
        $playerids = array($driver->player_id);
        $data = ['status' => $status];
        $message = $status == 2 ? trans('admin.message296') : trans('admin.message297');
        Onesignal::DriverPushMessage($playerids, $data, $message, 5, $merchant_id);
        return redirect()->back();
    }

    public function Logout($id)
    {
        $driver = Driver::findOrFail($id);
        $driver->login_logout = 2;
        $driver->save();
        $access_token_id = $driver->access_token_id;
        \DB::table('oauth_access_tokens')->where('id', '=', $access_token_id)->delete();
        \DB::table('oauth_refresh_tokens')->where('access_token_id', '=', $access_token_id)->delete();
        $playerids = array($driver->player_id);
        $data = [];
        $message = trans('admin.message298');
        Onesignal::DriverPushMessage($playerids, $data, $message, 6, $merchant_id);
    }

    public function PersonalDocExpire()
    {
        $merchant_id = Auth::user()->id;
        $currentDate = date("Y-m-d");
        $doc = DriverDocument::whereHas('Driver', function ($query) use ($merchant_id) {
            $query->where([['merchant_id', '=', $merchant_id]]);
        })->whereDate('expire_date', '<', $currentDate)->get();
        if (empty($doc->toArray())) {
            return redirect()->back();
        }
        $docment_ids = array_pluck($doc, 'id');
        $driver_ids = array_pluck($doc, 'driver_id');
        DriverDocument::whereIn('id', $docment_ids)->update(['document_verification_status' => 4]);
        $playerids = Driver::whereIn('id', $driver_ids)->where([['player_id', '!=', null]])->get(['player_id']);
        $playerids = array_pluck($playerids,'player_id');
        $message = trans('admin.message359');
        $data = ['title'=>$message];
        Onesignal::DriverPushMessage($playerids, $data, $message, 7, $merchant_id);
        return redirect()->back();
    }

    public function VehicleDocExpire()
    {

    }

    public function destroy($id)
    {
        //
    }
}
