<?php

namespace App\Http\Controllers\Admin;

use App\Models\CustomerSupport;
use App\Models\Onesignal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Retailer;
use App\Models\Customer;
use App\Models\Driver;


class AdminController extends Controller
{
    public function index()
    {   
        $retailer = Retailer::get()->count();
        $customers = Customer::get()->count();
        $drivers = Driver::get()->count();
        
        return view('admin.home',compact('retailer','customers','drivers'));
    }

    public function SetLangauge(Request $request, $locle)
    {
        $request->session()->put('locale', $locle);
        return redirect()->back();
    }


    public function OneSignal()
    {
        $onesignal = Onesignal::first();
        return view('admin.random.onesignal', compact('onesignal'));
    }

    public function UpdateOneSignal(Request $request)
    {
        $request->validate([
            'user_application_key' => 'required',
            'user_rest_key' => 'required',
            'driver_application_key' => 'required',
            'driver_rest_key' => 'required',
        ]);
        $onsignal = Onesignal::first();
        if (!empty($onsignal)) {
            $onsignal->user_application_key = $request->user_application_key;
            $onsignal->user_rest_key = $request->user_rest_key;
            $onsignal->driver_application_key = $request->driver_application_key;
            $onsignal->driver_rest_key = $request->driver_rest_key;
            $onsignal->save();
        } else {
            $onsignal = new Onesignal();
            $onsignal->user_application_key = $request->user_application_key;
            $onsignal->user_rest_key = $request->user_rest_key;
            $onsignal->driver_application_key = $request->driver_application_key;
            $onsignal->driver_rest_key = $request->driver_rest_key;
            $onsignal->save();
        }
        return redirect()->back()->with('onesignal', 'Updated');
    }

    public function Customer_Support()
    {
        $customer_supports = CustomerSupport::paginate(25);
        return view('admin.random.customer_support', compact('customer_supports'));
    }

    public function Customer_Support_Search(Request $request)
    {
        $query = CustomerSupport::where([['created_at', '!=', ""]]);
        if ($request->application) {
            $query->where('application', $request->application);
        }
        if ($request->date) {
            $query->whereDate('created_at', '=', $request->date);
        }
        if ($request->name) {
            $query->where('name', 'LIKE', "%$request->name%");
        }
        $customer_supports = $query->paginate(25);
        return view('admin.random.customer_support', compact('customer_supports'));
    }


}
