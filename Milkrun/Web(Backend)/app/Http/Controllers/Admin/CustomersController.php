<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use App\Models\Country;
use App\Models\Retailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::with('Retailer')->latest()->paginate(25);
        //echo "<pre>"; print_r($customers->toArray()); die();
        return view('admin.customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $retailers = Retailer::get();
        $countries = Country::get();
        return view('admin.customers.create', compact('retailers', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $retailer_id = $request->retailer_id;
        $password = Hash::make($request->customer_password);
        $request->file('profile');
        $profile_image = $request->profile->store('customer', 'public');
        $user = Customer::create([
            'customer_name' => $request->customer_name,
            'customer_email' => $request->customer_email,
            'customer_phone' => $request->country . $request->customer_phone,
            'customer_password' => $password,
            'customer_image' => $profile_image,
            'customer_address' => $request->customer_address,
            'customer_mrt_station' => 0
        ]);
        $user->Retailer()->attach($retailer_id);
        return redirect()->back()->with('customeradded', 'Customer Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customers $customers
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customers $customers
     * @return \Illuminate\Http\Response
     */
      public function edit($id)
    {
        $retailers = Retailer::get();
        $countries = Country::get();
        $customer = Customer::findOrFail($id);
        return view('admin.customers.edit', compact('retailers', 'countries','customer'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Customers $customers
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'retailer_id' => "required",
            'customer_name' => "required",
            'customer_email' => ['required', 'max:255', 'unique:customers,customer_email'],
            'customer_phone' => ['required', 'max:255', 'unique:customers,customer_phone'],
            'customer_address' => 'required',
            'profile' => 'required|file'
        ]);
        $customer = Customer::findOrFail($id);
        $customer->customer_name = $request->customer_name;
        $customer->customer_email = $request->customer_email;
        $customer->customer_phone = $request->customer_phone;
        $customer->customer_address = $request->customer_address;
        if ($request->customer_password == 1) {
            $password = Hash::make($request->customer_password);
            $customer->customer_password = $password;
        }
        if ($request->hasFile('profile')) {
            $request->file('profile');
            $profile_image = $request->profile->store('user', 'public');
            $customer->customer_image = $profile_image;
        }
        $customer->save();
        return redirect()->back()->with('rideradded', 'Rider Added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customers $customers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customers)
    {
        //
    }

    public function ChangeStatus($id, $status)
    {
        $validator = Validator::make(
            [
                'id' => $id,
                'status' => $status,
            ],
            [
                'id' => ['required'],
                'status' => ['required', 'integer', 'between:1,2'],
            ]);
        if ($validator->fails()) {
            return redirect()->back();
        }
        $customer = Customer::findOrFail($id);
        $customer->customer_status = $status;
        $customer->save();
        return redirect()->back();
    }

    public function Serach(Request $request)
    {
        $request->validate([
            'keyword' => "required",
            'parameter' => "required|integer|between:1,3",
        ]);
        switch ($request->parameter) {
            case "1":
                $parameter = "customer_name";
                break;
            case "2":
                $parameter = "customer_email";
                break;
            case "3":
                $parameter = "customer_phone";
                break;
        }

        $query = Customer::where([['created_at', '!=', ""]]);
        if ($request->keyword) {
            $query->where($parameter, 'like', '%' . $request->keyword . '%');
        }
        $customers = $query->paginate(25);
        return view('admin.customers.index', compact('customers'));
    }

}
