<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('message');
            $table->string('image')->nullable();
            $table->string('url')->nullable();
            $table->integer('user_id')->default(0);
            $table->integer('driver_id')->nullable(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_notifications');
    }
}
