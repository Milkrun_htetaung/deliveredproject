<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.message85'); ?></h3>

                </div>
            </div>
            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <?php if(session('onesignal')): ?>
                                            <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2"
                                                 role="alert">
                                                <span class="alert-icon"><i class="fa fa-info"></i></span>
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <strong><?php echo app('translator')->getFromJson('admin.message86'); ?></strong>
                                            </div>
                                        <?php endif; ?>
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data"
                                              action="<?php echo e(route('onesignal.store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <fieldset>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                <?php echo app('translator')->getFromJson('admin.message87'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   id="user_application_key" name="user_application_key"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.message87'); ?>"
                                                                   value="<?php if(!empty($onesignal)): ?><?php echo e($onesignal->user_application_key); ?><?php endif; ?>"
                                                                   required>
                                                            <?php if($errors->has('user_application_key')): ?>
                                                                <label class="danger"><?php echo e($errors->first('user_application_key')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                <?php echo app('translator')->getFromJson('admin.message88'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="user_rest_key"
                                                                   name="user_rest_key"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.message88'); ?>"
                                                                   value="<?php if(!empty($onesignal)): ?><?php echo e($onesignal->user_rest_key); ?><?php endif; ?>" required>
                                                            <?php if($errors->has('user_rest_key')): ?>
                                                                <label class="danger"><?php echo e($errors->first('user_rest_key')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                <?php echo app('translator')->getFromJson('admin.message89'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   id="driver_application_key"
                                                                   name="driver_application_key"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.message89'); ?>"
                                                                   value="<?php if(!empty($onesignal)): ?><?php echo e($onesignal->driver_application_key); ?><?php endif; ?>"
                                                                   required>
                                                            <?php if($errors->has('driver_application_key')): ?>
                                                                <label class="danger"><?php echo e($errors->first('driver_application_key')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                <?php echo app('translator')->getFromJson('admin.message90'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="driver_rest_key"
                                                                   name="driver_rest_key"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.message90'); ?>"
                                                                   value="<?php if(!empty($onesignal)): ?><?php echo e($onesignal->driver_rest_key); ?><?php endif; ?>" required>
                                                            <?php if($errors->has('driver_rest_key')): ?>
                                                                <label class="danger"><?php echo e($errors->first('driver_rest_key')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                </div>

                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>