<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CountryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required',
                Rule::unique('language_countries')->where(function ($query) {
                    return $query->where([['locale', '=', \Config::get('app.locale')]]);
                })],
            'isocode' => 'required',
            'phonecode' => 'required|integer',
            'maxnumphone' => 'required|integer|gte:minnumphone',
            'distance_unit' => 'required|integer|between:1,2',
            'default_language' => 'required',
            'currency' => 'required',
            'minnumphone' => 'required|integer|lte:maxnumphone'
        ];
    }
}
