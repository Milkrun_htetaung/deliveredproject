<div class="main-menu menu-fixed menu-dark menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

                <li class="nav-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="icon-globe"></i><span
                                class="menu-title"><?php echo app('translator')->getFromJson('admin.message43'); ?></span></a></li>

                <li class=" nav-item"><a href="#"><i class="fa fa-user-secret"></i><span class="menu-title"
                                                                                         data-i18n="nav.adminMangement.main"><?php echo app('translator')->getFromJson('admin.sub-admin'); ?></span></a>
                    <ul class="menu-content">

                            <li><a class="menu-item" href="#"
                                   data-i18n="nav.active_ride.1_column"><?php echo app('translator')->getFromJson('admin.message401'); ?></a>
                            </li>



                            <li><a class="menu-item" href="#"
                                   data-i18n="nav.active_ride.1_column"><?php echo app('translator')->getFromJson('admin.role'); ?></a>
                            </li>

                    </ul>
                </li>

        </ul>
    </div>
</div>
