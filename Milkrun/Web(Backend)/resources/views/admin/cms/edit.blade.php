@extends('admin.layouts.main')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('merchanttheme/css/summernote.css') }}">
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-10 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.message371')
                        (@lang('admin.message460') {{ strtoupper(Config::get('app.locale')) }})</h3>
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('cms.index') }}">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            @if(session('cmsupdate'))
                <div class="box no-border">
                    <div class="box-tools">
                        <p class="alert alert-info alert-dismissible">
                            <strong>@lang('admin.message402')</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                        </p>
                    </div>
                </div>
            @endif


            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data"
                                              action="{{ route('cms.update',$cmspage->id) }}">
                                            {{method_field('PUT')}}
                                            @csrf
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="lastName3">
                                                                @lang('admin.message212') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="title"
                                                                   name="title" value="{{ $cmspage->LanguageSingle->title }}"
                                                                   placeholder="@lang('admin.message212')"
                                                                   required>
                                                            @if ($errors->has('title'))
                                                                <label class="danger">{{ $errors->first('title') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                @lang('admin.message214') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <textarea class="summernote" id="description"
                                                                      name="description" rows="3"
                                                                      placeholder="@lang('admin.description')">{{ $cmspage->LanguageSingle->description }}</textarea>
                                                            @if ($errors->has('description'))
                                                                <label class="danger">{{ $errors->first('description') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection