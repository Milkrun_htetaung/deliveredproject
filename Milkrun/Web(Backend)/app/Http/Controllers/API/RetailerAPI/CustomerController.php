<?php
/**
 * Created by PhpStorm.
 * User: Yogesh Apporio
 * Date: 20-12-2018
 * Time: 16:14
 * app_type ==> 1 For Retailer
 * app_type ==> 2 For Deliverer
 * app_type ==> 3 For User
 */

namespace App\Http\Controllers\API\RetailerAPI;

use App\Models\Retailer;
use App\Models\Customer;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Client;

class CustomerController extends Controller
{

    public function AddRetailerCustomer(Request $request)
    {
//        $country_code = Country::find($request->country_id)->phonecode;
//        $customer_new_phone = $country_code . $request->customer_phone;
//        //echo $request->country_id.$request->customer_phone; die();
////        $request->add(['customer_phone']) = $request->country_id.$request->customer_phone;
//        $request->request->add(['customer_phone'=>$customer_new_phone]);
//        echo"<pre>";
//        print_r($request->all());
//        die();
       $validator = Validator::make($request->all(),
            [
                'customer_name' => 'required',
                'customer_email' => ['required'],
                'country_id' => 'required',
                'customer_phone' => 'required',
                'customer_password' => 'required',
                'customer_address' => 'required',
                'customer_mrt_station' => 'required',
//                'profile_image' => 'required'
            ]);

       if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors['0'], 'data' => []]);
       }
        $country_code = Country::find($request->country_id)->phonecode;
        $customer_new_phone = $country_code . $request->customer_phone;
        $retailer = $request->user('api_retailer');
        $retailer_id = $retailer->id;
        $customer_email=$request->customer_email;
        $customer_data = Customer::where(['customer_email' => $customer_email,'customer_phone' => $customer_new_phone])->first();
        $customer_id = $customer_data->id;
//        $pivot_data = Customer::where(['customer_id' => $customer_id,'retailer_id' => $retailer_id])->first();
        $password = Hash::
        ($request->customer_password);
//        list($format, $image) = explode(',', $request->profile_image);
//        $temp = explode('/', $format);
//        list($ext,) = explode(';', $temp[1]);
//        $file_name = time() . "_customer." . $ext;
//        file_put_contents(public_path() . '/images/customer/' . $file_name, base64_decode($image));
//        $profile_image = env('APP_URL')."/images/customer/".$file_name;
    
        
        
        $user = Customer::create([
            'customer_name' => $request->customer_name,
            'customer_email' => $customer_email,
            'customer_phone' => $customer_new_phone,
            'password' => $password,
//            'customer_image' => $profile_image,
            'customer_address' => $request->customer_address,
            'customer_mrt_station' => 0
        ]);
        $user->Retailer()->attach($retailer_id);

        $customers = $retailer->Customers()->get();
        if (!empty($customers)):
            return response()->json(['result' => "1", 'message' => "Customers Data", 'data' => $customers]);
        else:
            return response()->json(['result' => "0", 'message' => "No Customer Found", 'data' => []]);
        endif;
    }

    public function RetailerCustomers(Request $request)
    {
        $retailer = $request->user('api_retailer');
        $customers = Customer::all()->toArray();
        if (!empty($customers)):
            return response()->json(['result' => "1", 'message' => "Customers Data", 'data' => $customers]);
        else:
            return response()->json(['result' => "0", 'message' => "No Customer Found", 'data' => []]);
        endif;
    }


}
