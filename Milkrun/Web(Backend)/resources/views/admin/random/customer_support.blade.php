@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.message379')</h3>
                </div>
            </div>
            <div class="content-body">

                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="">
                                    <form method="post" action="{{ route('customer_support.search') }}">
                                        @csrf
                                        <div class="table_search">

                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <select class="form-control" name="application"
                                                            id="application">
                                                        <option value="">--@lang('admin.message72')--</option>
                                                        <option value="2">@lang('admin.message69')</option>
                                                        <option value="1">@lang('admin.message70')</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <input type="text" id="" name="name"
                                                           placeholder="@lang('admin.name')"
                                                           class="form-control col-md-12 col-xs-12">
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <input type="text" id="" name="date"
                                                           placeholder="@lang('admin.message381')"
                                                           class="form-control col-md-12 col-xs-12 datepickersearch"
                                                           id="datepickersearch">
                                                </div>
                                            </div>

                                            <div class="col-sm-2  col-xs-12 form-group ">
                                                <button class="btn btn-primary" type="submit" name="seabt12"><i
                                                            class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>@lang('admin.message67')</th>
                                                <th>@lang('admin.name')</th>
                                                <th>@lang('admin.email')</th>
                                                <th>@lang('admin.merchantPhone')</th>
                                                <th>@lang('admin.message380')</th>
                                                <th>@lang('admin.created_at')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php $sr = $customer_supports->firstItem() @endphp
                                            @foreach($customer_supports as $customer_support)
                                                <tr>
                                                    <td>
                                                        {{ $sr }}
                                                    </td>
                                                    <td>
                                                        @if($customer_support->application == 1)
                                                            @lang('admin.message70')
                                                        @else
                                                            @lang('admin.message69')
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ $customer_support->name }}
                                                    </td>
                                                    <td>
                                                        {{ $customer_support->email }}
                                                    </td>
                                                    <td>
                                                        {{ $customer_support->phone }}
                                                    </td>
                                                    <td>
                                                        <span class="map_address">{{ $customer_support->query }}</span>
                                                    </td>

                                                    <td>
                                                        {{ $customer_support->created_at->toformatteddatestring() }}
                                                    </td>
                                                </tr>
                                                @php $sr++  @endphp
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1">{{ $customer_supports->links() }}</div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>
@endsection