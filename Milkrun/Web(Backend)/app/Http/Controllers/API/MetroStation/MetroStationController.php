<?php
/**
 * Created by PhpStorm.
 * User: Yogesh Apporio
 * Date: 20-12-2018
 * Time: 16:02
 */

namespace App\Http\Controllers\API\MetroStation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Station;

class MetroStationController extends Controller
{
    public function MetroStation_List(Request $request)
    {
        $stations = Station::all()->toArray();
        if (!empty($stations)):
            return response()->json(['result' => "1", 'message' => 'Country Data', 'data' => $stations]);
        else:
            return response()->json(['result' => "0", 'message' => 'No Data Found', 'data' => []]);
        endif;

    }
}
