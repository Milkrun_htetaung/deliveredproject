@extends('merchant.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">{{ $driver->fullName }} @lang('admin.message22')</h3>
                </div>
                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('driver.index') }}">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="content-detached">
                <div class="content-body">
                    <section class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-head">
                                    <div class="card-header">
                                        <h4 class="card-title">@lang('admin.message23')</h4>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <!-- Task List table -->
                                        <div class="table-responsive">
                                            <table id="users-contacts"
                                                   class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                                                <thead>
                                                <tr>
                                                    <th>@lang('admin.VehicleType')</th>
                                                    <th>@lang('admin.servicetype')</th>
                                                    <th>@lang('admin.message24')</th>
                                                    <th>@lang('admin.message25')</th>
                                                    <th>@lang('admin.message26')</th>
                                                    <th>@lang('admin.message27')</th>
                                                    <th>@lang('admin.message28')</th>
                                                    <th>@lang('admin.action')</th>
                                                    <th>@lang('admin.create')</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($driver->DriverVehicles as $vehicle)
                                                    <tr>
                                                        <td class="text-center">
                                                            {{ $vehicle->VehicleType->vehicleTypeName }}
                                                        </td>
                                                        <?php $a = array() ?>
                                                        @foreach($vehicle->ServiceTypes as $serviceType)
                                                            <?php $a[] = $serviceType->serviceName; ?>
                                                        @endforeach
                                                        <td class="text-center">
                                                            {{ implode(',',$a) }}
                                                        </td>
                                                        <td class="text-center">
                                                            {{ $vehicle->vehicle_number }}
                                                        </td>
                                                        <td class="text-center">
                                                            {{ $vehicle->vehicle_color }}
                                                        </td>
                                                        <td class="text-center">
                                                            <img src="{{ asset($vehicle->vehicle_image) }}"
                                                                 alt="avatar" style="width: 100px;height: 100px;">
                                                        </td>
                                                        <td class="text-center">
                                                            <img src="{{ asset($vehicle->vehicle_number_plate_image) }}"
                                                                 alt="avatar" style="width: 100px;height: 100px;">
                                                        </td>
                                                        <td class="text-center">
                                                            <a href="{{ route('merchant.driver-vehicledetails',$vehicle->id) }}"
                                                               type="button" class="btn btn-icon btn-primary mr-1"><i
                                                                        class="fa fa-file-text-o"></i></a>
                                                        </td>
                                                        <td class="text-center">
                                                            @switch($vehicle->vehicle_verification_status)
                                                                @case(1)
                                                                @lang('admin.message19')
                                                                @break
                                                                @case(2)
                                                                <a type="button" class="btn btn-icon btn-success mr-1"
                                                                   href="{{ route('merchant.driver-vehicle-verify',[$vehicle->id,1]) }}"><i
                                                                            class="fa fa-check"></i></a>
                                                                <a type="button" class="btn btn-icon btn-danger mr-1"
                                                                   href="{{ route('merchant.driver-vehicle-verify',[$vehicle->id,3]) }}"><i
                                                                            class="fa fa-eye"></i></a>
                                                                @break
                                                                @case(3)
                                                                @lang('admin.message20')
                                                                @break
                                                            @endswitch
                                                        </td>
                                                        <td class="text-center">
                                                            {{ $vehicle->created_at }}
                                                        </td>


                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </div>
@endsection