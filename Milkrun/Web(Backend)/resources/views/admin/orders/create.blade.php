@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.melvin_retailer_create_message1')</h3>
                </div>
                <div class="col-md-6 col-12">
                    @if(session('rideradded'))
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>@lang('admin.melvin_retailer_create_message2')</strong>
                        </div>
                    @endif
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('retailer.index') }}">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            {{--@if(!empty($errors))--}}
                {{--{!! $errors !!}--}}
            {{--@endif--}}

            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data" action="{{ route('retailer.store') }}">
                                            @csrf
                                            <fieldset>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                @lang('admin.melvin_retailer_create_message3') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   id="retailer_shop_name"
                                                                   name="retailer_shop_name"
                                                                   placeholder="@lang('admin.melvin_retailer_create_message3')" required>
                                                            @if ($errors->has('retailer_shop_name'))
                                                                <label
                                                                    class="danger">{{ $errors->first('retailer_shop_name') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                @lang('admin.melvin_retailer_create_message4') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="user_name"
                                                                   name="user_name"
                                                                   placeholder="@lang('admin.melvin_retailer_create_message4')" required>
                                                            @if ($errors->has('user_name'))
                                                                <label
                                                                    class="danger">{{ $errors->first('user_name') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                @lang('admin.melvin_retailer_create_message5') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   id="retailer_shop_address"
                                                                   name="retailer_shop_address"
                                                                   placeholder="@lang('admin.melvin_retailer_create_message5')" required>
                                                            @if ($errors->has('retailer_shop_address'))
                                                                <label
                                                                    class="danger">{{ $errors->first('retailer_shop_address') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                @lang('admin.melvin_retailer_create_message6') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="email" class="form-control" id="user_email"
                                                                   name="user_email"
                                                                   placeholder="@lang('admin.melvin_retailer_create_message6')" required>
                                                            @if ($errors->has('user_email'))
                                                                <label
                                                                    class="danger">{{ $errors->first('user_email') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="location3">@lang('admin.melvin_retailer_create_message7') :</label>
                                                            <select class="form-control" name="country" id="country"
                                                                    required>
                                                                <option value="">@lang('admin.melvin_retailer_create_message8')</option>
                                                                @foreach($countries  as $country)
                                                                    <option data-min=""
                                                                            data-max=""
                                                                            value="{{ $country->phonecode }}">{{$country->name  }}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('country'))
                                                                <label
                                                                    class="danger">{{ $errors->first('country') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="lastName3">
                                                                @lang('admin.melvin_retailer_create_message9') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="user_phone"
                                                                   name="user_phone"
                                                                   placeholder="@lang('admin.melvin_retailer_create_message9')" required>
                                                            @if ($errors->has('user_phone'))
                                                                <label
                                                                    class="danger">{{ $errors->first('user_phone') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ProfileImage">
                                                                @lang('admin.melvin_retailer_create_message10') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="file" class="form-control" id="profile"
                                                                   name="profile"
                                                                   placeholder="@lang('admin.melvin_retailer_create_message10')" required>
                                                            @if ($errors->has('profile'))
                                                                <label
                                                                    class="danger">{{ $errors->first('profile') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                @lang('admin.melvin_retailer_create_message11') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="password" class="form-control" id="password"
                                                                   name="password"
                                                                   placeholder="@lang('admin.melvin_retailer_create_message11')" required>
                                                            @if ($errors->has('password'))
                                                                <label
                                                                    class="danger">{{ $errors->first('password') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>


                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> @lang('admin.melvin_retailer_create_message13')
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script>
        function RideType(val) {
            if (val == "1") {
                document.getElementById('corporate_div').style.display = 'block';
            } else {
                document.getElementById('corporate_div').style.display = 'none';
            }
        }
    </script>
@endsection
