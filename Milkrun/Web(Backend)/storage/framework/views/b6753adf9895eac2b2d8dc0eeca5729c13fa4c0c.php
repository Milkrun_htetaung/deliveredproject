
<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.message208'); ?></h3>
                </div>

                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            <a title="<?php echo app('translator')->getFromJson('admin.message209'); ?>" href="<?php echo e(route('cms.create')); ?>">
                                <button class="btn btn-icon btn-success mr-1"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">

                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="">
                                    <form method="post" action="<?php echo e(route('admin.cms.search')); ?>">
                                        <?php echo csrf_field(); ?>
                                        <div class="table_search">
                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <input type="text" id="" name="pagetitle"
                                                           placeholder="<?php echo app('translator')->getFromJson('admin.message373'); ?>"
                                                           class="form-control col-md-12 col-xs-12">
                                                </div>
                                            </div>


                                            <div class="col-sm-2  col-xs-12 form-group ">
                                                <button class="btn btn-primary" type="submit" name="seabt12"><i
                                                            class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th><?php echo app('translator')->getFromJson('admin.message213'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message212'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message214'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.action'); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $__currentLoopData = $cmspages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cmspage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td>
                                                        <?php if($cmspage->application == 1): ?>
                                                            User
                                                        <?php else: ?>
                                                            Driver
                                                        <?php endif; ?>

                                                    </td>
                                                    <td><?php if(empty($cmspage->LanguageSingle)): ?>
                                                            <span style="color:red"><?php echo e(trans('admin.name-not-given')); ?></span>
                                                            <span class="text-primary">( In <?php echo e($cmspage->LanguageAny->LanguageName->name); ?>

                                                                : <?php echo e($cmspage->LanguageAny->title); ?>

                                                                )</span>
                                                        <?php else: ?>
                                                            <?php echo e($cmspage->LanguageSingle->title); ?>

                                                        <?php endif; ?>
                                                    </td>
                                                    <td><?php if(empty($cmspage->LanguageSingle)): ?>
                                                            <span style="color:red"><?php echo e(trans('admin.name-not-given')); ?></span>
                                                            <span class="text-primary">( In <?php echo e($cmspage->LanguageAny->LanguageName->name); ?>

                                                                : <?php echo e(substr($cmspage->LanguageAny->description, 0, 70)); ?>

                                                                )</span>
                                                        <?php else: ?>
                                                            <?php echo e(substr($cmspage->LanguageSingle->description, 0, 70)); ?>

                                                        <?php endif; ?>
                                                    </td>

                                                    <td>
                                                        <a href="<?php echo e(route('cms.edit',$cmspage->id)); ?>"
                                                           data-original-title="Edit" data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_edit action_btn"> <i
                                                                    class="fa fa-pencil"></i> </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>