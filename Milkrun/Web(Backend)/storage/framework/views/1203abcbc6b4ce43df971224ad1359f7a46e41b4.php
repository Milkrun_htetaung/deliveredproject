<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><?php echo app('translator')->getFromJson('admin.message342'); ?></h4>
                            <a class="heading-elements-toggle"><i class="ft-more-horizontal font-medium-3"></i></a>
                        </div>
                        <div class="card-content">
                                <div class="col-md-6">
                                    <div class="widget-panel widget-style-2 bg-info">
                                        <i class="fa fa-group"></i>
                                        <h2 class="m-0 counter">0</h2>
                                        <div><?php echo app('translator')->getFromJson('admin.message344'); ?></div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="widget-panel widget-style-2 bg-warning">
                                        <i class="fa fa-male"></i>
                                        <h2 class="m-0 counter">0</h2>
                                        <div><?php echo app('translator')->getFromJson('admin.message345'); ?></div>
                                    </div>
                                </div>



                                <div class="col-md-6">
                                    <div class="widget-panel widget-style-2 bg-success">
                                        <i class="fa fa-building-o"></i>
                                        <h2 class="m-0 counter">0</h2>
                                        <div><?php echo app('translator')->getFromJson('admin.message346'); ?></div>
                                    </div>
                                </div>



                                <div class="col-md-6">
                                    <div class="widget-panel widget-style-2 bg-primary">
                                        <i class="fa fa-money"></i>
                                        <h2 class="m-0 counter">0</h2>
                                        <div><?php echo app('translator')->getFromJson('admin.message347'); ?></div>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><?php echo app('translator')->getFromJson('admin.message343'); ?></h4>
                            <a class="heading-elements-toggle"><i class="ft-more-horizontal font-medium-3"></i></a>
                        </div>


                        <div class="card-content">

                             <div class="col-md-6">
                                    <div class="widget-panel widget-style-2 bg-info">
                                        <i class="fa fa-cubes"></i>
                                        <h2 class="m-0 counter">0</h2>
                                        <div><?php echo app('translator')->getFromJson('admin.message348'); ?></div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="widget-panel widget-style-2 bg-warning">
                                        <i class="fa fa-plane"></i>
                                        <h2 class="m-0 counter">0</h2>
                                        <div><?php echo app('translator')->getFromJson('admin.message349'); ?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="widget-panel widget-style-2 bg-success">
                                        <i class="fa fa-times-circle-o"></i>
                                        <h2 class="m-0 counter">0</h2>
                                        <div><?php echo app('translator')->getFromJson('admin.message350'); ?></div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="widget-panel widget-style-2 bg-primary">
                                        <i class="fa fa-check"></i>
                                        <h2 class="m-0 counter">0</h2>
                                        <div><?php echo app('translator')->getFromJson('admin.message351'); ?></div>
                                    </div>
                                </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>