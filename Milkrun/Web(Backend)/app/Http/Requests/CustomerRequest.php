<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'retailer_id' => "required",
            'customer_name' => "required",
            'customer_email' => ['required', 'max:255', 'unique:customers,customer_email'],
            'customer_phone' => ['required', 'max:255', 'unique:customers,customer_phone'],
            'customer_address' => 'required',
            'customer_password' => "required|min:6",
            'profile' => 'required|file'
        ];
    }

    public function messages()
    {
        return [
            'customer_name.required' => trans('admin.melvin_customer_validate_message1'),
            'customer_email.required' => trans('admin.melvin_customer_validate_message2'),
            'customer_email.unique' => trans('admin.melvin_customer_validate_message3'),
            'customer_phone.required' => trans('admin.melvin_customer_validate_message4'),
            'customer_phone.unique' => trans('admin.melvin_customer_validate_message5'),
            'customer_address.required' => trans('admin.melvin_customer_validate_message6'),
            'customer_password.required' => trans('admin.melvin_customer_validate_message7'),
            'profile.required' => trans('admin.melvin_customer_validate_message8'),
            'retailer_id.required' => trans('admin.melvin_customer_validate_message9'),
        ];
    }
}
