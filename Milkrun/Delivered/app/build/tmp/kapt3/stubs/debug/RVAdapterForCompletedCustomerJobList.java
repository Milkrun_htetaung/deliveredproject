
import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001eB\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0015\u001a\u00020\u0016H\u0016J\u0018\u0010\u0017\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u0016H\u0016J\u0018\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0016H\u0016R \u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\"\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\"\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u000f\"\u0004\b\u0014\u0010\u0011\u00a8\u0006\u001f"}, d2 = {"LRVAdapterForCompletedCustomerJobList;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "LRVAdapterForCompletedCustomerJobList$JobViewHolder;", "()V", "list", "Ljava/util/ArrayList;", "Lcom/milkrun/delivered/entities/Order;", "getList", "()Ljava/util/ArrayList;", "setList", "(Ljava/util/ArrayList;)V", "onItemSelect", "Lkotlin/Function0;", "", "getOnItemSelect", "()Lkotlin/jvm/functions/Function0;", "setOnItemSelect", "(Lkotlin/jvm/functions/Function0;)V", "onItemTrack", "getOnItemTrack", "setOnItemTrack", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "JobViewHolder", "app_debug"})
public final class RVAdapterForCompletedCustomerJobList extends androidx.recyclerview.widget.RecyclerView.Adapter<RVAdapterForCompletedCustomerJobList.JobViewHolder> {
    @org.jetbrains.annotations.Nullable()
    private kotlin.jvm.functions.Function0<kotlin.Unit> onItemSelect;
    @org.jetbrains.annotations.Nullable()
    private kotlin.jvm.functions.Function0<kotlin.Unit> onItemTrack;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.milkrun.delivered.entities.Order> list;
    
    @org.jetbrains.annotations.Nullable()
    public final kotlin.jvm.functions.Function0<kotlin.Unit> getOnItemSelect() {
        return null;
    }
    
    public final void setOnItemSelect(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function0<kotlin.Unit> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final kotlin.jvm.functions.Function0<kotlin.Unit> getOnItemTrack() {
        return null;
    }
    
    public final void setOnItemTrack(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function0<kotlin.Unit> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.milkrun.delivered.entities.Order> getList() {
        return null;
    }
    
    public final void setList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.milkrun.delivered.entities.Order> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public RVAdapterForCompletedCustomerJobList.JobViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    RVAdapterForCompletedCustomerJobList.JobViewHolder holder, int position) {
    }
    
    public RVAdapterForCompletedCustomerJobList() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"LRVAdapterForCompletedCustomerJobList$JobViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "viewBinding", "Lcom/milkrun/delivered/databinding/JobRetailItemViewBinding;", "(Lcom/milkrun/delivered/databinding/JobRetailItemViewBinding;)V", "getViewBinding", "()Lcom/milkrun/delivered/databinding/JobRetailItemViewBinding;", "app_debug"})
    public static final class JobViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final com.milkrun.delivered.databinding.JobRetailItemViewBinding viewBinding = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.milkrun.delivered.databinding.JobRetailItemViewBinding getViewBinding() {
            return null;
        }
        
        public JobViewHolder(@org.jetbrains.annotations.NotNull()
        com.milkrun.delivered.databinding.JobRetailItemViewBinding viewBinding) {
            super(null);
        }
    }
}