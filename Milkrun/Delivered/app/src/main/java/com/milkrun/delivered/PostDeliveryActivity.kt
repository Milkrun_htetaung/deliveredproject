package com.milkrun.delivered

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.milkrun.delivered.ui.retailer.postdelivery.PostDeliveryFragment
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class PostDeliveryActivity : AppCompatActivity() , HasSupportFragmentInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>


    override fun supportFragmentInjector()=dispatchingAndroidInjector

    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, PostDeliveryActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)

        setContentView(R.layout.post_delivery_activity)

            Handler().postDelayed({
                supportFragmentManager.beginTransaction()
                    .replace(R.id.container, PostDeliveryFragment.newInstance())
//                    .setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out)
                    .commitNow()

            }, 40)



    }
}
