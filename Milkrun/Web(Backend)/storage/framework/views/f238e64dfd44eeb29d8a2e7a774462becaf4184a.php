<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.message379'); ?></h3>
                </div>
            </div>
            <div class="content-body">

                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="">
                                    <form method="post" action="<?php echo e(route('customer_support.search')); ?>">
                                        <?php echo csrf_field(); ?>
                                        <div class="table_search">

                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <select class="form-control" name="application"
                                                            id="application">
                                                        <option value="">--<?php echo app('translator')->getFromJson('admin.message72'); ?>--</option>
                                                        <option value="2"><?php echo app('translator')->getFromJson('admin.message69'); ?></option>
                                                        <option value="1"><?php echo app('translator')->getFromJson('admin.message70'); ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <input type="text" id="" name="name"
                                                           placeholder="<?php echo app('translator')->getFromJson('admin.name'); ?>"
                                                           class="form-control col-md-12 col-xs-12">
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-xs-12 form-group ">
                                                <div class="input-group">
                                                    <input type="text" id="" name="date"
                                                           placeholder="<?php echo app('translator')->getFromJson('admin.message381'); ?>"
                                                           class="form-control col-md-12 col-xs-12 datepickersearch"
                                                           id="datepickersearch">
                                                </div>
                                            </div>

                                            <div class="col-sm-2  col-xs-12 form-group ">
                                                <button class="btn btn-primary" type="submit" name="seabt12"><i
                                                            class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo app('translator')->getFromJson('admin.message67'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.name'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.email'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.merchantPhone'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message380'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.created_at'); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $sr = $customer_supports->firstItem() ?>
                                            <?php $__currentLoopData = $customer_supports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer_support): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td>
                                                        <?php echo e($sr); ?>

                                                    </td>
                                                    <td>
                                                        <?php if($customer_support->application == 1): ?>
                                                            <?php echo app('translator')->getFromJson('admin.message70'); ?>
                                                        <?php else: ?>
                                                            <?php echo app('translator')->getFromJson('admin.message69'); ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo e($customer_support->name); ?>

                                                    </td>
                                                    <td>
                                                        <?php echo e($customer_support->email); ?>

                                                    </td>
                                                    <td>
                                                        <?php echo e($customer_support->phone); ?>

                                                    </td>
                                                    <td>
                                                        <span class="map_address"><?php echo e($customer_support->query); ?></span>
                                                    </td>

                                                    <td>
                                                        <?php echo e($customer_support->created_at->toformatteddatestring()); ?>

                                                    </td>
                                                </tr>
                                                <?php $sr++  ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1"><?php echo e($customer_supports->links()); ?></div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>