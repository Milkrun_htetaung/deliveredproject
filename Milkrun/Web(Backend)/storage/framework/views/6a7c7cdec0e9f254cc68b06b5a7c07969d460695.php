
<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.RiderManagement'); ?></h3>
                </div>

                <div class="col-md-6 col-12">
                    <?php if(session('moneyAdded')): ?>
                        <div class="col-md-8 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo app('translator')->getFromJson('admin.message430'); ?></strong>
                        </div>
                    <?php endif; ?>
                </div>


                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            <a href="<?php echo e(route('users.create')); ?>">
                                <button type="button" class="btn btn-icon btn-success mr-1"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <section id="horizontal">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="">
                                    <form action="<?php echo e(route('user.search')); ?>" method="post">
                                        <?php echo csrf_field(); ?>
                                        <div class="table_search">
                                            <div class="col-md-2 col-xs-4 form-group ">
                                                <div class="input-group">
                                                    <select class="form-control" name="parameter"
                                                            id="parameter"
                                                            required>
                                                        <option value="1"><?php echo app('translator')->getFromJson('admin.RiderName'); ?></option>
                                                        <option value="2"><?php echo app('translator')->getFromJson('admin.email'); ?></option>
                                                        <option value="3"><?php echo app('translator')->getFromJson('admin.RiderPhone'); ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-xs-6 form-group ">
                                                <div class="input-group">
                                                    <input type="text" name="keyword"
                                                           placeholder="<?php echo app('translator')->getFromJson('admin.message337'); ?>"
                                                           class="form-control col-md-12 col-xs-12" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-2  col-xs-12 form-group ">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"
                                                                                                 aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th><?php echo app('translator')->getFromJson('admin.RiderName'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.RiderPhone'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.RiderEmail'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message276'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message277'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message292'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.ReferralCode'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.UserSignupType'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.message272'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.Registerdate'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.lastUpdate'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.Status'); ?></th>
                                                <th><?php echo app('translator')->getFromJson('admin.action'); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td>
                                                        <div class="media">
                                                            <div class="media-left pr-1"><span
                                                                        class="user_img"><img
                                                                            src="<?php if($user->UserProfileImage): ?> <?php echo e(asset($user->UserProfileImage)); ?> <?php else: ?> <?php echo e(asset("user.png")); ?> <?php endif; ?>"
                                                                            alt="avatar"><i></i></span>
                                                            </div>
                                                            <div class="user_name">
                                                                <?php echo e($user->UserName); ?>

                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><?php echo e($user->UserPhone); ?></td>
                                                    <td><?php echo e($user->email); ?></td>
                                                    <td>
                                                        <?php if($user->rating == "0.0"): ?>
                                                            <?php echo app('translator')->getFromJson('admin.message278'); ?>
                                                        <?php else: ?>
                                                            <?php while($user->rating>0): ?>
                                                                <?php if($user->rating >0.5): ?>
                                                                    <img src="<?php echo e(asset("star.png")); ?>"
                                                                         alt='Whole Star'>
                                                                <?php else: ?>
                                                                    <img src="<?php echo e(asset('halfstar.png')); ?>"
                                                                         alt='Half Star'>
                                                                <?php endif; ?>
                                                                <?php $user->rating--; ?>
                                                            <?php endwhile; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php if($user->total_trips): ?>
                                                            <?php echo e($user->total_trips); ?>

                                                        <?php else: ?>
                                                            <?php echo app('translator')->getFromJson('admin.message279'); ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td><a href="<?php echo e(route('merchant.user.wallet',$user->id)); ?>" j>
                                                            <?php if($user->wallet_balance): ?>
                                                                <?php echo e($user->wallet_balance); ?>

                                                            <?php else: ?>
                                                                0.00
                                                            <?php endif; ?>
                                                        </a>
                                                    </td>
                                                    <td><?php echo e($user->ReferralCode); ?></td>
                                                    <?php switch($user->UserSignupType):
                                                        case (1): ?>
                                                        <td><?php echo app('translator')->getFromJson('admin.Normal'); ?></td>
                                                        <?php break; ?>
                                                        <?php case (2): ?>
                                                        <td><?php echo app('translator')->getFromJson('admin.Google'); ?></td>
                                                        <?php break; ?>
                                                        <?php case (3): ?>
                                                        <td><?php echo app('translator')->getFromJson('admin.Facebook'); ?></td>
                                                        <?php break; ?>
                                                    <?php endswitch; ?>

                                                    <?php switch($user->UserSignupFrom):
                                                        case (1): ?>
                                                        <td><?php echo app('translator')->getFromJson('admin.application'); ?></td>
                                                        <?php break; ?>
                                                        <?php case (2): ?>
                                                        <td><?php echo app('translator')->getFromJson('admin.admin'); ?></td>
                                                        <?php break; ?>
                                                        <?php case (3): ?>
                                                        <td><?php echo app('translator')->getFromJson('admin.web'); ?></td>
                                                        <?php break; ?>
                                                    <?php endswitch; ?>
                                                    <td><?php echo e($user->created_at->toformatteddatestring()); ?></td>
                                                    <td><?php echo e($user->updated_at->toformatteddatestring()); ?></td>
                                                    <td>
                                                        <?php if($user->UserStatus == 1): ?>
                                                            <label class="label_success"><?php echo app('translator')->getFromJson('admin.active'); ?></label>
                                                        <?php else: ?>
                                                            <label class="label_danger"><?php echo app('translator')->getFromJson('admin.deactive'); ?></label>
                                                        <?php endif; ?>
                                                    </td>

                                                    <td>


                                                            <span data-target="#sendNotificationModelUser"
                                                                  data-toggle="modal" id="<?php echo e($user->id); ?>"><a
                                                                        data-original-title="Send Notification"
                                                                        data-toggle="tooltip"
                                                                        id="<?php echo e($user->id); ?>" data-placement="top"
                                                                        class="btn menu-icon btn_detail action_btn"> <i
                                                                            class="fa fa-bell"></i> </a></span>

                                                        <span data-target="#addMoneyModel"
                                                              data-toggle="modal" id="<?php echo e($user->id); ?>"><a
                                                                    data-original-title="Add Money"
                                                                    data-toggle="tooltip"
                                                                    id="<?php echo e($user->id); ?>" data-placement="top"
                                                                    class="btn menu-icon btn_detail action_btn"> <i
                                                                        class="fa fa-money"></i> </a></span>

                                                        <a href="<?php echo e(route('users.edit',$user->id)); ?>"
                                                           data-original-title="Edit" data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_edit action_btn"> <i
                                                                    class="fa fa-pencil"></i> </a>

                                                        <a href="<?php echo e(route('user.wallet',$user->id)); ?>"
                                                           data-original-title="User Wallet" data-toggle="tooltip"
                                                           data-placement="top"
                                                           class="btn menu-icon btn_money action_btn"> <i
                                                                    class="fa fa-money"></i> </a>


                                                        <a href="<?php echo e(route('users.show',$user->id)); ?>"
                                                           class="btn menu-icon btn_delete action_btn"
                                                           data-original-title="<?php echo app('translator')->getFromJson('admin.message68'); ?>"
                                                           data-toggle="tooltip"
                                                           data-placement="top"><span
                                                                    class="fa fa-user"></span></a>

                                                        <?php if($user->UserStatus == 1): ?>
                                                            <a href="<?php echo e(route('user.active-deactive',['id'=>$user->id,'status'=>2])); ?>"
                                                               data-original-title="Inactive" data-toggle="tooltip"
                                                               data-placement="top"
                                                               class="btn menu-icon btn_eye_dis action_btn"> <i
                                                                        class="fa fa-eye-slash"></i> </a>
                                                        <?php else: ?>
                                                            <a href="<?php echo e(route('user.active-deactive',['id'=>$user->id,'status'=>1])); ?>"
                                                               data-original-title="Active" data-toggle="tooltip"
                                                               data-placement="top"
                                                               class="btn menu-icon btn_eye action_btn"> <i
                                                                        class="fa fa-eye"></i> </a>
                                                        <?php endif; ?>
                                                    </td>

                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1"><?php echo e($users->links()); ?></div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="sendNotificationModelUser" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel33"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33"><?php echo app('translator')->getFromJson('admin.message299'); ?></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="#" enctype="multipart/form-data" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="modal-body">
                        <label><?php echo app('translator')->getFromJson('admin.message63'); ?>: </label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="title"
                                   name="title"
                                   placeholder="<?php echo app('translator')->getFromJson('admin.message63'); ?>" required>
                        </div>

                        <label><?php echo app('translator')->getFromJson('admin.message64'); ?>: </label>
                        <div class="form-group">
                           <textarea class="form-control" id="message" name="message"
                                     rows="3"
                                     placeholder="<?php echo app('translator')->getFromJson('admin.message64'); ?>"></textarea>
                        </div>

                        <label><?php echo app('translator')->getFromJson('admin.message65'); ?>: </label>
                        <div class="form-group">
                            <input type="file" class="form-control" id="image"
                                   name="image"
                                   placeholder="<?php echo app('translator')->getFromJson('admin.message65'); ?>" required>
                            <input type="hidden" name="persion_id" id="persion_id">
                        </div>

                        <label><?php echo app('translator')->getFromJson('admin.message66'); ?>: </label>
                        <div class="form-group">
                            <input type="url" class="form-control" id="url"
                                   name="url"
                                   placeholder="<?php echo app('translator')->getFromJson('admin.message66'); ?>" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                        <input type="submit" class="btn btn-outline-primary btn-lg" value="Send">
                    </div>
                </form>
            </div>
        </div>
    </div>

    
    <div class="modal fade text-left" id="addMoneyModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600" id="myModalLabel33"><?php echo app('translator')->getFromJson('admin.message200'); ?></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo e(route('user.add.wallet')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="modal-body">
                        <label><?php echo app('translator')->getFromJson('admin.message203'); ?>: </label>
                        <div class="form-group">
                            <select class="form-control" name="payment_method" id="payment_method" required>
                                <option value="1"><?php echo app('translator')->getFromJson('admin.message201'); ?></option>
                                <option value="2"><?php echo app('translator')->getFromJson('admin.message202'); ?></option>
                            </select>
                        </div>

                        <label><?php echo app('translator')->getFromJson('admin.message205'); ?>: </label>
                        <div class="form-group">
                            <input type="text" name="amount" placeholder="<?php echo app('translator')->getFromJson('admin.message205'); ?>"
                                   class="form-control" required>
                            <input type="hidden" name="add_money_user_id" id="add_money_driver_id">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                        <input type="submit" class="btn btn-outline-primary btn-lg" value="Add">
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>