@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.DriverManagement')</h3>
                </div>
                <div class="content-header-right col-md-4 col-12">
                    <div class="btn-group float-md-right">
                        <div class="heading-elements">
                            {{--<a href="{{ route('admin.driver.pending.show') }}">--}}
                                {{--<button class="btn btn-secondary btn-sm" style="position:relative;">--}}
                                    {{--<i  class="fa fa-bell"></i> @lang('admin.message280')--}}
                                    {{--<span style="position:absolute !important; left:0 !important; right: inherit !important; top:-10px !important;" class="badge badge-pill badge-default badge-info badge-default badge-up">{{ $pendingdrivers }}</span>--}}
                                {{--</button>--}}

                            {{--</a>--}}
                            <a href="{{route('drivers.create')}}">
                                <button type="button" class="btn btn-icon btn-success mr-1" title="@lang('admin.AddDriver')"><i class="fa fa-plus"></i>
                                </button>
                            <!--<button class="btn btn-primary btn-sm"><i
                                            class="ft-plus white"></i> @lang('admin.AddDriver')
                                    </button>-->
                            </a>

                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="horizontal">
                    <div class="row">
                        @if(session('moneyAdded'))
                            <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                                <span class="alert-icon"><i class="fa fa-info"></i></span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong>@lang('admin.message207')</strong>
                            </div>
                        @endif
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="">
                                        <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                            <thead>
                                            <tr>
                                                <th>@lang('admin.driverId')</th>
                                                <th>@lang('admin.fullname')</th>
                                                <th>@lang('admin.email')</th>
                                                <th>@lang('admin.merchantPhone')</th>
                                                <th>@lang('admin.message276')</th>
                                                <th>@lang('admin.message277')</th>
                                                <th>@lang('admin.message282')</th>
                                                <th>@lang('admin.message283')</th>
                                                <th>@lang('admin.message292')</th>
                                                <th>@lang('admin.Registerdate')</th>
                                                <th>@lang('admin.lastUpdate')</th>
                                                <th>@lang('admin.Status')</th>
                                                <th>@lang('admin.action')</th>
                                            </tr>
                                            </thead>
                                            @foreach($drivers as $driver)
                                                <tr>
                                                    <td>{{ $driver->id }}</td>
                                                    <td>{{ $driver->driver_name }}</td>
                                                    <td>{{ $driver->driver_email }}</td>
                                                    <td>{{ $driver->driver_phone }}</td>

                                                    <td>
                                                        @if ($driver->rating == "0.0")
                                                            @lang('admin.message278')
                                                        @else
                                                            @while($driver->rating>0)
                                                                @if($driver->rating >0.5)
                                                                    <img src="{{ asset("star.png") }}"
                                                                         alt='Whole Star'>
                                                                @else
                                                                    <img src="{{ asset('halfstar.png') }}"
                                                                         alt='Half Star'>
                                                                @endif
                                                                @php $driver->rating--; @endphp
                                                            @endwhile
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($driver->total_trips)
                                                            {{ $driver->total_trips }}
                                                        @else
                                                            @lang('admin.message279')
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($driver->total_earnings)
                                                            {{ $driver->total_earnings }}
                                                        @else
                                                            @lang('admin.message279')
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($driver->total_comany_earning)
                                                            {{ $driver->total_comany_earning }}
                                                        @else
                                                            @lang('admin.message279')
                                                        @endif
                                                    </td>
                                                    <td><a href="">{{ $driver->wallet_money }}</a></td>
                                                    <td>{{ $driver->created_at }}</td>
                                                    <td>{{ $driver->updated_at }}</td>
                                                    <td>
                                                        @if($driver->login_logout == 1)
                                                            @if($driver->online_offline == 1)
                                                                <label class="label_success"
                                                                       style="width:48px;display: inline-block;"> @lang('admin.message314')</label>
                                                            @else
                                                                <label class="label_info"
                                                                       style="width:48px;display: inline-block;"> @lang('admin.message315')</label>
                                                            @endif
                                                        @else
                                                            <label class="label_danger">@lang('admin.message313')</label>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <span data-target="#sendNotificationModel"
                                                              data-toggle="modal" id="{{ $driver->id }}"><a
                                                                    data-original-title="Send Notification"
                                                                    data-toggle="tooltip"
                                                                    id="{{ $driver->id }}" data-placement="top"
                                                                    class="btn menu-icon btn_eye action_btn"> <i
                                                                        class="fa fa-bell"></i> </a></span>

                                                        <span data-target="#addMoneyModel"
                                                              data-toggle="modal" id="{{ $driver->id }}"><a
                                                                    data-original-title="Add Money"
                                                                    data-toggle="tooltip"
                                                                    id="{{ $driver->id }}" data-placement="top"
                                                                    class="btn menu-icon btn_detail action_btn"> <i
                                                                        class="fa fa-money"></i> </a></span>

                                                        <a href=" "
                                                           class="btn menu-icon btn_money action_btn"><span
                                                                    class="fa fa-history"
                                                                    title="Driver Wallet"></span></a>

                                                        <a href=""
                                                           class="btn menu-icon btn_detail action_btn"><span
                                                                    class="fa fa-list-alt" title="View Driver Profile"></span></a>


                                                        @if($driver->driver_admin_status == 1)
                                                            <a href=""
                                                               data-original-title="Inactive"
                                                               data-toggle="tooltip" data-placement="top"
                                                               class="btn menu-icon btn_eye_dis action_btn"> <i
                                                                        class="fa fa-eye-slash"></i> </a>
                                                        @else
                                                            <a href=""
                                                               data-original-title="Active"
                                                               data-toggle="tooltip" data-placement="top"
                                                               class="btn menu-icon btn_eye action_btn"> <i class="fa fa-eye"></i>
                                                            </a>
                                                        @endif
                                                        @if($driver->login_logout == 1)
                                                            <a href=""
                                                               data-original-title="Logout"
                                                               data-toggle="tooltip" data-placement="top"
                                                               class="btn menu-icon btn_delete action_btn"> <i
                                                                        class="fa fa-sign-out"></i>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pagination1">{{ $drivers->links() }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    {{--<div class="modal fade text-left" id="sendNotificationModel" tabindex="-1" role="dialog"--}}
         {{--aria-labelledby="myModalLabel33"--}}
         {{--aria-hidden="true">--}}
        {{--<div class="modal-dialog" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<label class="modal-title text-text-bold-600" id="myModalLabel33">@lang('admin.message300')</label>--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true">&times;</span>--}}
                    {{--</button>--}}
                {{--</div>--}}
                {{--<form action="{{ route('merchant.sendsingle-driver') }}" method="post" enctype="multipart/form-data">--}}
                    {{--@csrf--}}
                    {{--<div class="modal-body">--}}
                        {{--<label>@lang('admin.message63'): </label>--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="text" class="form-control" id="title"--}}
                                   {{--name="title"--}}
                                   {{--placeholder="@lang('admin.message63')" required>--}}
                        {{--</div>--}}

                        {{--<label>@lang('admin.message64'): </label>--}}
                        {{--<div class="form-group">--}}
                           {{--<textarea class="form-control" id="message" name="message"--}}
                                     {{--rows="3"--}}
                                     {{--placeholder="@lang('admin.message64')"></textarea>--}}
                        {{--</div>--}}

                        {{--<label>@lang('admin.message65'): </label>--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="file" class="form-control" id="image"--}}
                                   {{--name="image"--}}
                                   {{--placeholder="@lang('admin.message65')" required>--}}
                            {{--<input type="hidden" name="persion_id" id="persion_id">--}}
                        {{--</div>--}}

                        {{--<label>@lang('admin.message66'): </label>--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="url" class="form-control" id="url"--}}
                                   {{--name="url"--}}
                                   {{--placeholder="@lang('admin.message66')" required>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="modal-footer">--}}
                        {{--<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">--}}
                        {{--<input type="submit" class="btn btn-outline-primary btn-lg" value="Send">--}}
                    {{--</div>--}}
                {{--</form>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="modal fade text-left" id="addMoneyModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"--}}
         {{--aria-hidden="true">--}}
        {{--<div class="modal-dialog" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<label class="modal-title text-text-bold-600" id="myModalLabel33">@lang('admin.message200')</label>--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true">&times;</span>--}}
                    {{--</button>--}}
                {{--</div>--}}
                {{--<form action="{{ route('merchant.AddMoney') }}" method="post">--}}
                    {{--@csrf--}}
                    {{--<div class="modal-body">--}}
                        {{--<label>@lang('admin.message203'): </label>--}}
                        {{--<div class="form-group">--}}
                            {{--<select class="form-control" name="payment_method" id="payment_method" required>--}}
                                {{--<option value="1">@lang('admin.message201')</option>--}}
                                {{--<option value="2">@lang('admin.message202')</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        {{--<label>@lang('admin.message204'): </label>--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="text" name="receipt_number" placeholder="@lang('admin.message204')"--}}
                                   {{--class="form-control" required>--}}
                        {{--</div>--}}

                        {{--<label>@lang('admin.message205'): </label>--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="text" name="amount" placeholder="@lang('admin.message205')"--}}
                                   {{--class="form-control" required>--}}
                            {{--<input type="hidden" name="add_money_driver_id" id="add_money_driver_id">--}}
                        {{--</div>--}}

                        {{--<label>@lang('admin.message206'): </label>--}}
                        {{--<div class="form-group">--}}
                            {{--<textarea class="form-control" id="title1" rows="3" name="description"--}}
                                      {{--placeholder="@lang('admin.message206')"></textarea>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="modal-footer">--}}
                        {{--<input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">--}}
                        {{--<input type="submit" class="btn btn-outline-primary btn-lg" value="Add">--}}
                    {{--</div>--}}
                {{--</form>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection
