<?php

namespace App\Http\Controllers\Admin;

use App\Models\LanguageCmsPage;
use App;
use App\Models\CmsPage;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CmsPagesController extends Controller
{

    public function index()
    {
        $cmspages = CmsPage::get();
        return view('admin.cms.index', compact('cmspages'));
    }

    public function create()
    {
        $pages = Page::get();
        return view('admin.cms.create', compact('pages'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'application' => 'required|integer|between:1,2',
            'page' => 'required|exists:pages,slug',
            'title' => 'required',
            'description' => 'required',
        ]);
        $cmsPage = CmsPage::updateOrCreate(
            ['application' => $request->application, 'slug' => $request->page,'description'=>$request->description],
            ['status' => 1]
        );
        $this->SaveLanguageCms($cmsPage->id, $request->title, $request->description);
        return redirect()->route('cms.index');
    }

    public function SaveLanguageCms($cms_page_id, $title, $description)
    {
        LanguageCmsPage::updateOrCreate([
            'locale' => App::getLocale(), 'cms_page_id' => $cms_page_id
        ], [
            'title' => $title,
            'description' => $description,
        ]);
    }

    public function edit($id)
    {
        $cmspage = CmsPage::findorFail($id);
        return view('admin.cms.edit', compact('cmspage'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
        $cmspage = CmsPage::findorFail($id);
        $this->SaveLanguageCms($cmspage->id, $request->title, $request->description);
        return redirect()->back()->with('cmsupdate', 'Cms Update');
    }

    public function Search(Request $request)
    {
        $query = CmsPage::where([['created_at', '!=', ""]]);
        if ($request->pagetitle) {
            $keyword = $request->pagetitle;
            $query->WhereHas('LanguageSingle', function ($q) use ($keyword) {
                $q->where('title', 'LIKE', "%$keyword%");
            });
        }
        $cmspages = $query->get();
        return view('merchant.cms.index', compact('cmspages'));
    }
}
