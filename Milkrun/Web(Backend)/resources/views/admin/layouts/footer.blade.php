<footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright  &copy; 2018 <a class="text-bold-800 grey darken-2" href="#" >ApporioTaxi </a>, All rights reserved. </span><span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span></p>
</footer>


<script src="{{ asset('js/vendor.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('js/menu.js') }}/" type="text/javascript"></script>
<script src="{{ asset('js/themeapp.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/datedropper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/summernote.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery-clock-timepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/my-script.js') }}" type="text/javascript"></script>

</body>
</html>