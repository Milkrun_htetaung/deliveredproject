@extends('admin.layouts.main')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">@lang('admin.melvin_retailer_edit_message1')</h3>
                </div>
                <div class="col-md-6 col-12">
                    @if(session('rideradded'))
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>@lang('admin.melvin_retailer_edit_message2')</strong>
                        </div>
                    @endif
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="{{ route('retailer.index') }}">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data"
                                              action="{{route('retailer.update', $user->id)}}">
                                            {{method_field('PUT')}}
                                            @csrf
                                            <fieldset>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                @lang('admin.melvin_retailer_edit_message3') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="retailer_shop_name"
                                                                   name="user_name" value="{{$user->retailer_shop_name}}"
                                                                   placeholder="@lang('admin.melvin_retailer_edit_message3')" required>
                                                            @if ($errors->has('retailer_shop_name'))
                                                                <label class="danger">{{ $errors->first('retailer_shop_name') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                @lang('admin.melvin_retailer_edit_message4') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="user_name"
                                                                   name="user_name" value="{{$user->UserName}}"
                                                                   placeholder="@lang('admin.melvin_retailer_edit_message4')" required>
                                                            @if ($errors->has('user_name'))
                                                                <label class="danger">{{ $errors->first('user_name') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                @lang('admin.melvin_retailer_edit_message5') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="retailer_shop_address"
                                                                   name="retailer_shop_address"
                                                                   placeholder="@lang('admin.melvin_retailer_edit_message5')"
                                                                   value="{{$user->retailer_shop_address}}" required>
                                                            @if ($errors->has('retailer_shop_address'))
                                                                <label class="danger">{{ $errors->first('retailer_shop_address') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>



                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="lastName3">
                                                                @lang('admin.melvin_retailer_edit_message6') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="user_phone"
                                                                   name="user_phone"
                                                                   placeholder="@lang('admin.melvin_retailer_edit_message6')"
                                                                   value="{{$user->UserPhone}}" required>
                                                            @if ($errors->has('user_phone'))
                                                                <label class="danger">{{ $errors->first('user_phone') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>


                                                </div>


                                                <div class="row">




                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                @lang('admin.melvin_retailer_edit_message7') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="email" class="form-control" id="user_email"
                                                                   name="user_email"
                                                                   placeholder="@lang('admin.melvin_retailer_edit_message7')"
                                                                   value="{{$user->email}}" required>
                                                            @if ($errors->has('user_email'))
                                                                <label class="danger">{{ $errors->first('user_email') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ProfileImage">
                                                                @lang('admin.melvin_retailer_edit_message8') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="file" class="form-control" id="profile"
                                                                   name="profile"
                                                                   placeholder="@lang('admin.melvin_retailer_edit_message8')">
                                                            @if ($errors->has('profile'))
                                                                <label class="danger">{{ $errors->first('profile') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                @lang('admin.melvin_retailer_edit_message9') :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="password" class="form-control" id="password"
                                                                   name="password"
                                                                   placeholder="@lang('admin.melvin_retailer_edit_message9')" disabled>
                                                            @if ($errors->has('password'))
                                                                <label class="danger">{{ $errors->first('password') }}</label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <fieldset class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="1" name="edit_password"
                                                                       id="edit_password" onclick="EditPassword()">
                                                                @lang('admin.melvin_retailer_edit_message10')
                                                            </label>
                                                        </fieldset>
                                                    </div>
                                                </div>


                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> @lang('admin.melvin_retailer_edit_message11')
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script>
        function EditPassword() {
            if (document.getElementById("edit_password").checked = true) {
                document.getElementById('password').disabled = false;
            }
        }

    </script>
@endsection
