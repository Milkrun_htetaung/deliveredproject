<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RetailerRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'retailer_shop' => ['required'],
            'retailer_contact_name' => ['required'],
            'retailer_shop_address'=>['required','unique:retailers,retailer_shop_address'],
            'retailer_phone' => ['required','unique:retailers,retailer_phone'],
            'retailer_email' => ['required','unique:retailers,retailer_email'],
            'retailer_password' => "required|min:6",
            'profile' => 'required|file'
        ];
    }

    public function messages()
    {
        return [
            'retailer_shop.required' => trans('admin.retailerShopName'),
            'retailer_contact_name.required' => trans('admin.retailerName'),
            'retailer_phone.required' => trans('admin.retailphoneRequire'),
            'retailer_phone.unique' => trans('admin.retailphoneUnique'),
            'retailer_email.required' => trans('admin.retailemailRequire'),
            'retailer_email.unique' => trans('admin.retailemailUnique'),
            'retailer_password.required' => trans('admin.passwordRequire'),
            'profile.required' => trans('admin.profileRequire'),
            'retailer_shop_address.required' => trans('admin.retailershopaddress'),
        ];
    }
}
