<?php
/**
 * Created by PhpStorm.
 * User: Yogesh Apporio
 * Date: 20-12-2018
 * Time: 11:34
 */

namespace App\Http\Controllers\API\Profiles\Retailer;

use App\Models\User;
use App\Models\Driver;
use App\Models\Retailer;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Client;

class RetailerProfileController extends Controller
{
    /************ ViewProfile APIs ************/
    public function View_Profile(Request $request)
    {
        $retailer = $request->user('api_retailer');
        return response()->json(['result' => "1", 'message' => 'Retailer Profile', 'data' => ['retailer' => $retailer, 'app_type' => "1"]]);

    }

    public function Edit_Profile(Request $request)
    {

        $retailer = $request->user('api_retailer');
        $validator = Validator::make($request->all(),
            [
                'retailer_shop' => "required",
                'retailer_contact_name' => "required",
                'retailer_email' => ['required', 'max:255', 'unique:retailers,retailer_email'],
                'retailer_shop_address' => "required",
            ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
        }
        if ($request->profile_image) {
            list($format, $image) = explode(',', $request->profile_image);
            $temp = explode('/', $format);
            list($ext,) = explode(';', $temp[1]);
            $file_name = time() . "_retailer_edit_profile." . $ext;
            file_put_contents(public_path() . '/images/retailer/' . $file_name, base64_decode($image));
            $retailer->retailer_image = env('APP_URL')."/images/retailer/".$file_name;
        }
        $retailer->retailer_shop = $request->retailer_shop;
        $retailer->retailer_contact_name = $request->retailer_contact_name;
        $retailer->retailer_email = $request->retailer_email;
        $retailer->retailer_shop_address = $request->retailer_shop_address;
        $retailer->save();
        return response()->json(['result' => "1", 'message' => "Profile Updated", 'data' => ['retailer' => $retailer, 'app_type' => "1"]]);
    }

}