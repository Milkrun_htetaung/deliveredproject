<?php
/**
 * Created by PhpStorm.
 * User: Yogesh Apporio
 * Date: 20-12-2018
 * Time: 11:34
 */

namespace App\Http\Controllers\API\Profiles\Driver;

use App\Models\User;
use App\Models\Driver;
use App\Models\Rating;
use App\Models\Retailer;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Client;

class DriverProfileController extends Controller
{
    /************ ViewProfile APIs ************/
    public function View_Profile(Request $request)
    {
        $driver = $request->user('api_driver');
        return response()->json(['result' => "1", 'message' => 'Driver Profile', 'data' => ['driver' => $driver, 'app_type' => "2"]]);

    }

    public function Edit_Profile(Request $request)
    {
        $driver = $request->user('api_driver');
        $validator = Validator::make($request->all(),
            [
                'driver_name' => 'required',
                'driver_email' => ['required', 'unique:drivers,driver_email'],
            ]);
        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
        }
        if ($request->profile_image) {
            list($format, $image) = explode(',', $request->profile_image);
            $temp = explode('/', $format);
            list($ext,) = explode(';', $temp[1]);
            $file_name = time() . "_driver_edit_profile." . $ext;
            file_put_contents(public_path() . '/images/driver/' . $file_name, base64_decode($image));
            $driver->driver_image = env('APP_URL')."/images/driver/".$file_name;
        }
        
        
        $driver->driver_name = $request->driver_name;
        $driver->driver_email = $request->driver_email;
        $driver->save();
        return response()->json(['result' => "1", 'message' => "Profile Updated", 'data' => ['driver' => $driver, 'app_type' => "2"]]);
    }
    
    public function Rating_Driver(Request $request)
    {
        $driver = $request->user('api_driver');
        $validator = Validator::make($request->all(),
            [
                'driver_id' => 'required',
                'order_id' => 'required',
                'driver_rating_star' => 'required',
                'driver_comment' => 'required',
                  
            ]);
        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
        }
        $rating = new Rating;
        $rating->driver_id = $request->driver_id;
        $rating->order_id = $request->order_id;
        $rating->driver_rating_star = $request->driver_rating_star;
        $rating->driver_comment = $request->driver_comment;
        $rating->rating_date = date('Y-m-d');
        $rating->save();
        $driver_id = $request->driver_id;
        $count = Rating::where('driver_id', $driver_id)->count(); 
        $all_data = Rating::where('driver_id', $driver_id)->get();  
        $all_data = json_decode($all_data);
        $all_rating=0;
        foreach($all_data as $val)
        {
            $all_rating += $val->driver_rating_star;
        }
        $driver_rating = $all_rating/$count;  
        $driver->driver_rating = number_format($driver_rating, 2, '.', '');;
        $driver->save();
         
        return response()->json(['result' => "1", 'message' => "Rating Updated", 'data' => []]);
    }

}