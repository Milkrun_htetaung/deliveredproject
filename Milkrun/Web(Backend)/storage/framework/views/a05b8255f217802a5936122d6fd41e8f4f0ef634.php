
<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo app('translator')->getFromJson('admin.AddRider'); ?></h3>
                </div>
                <div class="col-md-6 col-12">
                    <?php if(session('rideradded')): ?>
                        <div class="col-md-6 alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="fa fa-info"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo app('translator')->getFromJson('admin.rideradded'); ?></strong>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="content-header-right col-md-2 col-12">
                    <div class="btn-group float-md-right">
                        <a href="<?php echo e(route('users.index')); ?>">
                            <button type="button" class="btn btn-icon btn-success mr-1"><i class="fa fa-reply"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>


            <div class="content-body">
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="POST" class="steps-validation wizard-notification"
                                              enctype="multipart/form-data" action="<?php echo e(route('users.store')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="location3"><?php echo app('translator')->getFromJson('admin.RiderType'); ?> :</label>
                                                            <select class="form-control" name="rider_type"
                                                                    id="rider_type" onclick="RideType(this.value)"
                                                                    required>
                                                                <option value="">--Select Rider Type--</option>
                                                                <option value="1">Corporate</option>
                                                                <option value="2">Retail</option>
                                                            </select>

                                                            <?php if($errors->has('rider_type')): ?>
                                                                <label class="danger"><?php echo e($errors->first('rider_type')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                <?php echo app('translator')->getFromJson('admin.RiderName'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="user_name"
                                                                   name="user_name"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.RiderName'); ?>" required>
                                                            <?php if($errors->has('user_name')): ?>
                                                                <label class="danger"><?php echo e($errors->first('user_name')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row custom-hidden" id="corporate_div">
                                                    <div class="col-md-6 corporate_inr">
                                                        <div class="form-group">
                                                            <label for="location3"><?php echo app('translator')->getFromJson('admin.corporate_name'); ?>
                                                                :</label>
                                                            <select class="form-control" name="corporate_id"
                                                                    id="corporate_id">
                                                                <option value="">--Select Corporate--</option>
                                                                <?php $__currentLoopData = $corporates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $corporate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="<?php echo e($corporate->id); ?>"><?php echo e($corporate->corporate_name); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>

                                                            <?php if($errors->has('rider_type')): ?>
                                                                <label class="danger"><?php echo e($errors->first('rider_type')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 corporate_inr">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                                <?php echo app('translator')->getFromJson('admin.corporateemail'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="corporate_email"
                                                                   name="corporate_email"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.corporateemail'); ?>">
                                                            <?php if($errors->has('corporate_email')): ?>
                                                                <label class="danger"><?php echo e($errors->first('corporate_email')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="location3"><?php echo app('translator')->getFromJson('admin.message293'); ?> :</label>
                                                            <select class="form-control" name="country" id="country"
                                                                    required>
                                                                <option value=""><?php echo app('translator')->getFromJson('admin.message294'); ?></option>
                                                                <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option data-min="<?php echo e($country->maxNumPhone); ?>"
                                                                            data-max="<?php echo e($country->maxNumPhone); ?>"
                                                                            value="<?php echo e($country->phonecode); ?>"><?php if($country->LanguageCountrySingle): ?> <?php echo e($country->LanguageCountrySingle->name); ?> <?php else: ?> <?php echo e($country->LanguageCountryAny->name); ?> <?php endif; ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                            <?php if($errors->has('country')): ?>
                                                                <label class="danger"><?php echo e($errors->first('country')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="lastName3">
                                                                <?php echo app('translator')->getFromJson('admin.RiderPhone'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="user_phone"
                                                                   name="user_phone"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.RiderPhone'); ?>" required>
                                                            <?php if($errors->has('user_phone')): ?>
                                                                <label class="danger"><?php echo e($errors->first('user_phone')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                <?php echo app('translator')->getFromJson('admin.RiderEmail'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="email" class="form-control" id="user_email"
                                                                   name="user_email"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.RiderEmail'); ?>" required>
                                                            <?php if($errors->has('user_email')): ?>
                                                                <label class="danger"><?php echo e($errors->first('user_email')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailAddress5">
                                                                <?php echo app('translator')->getFromJson('admin.RiderPassword'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="password" class="form-control" id="password"
                                                                   name="password"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.RiderPassword'); ?>" required>
                                                            <?php if($errors->has('password')): ?>
                                                                <label class="danger"><?php echo e($errors->first('password')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ProfileImage">
                                                                <?php echo app('translator')->getFromJson('admin.ProfileImage'); ?> :
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="file" class="form-control" id="profile"
                                                                   name="profile"
                                                                   placeholder="<?php echo app('translator')->getFromJson('admin.ProfileImage'); ?>" required>
                                                            <?php if($errors->has('profile')): ?>
                                                                <label class="danger"><?php echo e($errors->first('profile')); ?></label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script>
        function RideType(val) {
            if (val == "1") {
                document.getElementById('corporate_div').style.display = 'block';
            } else {
                document.getElementById('corporate_div').style.display = 'none';
            }
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>