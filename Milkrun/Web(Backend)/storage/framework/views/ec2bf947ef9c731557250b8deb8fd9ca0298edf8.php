<div class="main-menu menu-fixed menu-dark menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="nav-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="icon-globe"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.message43'); ?></span></a></li>

            <li class=" nav-item"><a href="#"><i class="fa fa-user-secret"></i><span class="menu-title"
                                                                                     data-i18n="nav.adminMangement.main"><?php echo app('translator')->getFromJson('admin.sub-admin'); ?></span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="#"
                           data-i18n="nav.active_ride.1_column"><?php echo app('translator')->getFromJson('admin.message401'); ?></a>
                    </li>


                    <li><a class="menu-item" href="#"
                           data-i18n="nav.active_ride.1_column"><?php echo app('translator')->getFromJson('admin.role'); ?></a>
                    </li>

                </ul>
            </li>


            <li class="nav-item"><a
                    href="<?php echo e(route('drivers.index')); ?>"><i class="icon-key"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.DriverManagement'); ?></span></a>
            </li>

            <li class="nav-item"><a
                    href="<?php echo e(route('customers.index')); ?>"><i class="icon-graduation"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.melvin_customer_message1'); ?></span></a>
            </li>

            <li class="nav-item"><a
                    href="<?php echo e(route('retailer.index')); ?>"><i class="icon-user"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.melvin_sidebar_message1'); ?></span></a>
            </li>

            <li class="nav-item"><a
                    href="<?php echo e(route('orders.index')); ?>"><i class="icon-organization"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.melvin_order_message1'); ?></span></a>
            </li>

            <li class="nav-item"><a
                    href="<?php echo e(route('stations.index')); ?>"><i class="fa fa-bell-o"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.melvin_station_message1'); ?></span></a></li>

            <li class="nav-item"><a
                    href="<?php echo e(route('measurements.index')); ?>"><i class="icon-settings"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.melvin_measurement_message1'); ?></span></a></li>

            <li class="nav-item"><a href="<?php echo e(route('documents.index')); ?>"><i class="fa fa-file"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.melvin_sidebar_message2'); ?></span></a></li>

            <li class="nav-item"><a
                    href="<?php echo e(route('onesignal.index')); ?>"><i class="fa fa-bell-o"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.message85'); ?></span></a></li>

            <li class="nav-item"><a
                    href="<?php echo e(route('promotions.index')); ?>"><i class="fa fa-bell-o"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.message60'); ?></span></a></li>

            <li class="nav-item"><a
                    href="<?php echo e(route('customer_support.index')); ?>"><i class="fa fa-headphones"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.message379'); ?></span></a></li>

            <li class="nav-item"><a href="<?php echo e(route('cms.index')); ?>"><i class="fa fa-columns"></i><span
                        class="menu-title"><?php echo app('translator')->getFromJson('admin.message208'); ?></span></a></li>


        </ul>
    </div>
</div>
