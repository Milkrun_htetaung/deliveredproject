<?php
/**
 * Created by PhpStorm.
 * User: Yogesh Apporio
 * Date: 20-12-2018
 * Time: 16:14
 * app_type ==> 1 For Retailer
 * app_type ==> 2 For Deliverer
 * app_type ==> 3 For User
 */

namespace App\Http\Controllers\API\Order;

use App\Models\Driver;
use App\Models\Retailer;
use App\Models\Customer;
use App\Models\Country;
use App\Models\Onesignal;
use App\Models\OrderManagement;
use App\Models\Station;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Client;

class OrderController extends Controller
{

    public function Post_Order(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'customer_id' => 'required|exists:customers,id',
                'mrt_station_id' => 'required|exists:stations,id',
                'size' => 'required',
                'quantity' => 'required',
                'weight' => 'required',
//                'images' => 'required',
//                'documents' => 'required'
            ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors['0'], 'data' => []]);
        }
        $retailer = $request->user('api_retailer');

        $retailer_id = $retailer->id;
        $pickup_address = $retailer->retailer_shop_address;
        $drop_address = Station::find($request->mrt_station_id);
        $drop_address=$drop_address->station_address;
//        list($format, $image) = explode(',', $request->images);
//        $temp = explode('/', $format);
//        list($ext,) = explode(';', $temp[1]);
//        $file_name = time() . "_register_driver." . $ext;
//        file_put_contents(public_path() . '/images/orders/' . $file_name, base64_decode($image));
//        $images = env('APP_URL')."/images/orders/".$file_name;
//
//        list($format, $image) = explode(',', $request->documents);
//        $temp = explode('/', $format);
//        list($ext,) = explode(';', $temp[1]);
//        $file_name = time() . "_order_document." . $ext;
//        file_put_contents(public_path() . '/images/orders/' . $file_name, base64_decode($image));
//        $documents = env('APP_URL')."/images/orders/".$file_name;
        
        
        $order_data = OrderManagement::create([
            'driver_id' => '0',
            'retailer_id' => $retailer_id,
            'customer_id' => $request->customer_id,
            'pickup_address' => $pickup_address,
            'dropoff_address' => $drop_address,
            'size' => $request->size,
            'weight' => $request->weight,
            'quantity' => $request->quantity,
//            'images' => $images,
//            'documents' => $documents,
//            'delivery_charges' => $request->delivery_charges,
            'delivery_charges' => "100",
            'payment_id' => 1,
            'order_status' => 1,
            'posting_time' => strtotime('now'),
            'order_status_message' => "Order Placed"
        ]);

        $driverdevices = Driver::where([['player_id', '!=', ""],['driver_admin_status', '=', "1"],['driver_status', '=', "1"]])->get();
        $playerids = array_pluck($driverdevices, 'player_id');
        $data = $order_data->id;
        $message = "New Order Placed";
        Onesignal::UserPushMessage($playerids, $data, $message, 1,$retailer_id);
        return response()->json(['result' => "1", 'message' => "Orde Place Succesfully", 'data' => $order_data]);
    }


    /******** During Ride ********/
    public function OrderAccept(Request $request)
    {
        $driver = $request->user('api_driver');
        $driver_id = $driver->id;
        $validator = Validator::make($request->all(), [
            'booking_id' => [
                'required',
                'integer',
            ],
            'booking_id' => 'required|exists:order_managements,id',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
        }
        $booking_id = $request->booking_id;
        $booking_data = OrderManagement::find($booking_id);
        if($booking_data->driver_id == 0) {
            $retailer_data = Retailer::find($booking_data->retailer_id);
            OrderManagement::where([['id', '=', $booking_id]])->update(['order_status' => 2, 'driver_id' => $driver_id, 'order_status_message' => "Order Accepted"]);
            $message = "Order Accepted";
            $playerids[] = $retailer_data->player_id;
            $data = $booking_id;
            Onesignal::RetailerPushMessage($playerids, $data, $message, "2", $booking_data->retailer_id);
            $booking_data = OrderManagement::find($booking_id);
            return response()->json(['result' => "1", 'message' => "Order Accepted", 'data' => $booking_data]);
        }else{
            return response()->json(['result' => "0", 'message' => "Order Already Accepted", 'data' => []]);
        }

    }


    /******** During Ride ********/
    public function OrderPicked(Request $request)
    {
        $driver = $request->user('api_driver');
        $driver_id = $driver->id;
        $validator = Validator::make($request->all(), [
            'booking_id' => [
                'required',
                'integer',
            ],
            'booking_id' => 'required|exists:order_managements,id',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
        }
        $booking_id = $request->booking_id;
        $booking_data = OrderManagement::find($booking_id);
        if($booking_data->order_status == 2) {
            $retailer_data = Retailer::find($booking_data->retailer_id);
            OrderManagement::where([['id', '=', $booking_id]])->update(['order_status' => 3, 'order_status_message' => "Order Picked"]);
            $message = "Order Picked by Deliverer";
            //$playerids = array_pluck($retailer_data, 'player_id');
            $playerids[] = $retailer_data->player_id;
            $data = $booking_id;
            Onesignal::RetailerPushMessage($playerids, $data, $message, "3", $booking_data->retailer_id);
            $booking_data = OrderManagement::find($booking_id);
            return response()->json(['result' => "1", 'message' => "Order Accepted", 'data' => $booking_data]);
        }else{
            return response()->json(['result' => "0", 'message' => "Order Status Already Chnaged", 'data' => []]);
        }

    }

    /******** During Ride ********/
    public function OrderOnWay(Request $request)
    {
        $driver = $request->user('api_driver');
        $driver_id = $driver->id;
        $validator = Validator::make($request->all(), [
            'booking_id' => [
                'required',
                'integer',
            ],
            'booking_id' => 'required|exists:order_managements,id',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
        }
        $booking_id = $request->booking_id;
        $booking_data = OrderManagement::find($booking_id);
        if($booking_data->order_status == 3) {
            $retailer_data = Retailer::find($booking_data->retailer_id);
            OrderManagement::where([['id', '=', $booking_id]])->update(['order_status' => 4, 'order_status_message' => "Order On Way"]);
            $message = "Order On the Way";
            //$playerids = array_pluck($retailer_data, 'player_id');
            $playerids[] = $retailer_data->player_id;
            $data = $booking_id;
            Onesignal::RetailerPushMessage($playerids, $data, $message, "4", $booking_data->retailer_id);
            $booking_data = OrderManagement::find($booking_id);
            return response()->json(['result' => "1", 'message' => "Order Accepted", 'data' => $booking_data]);
        }else{
            return response()->json(['result' => "0", 'message' => "Order Status Already Chnaged", 'data' => []]);
        }

    }


    /******** During Ride ********/
    public function OrderEnd(Request $request)
    {
        $driver = $request->user('api_driver');
        $driver_id = $driver->id;
        $validator = Validator::make($request->all(), [
            'booking_id' => [
                'required',
                'integer',
            ],
            'booking_id' => 'required|exists:order_managements,id',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->all();
            return response()->json(['result' => "0", 'message' => $errors[0], 'data' => []]);
        }
        $booking_id = $request->booking_id;
        $booking_data = OrderManagement::find($booking_id);
        if($booking_data->order_status == 4) {
            $retailer_data = Retailer::find($booking_data->retailer_id);
            OrderManagement::where([['id', '=', $booking_id]])->update(['order_status' => 5, 'order_status_message' => "Order Delivered"]);
            $message = "Order Delivered";
            //$playerids = array_pluck($retailer_data, 'player_id');
            $playerids[] = $retailer_data->player_id;
            $data = $booking_id;
            Onesignal::RetailerPushMessage($playerids, $data, $message, "5", $booking_data->retailer_id);
            $booking_data = OrderManagement::find($booking_id);
            return response()->json(['result' => "1", 'message' => "Order Accepted", 'data' => $booking_data]);
        }else{
            return response()->json(['result' => "0", 'message' => "Order Status Already Chnaged", 'data' => []]);
        }

    }






}
