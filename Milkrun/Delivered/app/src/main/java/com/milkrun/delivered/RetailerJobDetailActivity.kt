package com.milkrun.delivered

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.milkrun.delivered.ui.retailer.jobDetail.RetailerJobDetailFragment

class RetailerJobDetailActivity : AppCompatActivity() {
    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, RetailerJobDetailActivity::class.java)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.retailer_job_detail_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, RetailerJobDetailFragment.newInstance())
                .commitNow()
        }
    }

}
